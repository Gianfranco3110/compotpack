<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/**************RESPUESTA DE PASARELA DE PAGO************/
Route::post('pago/confirm', 'ListaPlanController@confirm')->name('pago.confirm');
Route::get('pago/confirmget', 'ListaPlanController@confirm')->name('pago.confirmget');

Route::group(['middleware' => ['auth']], function() { //**************************PERFILES*******////
    Route::get('perfil/cliente', 'PerfilController@cliente')->name('perfil.cliente');
    Route::post('perfil/cliente', 'PerfilController@clienteUpdate')->name('perfil.clienteUpdate');
    Route::get('perfil/recolector', 'PerfilController@recolector')->name('perfil.recolector');

    /**************RESPUESTA DE PASARELA DE PAGO************/
    Route::post('pago/response', 'ListaPlanController@response')->name('pago.response');

});

Route::get('/app', function () {
  return redirect('app/index');
});

/**************Control del FrontEnd**************/
Route::group(['middleware' => ['web']], function () {

  Route::prefix('app')->group(function (){

    Route::get('/', 'FrontendController@index')->name('frontend.index');
    Route::get('/login', 'FrontendController@loginForm')->name('frontend.loginForm');
    Route::get('/logout', 'FrontendController@logout')->name('frontend.logout');
    Route::post('/login/authenticate', 'FrontendController@authenticate')->name('frontend.authenticate');
    Route::get('/registro', 'FrontendController@registroForm')->name('frontend.registroForm');
    Route::post('/registro/signup', 'FrontendController@signup')->name('frontend.signup');
    Route::get('/home', 'FrontendController@home')->name('frontend.home');

    Route::get('/account', 'FrontendController@account')->name('frontend.account')->middleware('auth.frontend');
    Route::post('/account/update', 'FrontendController@updateAccount')->name('frontend.account.update')->middleware('auth.frontend');
    Route::get('/mensual', 'FrontendController@mensual')->name('frontend.mensual');
    Route::get('/pago', 'FrontendController@pago')->name('frontend.pago');
    Route::get('/planes', 'FrontendController@planes')->name('frontend.planes');
    Route::get('/planes/{planId}', 'FrontendController@plan')->name('frontend.plan');
    Route::get('/tarjeta', 'FrontendController@tarjeta')->name('frontend.tarjeta');
    Route::get('/trimestral', 'FrontendController@trimestral')->name('frontend.trimestral');
  });

});

/**************OBTENER DIAS HABILITADOS PARA RECOLECCCION**************/
Route::get('/getAvailableDays', 'AjaxController@getAvailableDays')->name('ajax.getAvailableDays');

Route::group(['middleware' => ['auth', 'CheckClientePerfil']], function() { /**************El cliente debe tener el perfil completado para ingresar a estas opciones****/

    /*Route::get('/', function () {
        return view('welcome');
    });*/

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', 'HomeController@index')->name('home');

	/*****************CLIENTES***********************/
	Route::get('cliente/{cliente}/setStatus', 'ClienteController@setStatus')->name('cliente.setStatus');
	Route::get('cliente/{cliente}/destroy', 'ClienteController@destroy')->name('cliente.destroy');
	Route::post('cliente/{cliente}/update', 'ClienteController@update')->name('cliente.update');
    Route::resource('cliente', ClienteController::class, [
    	'except' => [ 'destroy','update']
    ]);

    /*****************RECOLECTORES***********************/
	Route::get('recolector/{recolector}/setStatus', 'RecolectorController@setStatus')->name('recolector.setStatus');
	Route::get('recolector/{recolector}/destroy', 'RecolectorController@destroy')->name('recolector.destroy');
	Route::post('recolector/{recolector}/update', 'RecolectorController@update')->name('recolector.update');
    Route::resource('recolector', RecolectorController::class, [
    	'except' => [ 'destroy','update']
    ]);

    /*****************PLANES***********************/
    Route::get('plan/{plan}/setStatus', 'PlanController@setStatus')->name('plan.setStatus');
    Route::get('plan/{plan}/destroy', 'PlanController@destroy')->name('plan.destroy');
    Route::post('plan/{plan}/update', 'PlanController@update')->name('plan.update');
    Route::resource('plan', PlanController::class, [
        'except' => [ 'destroy','update']
    ]);

    /*****************ZONAS***********************/
    Route::get('zona/{zona}/setStatus', 'ZonaController@setStatus')->name('zona.setStatus');
    Route::get('zona/{zona}/destroy', 'ZonaController@destroy')->name('zona.destroy');
    Route::post('zona/{zona}/update', 'ZonaController@update')->name('zona.update');
    Route::resource('zona', ZonaController::class, [
        'except' => [ 'destroy','update']
    ]);

    /*****************RUTAS***********************/
    Route::get('ruta/{ruta}/setStatus', 'RutaController@setStatus')->name('ruta.setStatus');
    Route::get('ruta/{ruta}/destroy', 'RutaController@destroy')->name('ruta.destroy');
    Route::post('ruta/{ruta}/update', 'RutaController@update')->name('ruta.update');
    Route::get('ruta/index/{daySelected?}/{weekSelected?}', 'RutaController@index')->name('ruta.index');
    Route::resource('ruta', RutaController::class, [
        'except' => [ 'destroy','update', 'index']
    ]);

    /*****************REASIGNAR FECHAS ***********************/
    Route::get('reasignar/{fecha?}', 'HomeController@reasignarFechas')->name('reasignar');

    /*****************RECOLECTOR RUTA***********************/
    Route::get('rutarecolector', 'RecolectorRutaController@index')->name('ruta.recolector');

    /*****************RECOLECTOR REPORTE***********************/
    Route::get('reporterecolector', 'ReporteRecolectorController@index')->name('reporte.recolector');
    Route::get('reportegeneral', 'ReporteGeneralController@index')->name('reporte.general');


    /*****************PAGOS RECOLECTORES***********************/
    Route::get('pagosrecolector', 'PagosRecolectorController@index')->name('pagos.recolector');
    Route::get('saveHistory', 'PagosRecolectorController@saveHistory')->name('pagos.saveHistory');
    Route::get('historial', 'PagosRecolectorController@historial')->name('pagos.historial');

    /***********************PETICIONES AJAX***************/
    Route::post('/updateRecolectorRoute','AjaxController@updateRecolectorRoute')->name('ajax.updateRecolectorRoute');
    Route::post('/updateNovedadRoute','AjaxController@updateNovedadRoute')->name('ajax.updateNovedadRoute');
    Route::post('/updateForEventDay','AjaxController@updateForEventDay')->name('ajax.updateForEventDay');
    Route::post('/saveRecolectionStatus','AjaxController@saveRecolectionStatus')->name('ajax.saveRecolectionStatus');
    Route::post('/setNewDateEvent','AjaxController@setNewDateEvent')->name('ajax.setNewDateEvent');
    Route::post('/updateDateEvent','AjaxController@updateDateEvent')->name('ajax.updateDateEvent');
    Route::post('/deleteDateEvent','AjaxController@deleteDateEvent')->name('ajax.deleteDateEvent');
    Route::post('/validatePickup','AjaxController@validatePickup')->name('ajax.validatePickup');
    Route::post('/deleteImage','AjaxController@deleteImage')->name('ajax.deleteImage');

    /***********************Lista de planes para clientes***************/
    Route::get('listado/planes','ListaPlanController@index')->name('listado.planes');

    /***********************Historico de pagos Cliente***************/
    Route::get('historial/cliente','PerfilController@historialPayments')->name('historial.cliente');

    /***********************Historico de pagos ADMIN***************/
    Route::get('historial/clientes','PerfilController@historialPaymentsAdmin')->name('historial.clientes');

    /******************ZONAS y RUTAS CONFIG********************/
    Route::get('mapa/index', 'MapaController@index')->name('mapa.index');
    Route::resource('mapa', MapaController::class, [
        'except' => [ 'destroy','update']
    ]);

    /******************Inventarios********************/
    Route::get('inventarios/index', 'InventariosController@index')->name('inventarios.index');
    Route::get('inventarios/{inventarios}/destroy', 'InventariosController@destroy')->name('inventarios.destroy');
    Route::post('inventarios/{item}/update', 'InventariosController@update')->name('inventarios.update');
    Route::resource('inventarios', InventariosController::class, [
        'except' => [ 'destroy','update']
    ]);

    /******************LISTADO DE RUTAS********************/
    /*Route::get('listarutas/index', 'ListaRutasController@index')->name('listarutas.index');
    Route::resource('listarutas', ListaRutasController::class, [
        'except' => [ 'destroy','update']
    ]);*/

});

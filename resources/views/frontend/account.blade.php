@extends('layouts.frontend')

@section('content')
<div class="container py-5 px-1" id="main">

  <h1 class="h1 mb-0 mt-5 text-center">Mi cuenta</h1>

  @if(session('success'))
    <div class="alert alert-success mt-2 text-center" role="alert">
      <strong>{{ session('success') }}</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif

  <form action="{{route('frontend.account.update')}}" class="d-flex flex-column text-center py-5" method="POST">
    @csrf

    <div class="form-group d-flex align-items-center justify-content-center py-2 px-1">
        <p class="m-0">Nombre</p>
        <input type="text" class="input-grey mx-2 @error('nombre') is-invalid @enderror" name="nombre" value="{{$user->name}}" placeholder="Nombre" required>
        @error('nombre')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
        <img src="{{ asset('img/pencil-sharp.svg') }}">
    </div>

    <div class="form-group d-flex align-items-center justify-content-center py-2 px-1">
        <p class="m-0">Apellido</p>
        <input type="text" class="input-grey mx-2 @error('apellido') is-invalid @enderror" name="apellido" value="{{$user->apellido}}"  placeholder="Apellido" required>
        @error('apellido')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
        <img src="{{ asset('img/pencil-sharp.svg') }}">
    </div>

    <div class="form-group d-flex align-items-center justify-content-center py-2 px-1 mb-1">
        <p class="m-0">Cédula</p>
        <input type="text" class="input-grey mx-2 @error('cedula') is-invalid @enderror" name="cedula" value="{{$user->cedula}}" placeholder="Cedula" required>
        @error('cedula')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
        <img src="{{ asset('img/pencil-sharp.svg') }}">
    </div>
    <small class="small-gray text-center mb-4 mx-auto">*Para hacer la factura y enviártela al correo</small>

    <div class="form-group d-flex align-items-center justify-content-center py-2 px-1">
        <p class="m-0">Correo</p>
        <input type="email" class="input-grey mx-2 @error('email') is-invalid @enderror" name="email" value="{{$user->email}}" placeholder="Correo" disabled>
        @error('email')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
        <!--img src="img/pencil-sharp.svg') }}"-->
    </div>

    <div class="form-group d-flex align-items-center justify-content-center py-2 px-1 mb-1">
        <p class="m-0 d-block">WhatsApp</p>
        <input type="text" class="input-grey mx-2 @error('whatsapp') is-invalid @enderror" type="text" name="whatsapp" value="{{$user->whatsapp}}"  placeholder="WhatsApp" required>
        @error('whatsapp')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
        <img src="{{ asset('img/pencil-sharp.svg') }}">
    </div>
    <small class="small-gray mb-4 mx-auto text-center">*Asegúrate de usar un celular que tenga WhatsApp. Todas las notificaciones son enviadas por este medio. Verifica que se ingrese un número de 10 dígitos sin espacios</small>

    <div class="form-group d-flex align-items-center justify-content-center py-2 px-1 mb-1">
        <p class="m-0 d-block">Contraseña</p>
        <input type="password" class="input-grey ml-2 @error('password') is-invalid @enderror" name="password" value="" placeholder="Nueva Contraseña"s>
        @error('password')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
    </div>
    <a href="" class="link-gray text-right mr-3">CAMBIAR CONTRASEÑA</a>

    <small class="small-green text-center mx-auto mt-3">LUGAR PARA RECOGER LOS RESIDUOS</small>
    <div class="form-group d-flex align-items-center justify-content-center py-2 px-1 my-0">
        <p class="m-0 d-block">Dirección</p>
        <input type="text" class="input-grey mx-2 @error('direccion') is-invalid @enderror" name="direccion" value="{{$user->direccion}}" placeholder="Dirección" required>
        @error('direccion')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
        <img src="{{ asset('img/pencil-sharp.svg') }}">
    </div>

    <img src="{{ asset('img/mapa.png') }}" class="mapa img-fluid d-block mx-auto my-3">

    <div class="d-flex justify-content-between justify-content-sm-around text-center">
        <input type="checkbox" id="checkbox" class="checkbox @error('casa_apartamento') is-invalid @enderror" name="casa_apartamento" value="Casa" {{ ($user->casa_apartamento == 'casa') ? 'checked' : '' }}>
        <label for="checkbox" class="py-2 px-3 casa">Casa</label>
        <input type="checkbox" id="checkbox2" class="checkbox @error('casa_apartamento') is-invalid @enderror" name="casa_apartamento" value="Apartamento" {{ ($user->casa_apartamento == 'apartamento') ? 'checked' : '' }}>
        <label for="checkbox2" class="py-2 px-3 apart">Apartamento</label>
        @error('casa_apartamento')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
    </div>

    <div class="form-group d-flex align-items-center justify-content-center py-2 px-1 my-3">
        <p class="m-0 d-block">Torre/apto</p>
        <input type="text" class="input-grey ml-2 @error('torre_apto') is-invalid @enderror" name="torre_apto" value="{{ $user->torre_apto}}" placeholder="# de Torre/apto" required>
        @error('torre_apto')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
    </div>

    <small class="small-green text-center mx-auto mt-3 mb-2 mx-auto">DÍA DE RECOLECCIÓN</small>
    <div class="form-group2 d-flex align-items-center justify-content-between mb-3 @error('dia_recoleccion') is-invalid @enderror">
        <input type="radio" class="checkbox" id="checkbox3" name="dia_recoleccion" value="Mon" {{ ($user->dias_recoleccion == 'Mon') ? 'checked' : '' }}>
        <label for="checkbox3" class="p-1 lun">Lunes</label>
        <input type="radio" class="checkbox" id="checkbox4" name="dia_recoleccion" value="Tue" {{ ($user->dias_recoleccion == 'Tue') ? 'checked' : '' }}>
        <label for="checkbox4" class="p-1 mart">Martes</label>
        <input type="radio" class="checkbox" id="checkbox5" name="dia_recoleccion" value="Wed" {{ ($user->dias_recoleccion == 'Wed') ? 'checked' : '' }}>
        <label for="checkbox5" class="p-1 mier">Miercoles</label>
        <input type="radio" class="checkbox" id="checkbox6" name="dia_recoleccion" value="Thu" {{ ($user->dias_recoleccion == 'Thu') ? 'checked' : '' }}>
        <label for="checkbox6" class="p-1 juev">Jueves</label>
        <input type="radio" class="checkbox" id="checkbox7" name="dia_recoleccion" value="Fri" {{ ($user->dias_recoleccion == 'Fri') ? 'checked' : '' }}>
        <label for="checkbox7" class="p-1 vier">Viernes</label>
        <!--input type="checkbox" class="checkbox" id="checkbox8" name="dia_recoleccion" {{ ($user->dia_recoleccion == 'Mon') ? 'checked' : '' }}>
        <label for="checkbox8" class="p-1 any">Cualquiera</label-->
    </div>
    @error('dia_recoleccion')
      <span class="invalid-feedback show" role="alert">
          <strong>{{ $message }}</strong>
      </span>
    @enderror

    <small class="small-gray text-center mt-1 mb-3 mb-md-4 mx-auto"><b>1)</b>Las recogidas las haremosentre las 8:00 a.m. y 6:00 p.m. <b>2)</b>Si el día es festivo, la recogida se moverá a un día cercano. <b>3)</b>Cuando confirmemos tu suscripción te indicaremos las fechas de recogida al WhatsApp y correo suministrados.</small>

    <small class="link-gray text-center mx-auto">¿CÚANTAS PERSONAS HABITAN EN TU VIVIENDA?</small>
    <div class="form-group3 d-flex align-items-center justify-content-center py-2 px-1 mb-3">
        <input type="text" class="input-grey mx-2 @error('pregunta_1') is-invalid @enderror" name="pregunta_1" value="{{ $user->pregunta_1 }}" placeholder="Respuesta" required>
        @error('pregunta_1')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
        <img src="{{ asset('img/pencil-sharp.svg') }}" class="mr-0 ml-auto">
    </div>

    <small class="link-gray text-center mx-auto">¿CÚANTAS PLANTAS TIENES EN TU VIVIENDA?</small>
    <div class="form-group3 d-flex align-items-center justify-content-center py-2 px-1 mb-3">
        <input type="text" class="input-grey mx-2 @error('pregunta_2') is-invalid @enderror" name="pregunta_2" value="{{ $user->pregunta_2 }}" placeholder="Respuesta" required>
        @error('pregunta_2')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
        <img src="{{ asset('img/pencil-sharp.svg') }}" class="mr-0 ml-auto">
    </div>

    <!--div class="d-flex align-items-center justify-content-center">
        <input type="checkbox" class="check ml-0 mr-1" id="check">
        <label for="check" class="checklabel"><small class="link-gray my-3 m-0">Aceptar TyC y política de tratamiento de datos</small></label>
    </div-->

    <input type="submit" value="GUARDAR Y ESCOGER PLAN" class="btn btn-darkgray btn-block mt-4 mx-auto">
  </form>

  <img src="{{ asset('img/planeta.png') }}" class="img-planet img2 d-block ml-auto mr-0">

  <img src="{{ asset('img/flecha-gris.png') }}" class="flecha-gris arriba" id="flecha-arriba">
  <img src="{{ asset('img/flecha-gris.png') }}" class="flecha-gris abajo" id="flecha-abajo">
</div>
@endsection

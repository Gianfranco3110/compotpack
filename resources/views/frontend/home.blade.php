@extends('layouts.frontend')

@section('content')
<div class="container py-2 px-1">
    <p class="text-small text-center m-0">Recolección de <br> residuos compostables</p>
    <a href="{{route('frontend.index')}}"><img src="{{ asset('img/logo-2.png') }}" class="logo2 d-block mx-auto mb-3"></a>
    <h1 class="h1 my-2 text-center">¿CÓMO FUNCIONA?</h1>

    <div class="video" controls poster="{{ asset('img/poster.png') }}">
        <video controls poster="{{ asset('img/poster.png') }}" id="video"></video>
        <iframe class="iframe" src="" frameborder="0" id="youtube" allow='autoplay' style="display:none;"></iframe>
        <a><img src="{{ asset('img/play.png') }}" class="play" id="btn-play"></a>
    </div>


    <img src="{{ asset('img/flecha-verde.png') }}" class="flecha2 d-block mx-auto my-2">
    @if(Auth::check())
    <div class="d-flex flex-column text-center">
      <p class="m-0">¡Hola {{Auth::user()->name}}¡</p>
      <a href="{{route('frontend.account')}}" class="btn btn-warn btn-block mx-auto">Mi cuenta</a>
    </div>
    @else
    <div class="d-flex flex-column text-center">
      <a href="{{route('frontend.loginForm')}}" class="btn btn-warn btn-block mx-auto">Iniciar sesión</a>
      <p class="m-0">o</p>
      <a href="{{route('frontend.registroForm')}}" class="btn btn-warn btn-block mx-auto">Registrarme</a><!--Suscribirme-->
    </div>
    @endif

    <img src="{{ asset('img/planeta.png') }}" class="img-planet img1 d-block ml-auto mr-0">
</div>
@endsection

@extends('layouts.frontend')

@section('content')
<div class="container py-2 px-1">
    <p class="text-small text-center">Recolección de <br> residuos compostables</p>

    <a href="{{route('frontend.home')}}"><img src="{{ asset('img/logo-2.png') }}" class="logo2 d-block mx-auto mb-3"></a>

    <h1 class="h1 my-3 text-center">Iniciar sesión</h1>

    <form action="{{route('frontend.authenticate')}}" class="d-flex flex-column text-center" method="post">
        @csrf
        <input type="email" class="form-control input-warn mx-auto @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Correo" required>
        @error('email')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
        <p class="my-3"></p>
        <input type="password" class="form-control input-warn mx-auto @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" required>
        @error('password')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
         @enderror
        <!--a href="tarjeta.html" class="btn btn-darkgreen btn-block mt-4 mx-auto">Continuar</a-->
        <input type="submit" value="Continuar" class="btn btn-darkgreen btn-block mt-4 mx-auto">
    </form>

    <img src="{{ asset('img/planeta.png') }}" class="img-planet img1 d-block ml-auto mr-0">
</div>
@endsection

@extends('layouts.frontend')

@section('content')
  <div class="container py-5 px-1">
    <a href="{{route('frontend.home')}}"><img src="{{ asset('img/logo-1.png') }}" class="logo d-block mx-auto"></a>
    <img src="{{ asset('img/flecha-amarilla.png') }}" class="flecha d-block mx-auto my-3">
    <h1 class="text-center">PULSA POR UN <br> PLANETA MÁS LIMPIO</h1>
  </div>
@endsection

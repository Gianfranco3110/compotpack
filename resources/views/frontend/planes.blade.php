@extends('layouts.frontend')

@section('content')
<div class="container py-5 px-1" id="main">

  <h1 class="h1 mb-0 mt-5 text-center">PLANES</h1>

  @if(session('success'))
    <div class="alert alert-danger mt-2 text-center" role="alert">
      <strong>{{ session('success') }}</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
  @if(session('error'))
    <div class="alert alert-success mt-2 text-center" role="alert">
      <strong>{{ session('error') }}</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif

  @foreach($planesOnce as $plan)

  <a href="{{route('frontend.plan',['planeId'=>$plan->id])}}">
      <div class="warning d-flex flex-column align-items-center justify-content-center text-center px-4 py-5">
          <h1 class="h1">{{ $plan->titulo }}</h1>
          <small class="m-0">Crédito</small>
          <h2 class="py-0 px-4">${{ number_format($plan->precio, 0, '.', ',') }}</h2>
      </div>
  </a>
  <br>

  @endforeach

  <a href="{{route('frontend.trimestral')}}">
      <div class="darkgreen d-flex flex-column align-items-center justify-content-center text-center px-4 py-5">
          <h1 class="h1">PLAN TRIMESTRAL</h1>
          <small class="m-0">Otros medios de pago</small>
          <h2 class="py-0 px-4">$135.000</h2>
      </div>
  </a>
</div>
@endsection

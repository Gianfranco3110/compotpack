@extends('layouts.frontend')

@section('content')
<div class="container py-5 px-1" id="main">

  <h1 class="h1 mb-0 mt-5 text-center">PLAN</h1>

  <div class="warning d-flex flex-column align-items-center justify-content-center text-center p-2">
    <h1 class="h1 m-0">{{ $plan->titulo }}</h1>
    <small class="m-0">Crédito</small>
    <small class="m-0">Puedes registrar tu tarjeta de crédito y se realizará un cobro automático cada mes.</small>

    <hr class="mx-0 mb-0 mt-2">

    <div class="d-flex align-items-center justify-content-between my-2">
      <div class="box-img d-flex align-items-center justify-content-center mr-2">
        <img src="{{ asset('img/Cpack.jpg') }}" data-toggle="modal" data-target="#modal2">
      </div>
      <div class="items d-flex flex-column justify-content-center">
        <small class="m-0 kit">KIT INICIAL</small>
        <small class="text-left">Fundamental para el funcionamiento del servicio, para evitar olores y asegurar un compostaje adecuado. Solo se paga una vez.</small>
      </div>
    </div>
  </div>

  <form class="col">
    <div class="form-check d-flex align-items-center my-3 justify-content-center justify-content-sm-around">
        <div class="d-flex align-items-center">
            <input type="radio" class="check ml-0 mb-2 mr-1" name="kit" id="sin_kit" onchange="sinKit(this)">
            <label for="sin_kit" class="lab">
                <small class="link-gray m-0">Ya tengo Kit inicial</small>
            </label>
        </div>
        <div class="d-flex align-items-center">
            <input type="radio" class="check ml-auto mb-2 mr-1" name="kit" id="con_kit" onchange="conKit(this)">
            <label for="con_kit" class="lab">
                <small class="link-gray m-0">Adquirir Kit inicial</small>
            </label>
        </div>
    </div>

    <p class="lead bold text-center">RESUMEN DE PAGO</p>
    <div class="group d-flex align-items-center px-3 py-1 py-md-2">
        <p class="lead m-0">PLAN</p>
        <span class="text-center">{{ $plan->titulo }}</span>
    </div>

    <div class="group d-flex align-items-center px-3 py-1 py-md-2">
        <p class="lead m-0">KIT INICIAL</p>
        <span class="lead text-center" id="valorKit"></span>
    </div>

    <div class="group2 d-flex align-items-center px-3 py-1 py-md-2">
        <p class="lead bold m-0">TOTAL PRIMER MES</p>
        <span class="bold text-center" id="totalMes">$60.000</span>
    </div>

    <div class="group2 d-flex align-items-center px-3 py-1 py-md-2">
        <p class="lead bold m-0">TOTAL SIGUIENTE MES</p>
        <span class="lead text-center" id="totalNext">$45.000</span>
    </div>

    <input type="button" value="PAGAR" class="btn btn-darkgray-sm btn-block mt-4 mx-auto" onclick="makePayment()">
    <!--input type="button" value="PAGAR" class="btn btn-darkgray-sm btn-block mt-4 mx-auto" id="btnModal" data-toggle="modal" data-target="#modal"-->

  </form>

  <div class="modal fade" id="modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header px-3">
          <h4 class="modal-title">AGREGAR TARJETA</h4>
          <button class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body d-flex flex-column">
          <input type="text" class="input-lightgrey py-3 px-2 mb-2 mx-2" placeholder="NOMBRE COMPLETO" required>
          <input type="text" class="input-lightgrey py-3 px-2 mb-2 mx-2" placeholder="NÚMERO DE LA TARJETA" required>

          <div class="d-flex align-items-center mx-2 my-3">
            <input type="text" class="input-light-grey input-sm" placeholder="MM" required>
            <p class="my-0 mx-2 sep">/</p>
            <input type="text" class="input-light-grey input-sm" placeholder="YY" required>
            <input type="text" class="input-light-grey input-sm ml-2" placeholder="CCV" required>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal2" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
              <div class="modal-header px-3">
                  <button class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <img src="{{ asset('img/Cpack.jpg') }}" class="img-fluid">
              </div>
          </div>
      </div>
  </div>

  <script type="text/javascript" src="https://checkout.epayco.co/checkout.js">   </script>

<script type="text/javascript">

  const mPrecioPlan = Number({{ $plan->precio }});
  const mValorKit = 15000;
  const mName = "{{ $plan->titulo}}";
  const mDescription = "{{ $plan->titulo }}. {{ $plan->frecuencia_cobro }} Mes(es).";
  const mResponse = "{{ route('pago.response') }}";
  const mConfirm = "{{ route('pago.confirm') }}";
  const mId = "{{ Auth::user()->id.'_'.$plan->id.'_'.time() }}";

  const handler = ePayco.checkout.configure({
  				key: '787c4340a28c7c4cbfa029c0f948db33',
  				test: true
  });

  function makePayment(){
    var total = mPrecioPlan;

    if($("#con_kit").attr("checked")){
      total += mValorKit;
    }

    var data={
      //Parametros compra (obligatorio)
      name: mName,
      description: mDescription,
      invoice: mId,
      currency: "cop",
      amount: total,
      country: "co",
      lang: "es",

      //Onpage="false" - Standard="true"
      external: "true",

      //Atributos opcionales
      confirmation: mConfirm,
      response: mResponse,
    }

    handler.open(data);
  }

  function sinKit(element){
    var valorKitText = document.getElementById("valorKit");
    var totalMes = document.getElementById("totalMes");
    var totalNext = document.getElementById("totalNext");

    var total = mPrecioPlan;
    valorKitText.innerHTML = "$0";
    totalMes.innerHTML = "$"+total;
    totalNext.innerHTML = "$"+mPrecioPlan;

    $("#con_kit").removeAttr("checked");

  }
  function conKit(element){
    var valorKitText = document.getElementById("valorKit");
    var totalMes = document.getElementById("totalMes");
    var totalNext = document.getElementById("totalNext");

    var total = mPrecioPlan+mValorKit;
    valorKitText.innerHTML = "$"+mValorKit;
    totalMes.innerHTML = "$"+total;
    totalNext.innerHTML = "$"+mPrecioPlan;

    $("#sin_kit").removeAttr("checked");
    $("#con_kit").attr("checked", "");
  }

  $(function () {
    $("#con_kit").attr("checked", "");
    conKit($("#con_kit"));
  });

</script>

</div>
@endsection

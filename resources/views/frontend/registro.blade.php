@extends('layouts.frontend')

@section('content')
<div class="container py-2 px-1">
  <a href="{{route('frontend.home')}}"><img src="/img/logo-2.png" class="logo2 d-block mx-auto mb-3"></a>

  <h1 class="h1 my-3 text-center">Registro</h1>

  <form action="{{route('frontend.signup')}}" class="d-flex flex-column text-center" method="POST">
    @csrf
    <input type="text" class="form-control input-green mx-auto mb-2  @error('nombre') is-invalid @enderror" name="nombre" placeholder="Nombre" required>
    @error('nombre')
      <span class="invalid-feedback show" role="alert">
          <strong>{{ $message }}</strong>
      </span>
    @enderror
    <input type="text" class="form-control input-green mx-auto mb-2 @error('apellido') is-invalid @enderror" name="apellido" placeholder="Apellido" required>
    @error('apellido')
      <span class="invalid-feedback show" role="alert">
          <strong>{{ $message }}</strong>
      </span>
    @enderror
    <input type="email" class="form-control input-green mx-auto mb-2 @error('email') is-invalid @enderror" name="email" placeholder="Correo" required>
    @error('email')
      <span class="invalid-feedback show" role="alert">
          <strong>{{ $message }}</strong>
      </span>
    @enderror

    <input class="form-control input-green mx-auto mb-2 @error('telefono') is-invalid @enderror" type="text" name="telefono" onkeypress='return validaNumericos(event)' maxlength="10" placeholder="Telefono" required>
    @error('telefono')
      <span class="invalid-feedback show" role="alert">
          <strong>{{ $message }}</strong>
      </span>
    @enderror

    <input class="form-control input-green mx-auto mb-2 @error('whatsapp') is-invalid @enderror" type="text" name="whatsapp" onkeypress='return validaNumericos(event)' maxlength="10" placeholder="WhatsApp" required>
    @error('whatsapp')
      <span class="invalid-feedback show" role="alert">
          <strong>{{ $message }}</strong>
      </span>
    @enderror

    <small class="mt-1 mb-3 mx-auto">Asegúrate de usar un celular que tenga WhatsApp. Todas las notificaciones son enviadas por este medio. Verifica que se ingrese un número de 10 dígitos sin espacios</small>

    <input type="password" class="form-control input-green mx-auto @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" required>
    <small class="mt-1 mb-3 mx-auto">Mínimo 8 caracteres</small>
    @error('password')
      <span class="invalid-feedback show" role="alert">
          <strong>{{ $message }}</strong>
      </span>
    @enderror

    <input type="password" class="form-control input-green mx-auto @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder="Confirmar Contraseña" required>
    @error('password_confirmation')
      <span class="invalid-feedback show" role="alert">
          <strong>{{ $message }}</strong>
      </span>
    @enderror

    <input type="text" class="form-control input-green mx-auto mt-2 @error('direccion') is-invalid @enderror" id="direccion" name="direccion" placeholder="Dirección" required>
    @error('direccion')
      <span class="invalid-feedback show" role="alert">
          <strong>{{ $message }}</strong>
      </span>
    @enderror

    <div class="mx-auto input-green mt-2">
        Ubicación en mapa
        <div id="map" style="min-height: 350px; width: 100%;">
        </div>
        <div class="form-group no-style mt-1">
          <div class="inline-block">
            <label>Latitud</label>
              <input type="text" value="4.65" class="form-control" name="latitud" id="latitud" required readonly>
          </div>

          <div class="inline-block">
            <label>Longitud</label>
              <input type="text" value="-74.058" class="form-control" name="longitud" id="longitud" required readonly>
          </div>
        </div>
      <button class="btn btn-primary mb-2" type="button" id="detectGeo">Detectar mi Ubicación</button>
    </div>

    <input type="text" class="form-control input-green mx-auto mt-2 @error('casa_apartamento') is-invalid @enderror" name="casa_apartamento" maxlength="12" placeholder="Casa ó Apartamento" required>
    @error('casa_apartamento')
      <span class="invalid-feedback show" role="alert">
          <strong>Asegúrate de solo Colocar Casa ó Apartamento</strong>
      </span>
    @enderror

    <input type="text" class="form-control input-green mx-auto mt-2 @error('torre_apto') is-invalid @enderror" name="torre_apto" placeholder="# de Torre/Apto" required>
    @error('torre_apto')
      <span class="invalid-feedback show" role="alert">
          <strong>{{ $message }}</strong>
      </span>
    @enderror

    <div class="mx-auto input-green mt-2">
      Día de recolección
      <div class="form-group no-style">

        <div class="form-check inline-block">
          <input class="no-style" type="radio" name="dia_recoleccion" id="input-Mon" onchange="daySelected(this.value)" value="Mon">
          <label class="no-style inline-flex" for="input-Mon" id="label-Mon">
            Lunes
          </label>
        </div>
        <div class="form-check inline-block">
          <input class="no-style " type="radio" name="dia_recoleccion" id="input-Tue" onchange="daySelected(this.value)" value="Tue">
          <label class="no-style inline-flex" for="input-Tue" id="label-Tue">
            Martes
          </label>
        </div>
        <div class="form-check inline-block">
          <input class="no-style" type="radio" name="dia_recoleccion" id="input-Wed" onchange="daySelected(this.value)" value="Wed">
          <label class="no-style inline-flex" for="input-Wed" id="label-Wed">
            Miercoles
          </label>
        </div>
        <div class="form-check inline-block">
          <input class="no-style" type="radio" name="dia_recoleccion" id="input-Thu" onchange="daySelected(this.value)" value="Thu">
          <label class="no-style inline-flex" for="input-Thu" id="label-Thu">
            Jueves
          </label>
        </div>
        <div class="form-check inline-block">
          <input class="no-style" type="radio" name="dia_recoleccion" id="input-Fri" onchange="daySelected(this.value)" value="Fri">
          <label class="no-style inline-flex" for="input-Fri" id="label-Fri">
            Viernes
          </label>
        </div>

        @error('dia_recoleccion')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror

        <div class="mx-auto m-2">
          <div id="disclaimers" class="alert alert-warning d-none">
            <ul class="text-left">
              <li style="font-size:15px;">Las recogidas las haremos entre las 8am y 6pm.</li>
              <li style="font-size:15px;">Si el día es festivo, la recogida se moverá a un día cercano.</li>
              <li style="font-size:15px;">Cuando confirmemos el pago te indicaremos las fechas de recogida al WhatsApp y correo suministrados.</li>
            </ul>
          </div>
        </div>

      </div>
    </div>

    <input type="text" class="form-control input-green mx-auto mt-2 @error('pregunta_1') is-invalid @enderror" name="pregunta_1" placeholder="¿Cuántas personas habitan en tu vivienda?" required>
    @error('pregunta_1')
      <span class="invalid-feedback show" role="alert">
          <strong>{{ $message }}</strong>
      </span>
    @enderror

    <input type="text" class="form-control input-green mx-auto mt-2 @error('pregunta_2') is-invalid @enderror" name="pregunta_2" placeholder="¿Cuántas plantas tienes en tu vivienda?" required>
    @error('pregunta_2')
      <span class="invalid-feedback show" role="alert">
          <strong>{{ $message }}</strong>
      </span>
    @enderror

    <div class="d-flex align-items-center justify-content-center">
        <input type="checkbox" class="check ml-0 mr-3 @error('acepto_TyC') is-invalid @enderror" id="checkb" name="acepto_TyC" required>
        <small class="my-3 m-0">Aceptar Condiciones Generales de Uso y la Política de Tratamiento de Datos</small>
        @error('acepto_TyC')
          <span class="invalid-feedback show" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
    </div>

    <input type="submit" value="Crear cuenta" class="btn btn-darkgreen btn-block mt-2 mx-auto">
  </form>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCF9LXYgTkSgr5QOu9OJelPqvJB-1Ij9BE" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('maps/gmaps.js') }}"></script>

<script type="text/javascript">

var latLocation = 4.65;
var lngLocation = -74.058;

var diasHabilitados = [];

function daySelected(value){
var alert = document.getElementById("disclaimers");
alert.className = "alert alert-warning fade show";
isDaySelected = true;
}

var map = new GMaps({
  el: '#map',
  lat: latLocation,
  lng: lngLocation,
  zoomControl : true,
  zoomControlOpt: {
      style : 'SMALL',
      position: 'TOP_LEFT'
  },
  zoom : 17,
  panControl : false,
  streetViewControl : false,
  mapTypeControl: false,
  overviewMapControl: false
});

var marker = map.addMarker({
  lat: latLocation,
  lng: lngLocation,
  draggable: true,
  dragend: function(event) {
      var lat = event.latLng.lat();
      var lng = event.latLng.lng();
      //alert('draggable '+lat+" - "+ lng);
      latLocation = lat;
      lngLocation = lng;
      latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
      $("#latitud").val(lat);
      $("#longitud").val(lng);
      //getGeocode(latlng);

      if(!map.checkGeofence(parseFloat(lat), parseFloat(lng), polygon)) { /***DETERMINAR SI ESTA DENTRO DEL POLIGONO UN PUNTO******/
        $('#direccion').addClass('is-invalid');
        $('#goRegister').prop('disabled', true);
        alert("La dirección indicada no esta dentro de la zona de cobertura. Por favor modificar la dirección del cliente.");
      }
      else{
        $('#goRegister').prop('disabled', false);
        $('#direccion').removeClass('is-invalid');
        console.log("SI ESTA");
      }
  },
  title: 'Marker #' ,
  infoWindow: {
      content: "Tu Dirección"
  }
});

var path = [[4.770928117510021, -74.04265239031186], [4.768514405885835, -74.0440889763219], [4.770086920195376, -74.05161471730683], [4.765520570614821, -74.05264647211928], [4.7724759254331826, -74.06384404675462], [4.763493329879006, -74.06973373342461], [4.720508073457822, -74.09427476213008], [4.717209837728416, -74.09272511207857], [4.711497532293983, -74.0934947391899], [4.678742079110091, -74.1198813070372], [4.626467500125243, -74.09451574130638], [4.624529109373487, -74.08309819323918], [4.620051787632438, -74.0866387502758], [4.603219082700261, -74.06236438227229] /*carreara 4 Este*/, [4.610027444116612, -74.06581683497656], [4.623711588338709, -74.05651986207768], [4.6382116916530425, -74.052425340121], [4.6382116916530425, -74.052425340121], [4.667446185526047, -74.04412931225994], [4.685594146626684, -74.03533813302084], [4.683265664738547, -74.02883506907601], [4.686530338277964, -74.01898413085281], [4.706214075556662, -74.01963443711685], [4.715599661879258, -74.0180447991568], [4.728057571428454, -74.01243289574812], [4.735042544131227, -74.01178258927786], [4.74274753529203, -74.01421521681955], [4.761757612746261, -74.0210554768808], [4.769294291108273, -74.02235608971907], [4.769798333514273, -74.02664329483824], [4.77018604607833, -74.02685351796043]];

polygon = map.drawPolygon({
  paths: path, // pre-defined polygon shape
  strokeColor: '#000000',
  strokeOpacity: 1,
  strokeWeight: 3,
  fillColor: '#E5FFD2',
  fillOpacity: 0.2,
  zIndex: 50000000000000
});

$("#direccion").keyup(function(){
var dirRequest = $("#direccion").val();
GMaps.geocode({
  address: dirRequest,
  callback: function(results, status) {
    if (status == 'OK') {
      var latlng = results[0].geometry.location;
      latLocation = parseFloat(latlng.lat());
      lngLocation = parseFloat(latlng.lng());
      var latlng = {lat: parseFloat(latLocation), lng: parseFloat(lngLocation)};
      $("#latitud").val(latLocation);
      $("#longitud").val(lngLocation);
      map.setCenter(latlng);
      marker.setPosition(latlng);

      if(!map.checkGeofence(parseFloat(latlng.lat()), parseFloat(latlng.lng()), polygon)) { /***DETERMINAR SI ESTA DENTRO DEL POLIGONO UN PUNTO******/
        $('#direccion').addClass('is-invalid');
        $('#goRegister').prop('disabled', true);
        alert("La dirección indicada no esta dentro de la zona de cobertura. Por favor modificar la dirección del cliente.");
      }
      else{
        $('#goRegister').prop('disabled', false);
        $('#direccion').removeClass('is-invalid');
        console.log("SI ESTA");
      }
    }
  }
});
});

$("#detectGeo").click(function(){
GMaps.geolocate({
success: function(position) {
  esLocation = true;
  latLocation = position.coords.latitude;
  lngLocation = position.coords.longitude;
  var latlng = {lat: parseFloat(latLocation), lng: parseFloat(lngLocation)};
  $("#latitud").val(latLocation);
  $("#longitud").val(lngLocation);
  GMaps.geocode({
  location: latlng,
  callback: function(results, status) {
    if (status == 'OK') {
      var direc = results[0].formatted_address;
      var res = direc.split(",");
        $("#direccion").val(res[0]+","+res[1]+".");
     map.setCenter(latlng);
     marker.setPosition(latlng);
    }
    else{
      alert("No hemos podido detectar tu ubicación.");
    }
  }
});

},
error: function(error) {
  alert('Ha fallado la geolocalización, debes dar permisos al navegador para poder usar estar opción.');//+error.message);
},
not_supported: function() {
  alert("Tu navegador no es compatible con geolacalización");
},
always: function() {
  //alert("Done!");
}
});
});

function getGeocode (latlng) {
GMaps.geocode({
  location: latlng,
  callback: function(results, status) {
    if (status == 'OK') {
      var direc = results[0].formatted_address;
    $("#direccion").val(direc);
  }
  }
 });
}

$(function () {
$.ajax({
  //url:'https://compostpack.com/webApp01-30-2021/compotpack/public/getAvailableDays',
  url:"{{route('ajax.getAvailableDays')}}",
  headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
  type:"GET",
  success:function(data){
    if(data.status){
      diasHabilitados = data.data;
      for (const property in diasHabilitados) {
        if(!diasHabilitados[property]){
          $("#input-"+property).prop('disabled', true);
          $("#label-"+property).tooltip({'trigger':'hover', 'title': 'No Disponible'});
        }
      }

    }else{
      console.log(data);
    }
  },
  error:function(data){
    alert("Error: "+data.responseJSON.message);
    console.log(data);
  }
});
});

$(document).ready(function() {

});

</script>
@endsection

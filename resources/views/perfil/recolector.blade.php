@extends('layouts.main')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Mi Perfil') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

          	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Mis Datos') }}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">

                <div class="row">
                	<div class="col-md-4">
                		<div class="form-group">
		                	<label>Nombre</label>
                      <div>{{$recolector->name }}</div>
		                    <!--input type="text" value="{{ old('name', $recolector->name) }}" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Nombre Completo"-->
		                </div>

		                <div class="form-group">
		                	<label>Email</label>
                      <div>{{$recolector->name }}</div>
		                  <!--input type="email" value="{{ old('email', $recolector->email) }}" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Email" required readonly-->
		                </div>

		                <div class="form-group">
		                	<label>Dirección</label>
                      <div>{{$recolector->direccion }}</div>
		                  <!--input type="text" value="{{ old('direccion', $recolector->direccion) }}" class="form-control @error('direccion') is-invalid @enderror" name="direccion" id="direccion" placeholder="Dirección" required-->
		                </div>
                    <div class="form-group">
                      <label>Capacidad Diaria</label>
                      <div>{{$recolector->capacidad }}</div>
                    </div>

                    <label>Días de recolección</label>
                    <div class="form-group">

                      @foreach ($semana as $key => $dia)
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="exampleRadios{{ $key }}" value="{{ $key }}" {{ $diasRecoleccion[$key] == 1 ? 'checked' : '' }} disabled>
                        <label class="form-check-label" for="exampleRadios{{ $key }}">
                          {{ $dia }}
                        </label>
                      </div>
                      @endforeach
                    </div>
                	</div>

                	<div class="col">

                    <div class="form-group">
                      <label>Whatsapp</label>
                      <div>{{$recolector->whatsapp }}</div>
                    </div>

                		<div class="form-group">
                      <label>Vehiculo</label>
                      <div>{{$recolector->vehiculo }}</div>
                    </div>

                    <div class="form-group">
                      <label>Color del Vehiculo</label>
                      <div>{{$recolector->color }}</div>
                    </div>

                    <div class="form-group">
                      <label>Placa</label>
                      <div>{{$recolector->placa }}</div>
                    </div>
                	</div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Foto Actual</label>
                      <br>
                      <img style="max-width:120px;" src="{{ asset('uploads/usuarios/'.$recolector->image) }}">
                      <button class="btn btn-primary" type="button" onclick="verImagen('{{ asset('uploads/usuarios/'.$recolector->image) }}')">Ver</button>
                    </div>

                    <div class="form-group">
                      <label>Foto Licencia Conducción</label>
                      <br>
                      <img style="max-width:120px;" src="{{ asset('uploads/usuarios/'.$recolector->licencia) }}">
                      <button class="btn btn-primary" type="button" onclick="verImagen('{{ asset('uploads/usuarios/'.$recolector->licencia) }}')">Ver</button>
                    </div>

                    <div class="form-group">
                      <label>Foto SOAT</label>
                      <br>
                      <img style="max-width:120px;" src="{{ asset('uploads/usuarios/'.$recolector->soat) }}">
                      <button class="btn btn-primary" type="button" onclick="verImagen('{{ asset('uploads/usuarios/'.$recolector->soat) }}')">Ver</button>
                    </div>
                  </div>

                	<div class="col-md-6">
                    <a href="{{ url()->previous() }}" class="btn btn-danger" type="button">Regresar</a>
                	</div>
                </div>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>

      <div class="modal fade" id="verImageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Foto</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <img style="width:100%;" id="verFoto">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <script>
        function verImagen(url){
          $('#verFoto').attr('src', url);
          $('#verImageModal').modal('show');
        }
      </script>
    </section>
    <!-- /.content -->
@endsection

@extends('layouts.main')

@section('extras-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Historial de Pagos') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
			
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Historial') }}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
              
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Identificador</th>
                    <th>Monto</th>
                    <th>Fecha</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach ($historial as $key => $historia)
                  <tr>
                    <td>{{ $historia->id }}</td>
                    <td>${{ number_format($historia->x_amount, 0 , '.',',') }}</td>
                    <td>{{ date('d/m/Y H:i:s', strtotime($historia->fecha)) }}</td>
                    <td>{{ $historia->estado }}</td>
                    <td>
                      <div class="btn-group">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#historyModal{{ $key }}">
                            <button type="button" class="btn btn-default" title="Mas Detalles"><i class="nav-icon fas fa-eye"></i></button>
                        </a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Identificador</th>
                    <th>Monto</th>
                    <th>Fecha</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                  </tr>
                  </tfoot>
              </table>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->

    @foreach($historial as $key => $historia)
    <!-- Modal -->
    <div class="modal fade" id="historyModal{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Detalles de la transacción</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <meta name="csrf-token" content="{{ csrf_token() }}" />
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <!-- /.card-header -->
                  <div class="card-body p-0">
                    <table class="table table-striped">
                      <tbody>
                        <tr>
                          <td><b># Factura:</b></td>
                          <td>{{ $historia->id }}</td>
                        </tr>

                        <tr>
                          <td><b>Monto:</b></td>
                          <td>${{ number_format($historia->x_amount, 0 , '.',',') }}</td>
                        </tr>

                        <tr>
                          <td><b>Estado:</b></td>
                          <td>{{ $historia->estado }}</td>
                        </tr>

                        <tr>
                          <td><b>Fecha:</b></td>
                          <td>{{ date('d/m/Y H:i:s', strtotime($historia->fecha)) }}</td>
                        </tr>

                        <tr>
                          <td><b>Referencia:</b></td>
                          <td>{{ $historia->x_ref_payco }}</td>
                        </tr>

                        <tr>
                          <td><b>Epayco Id:</b></td>
                          <td>{{ $historia->x_transaction_id }}</td>
                        </tr>

                        <tr>
                          <td><b>Moneda:</b></td>
                          <td>{{ $historia->x_currency_code }}</td>
                        </tr>

                        <tr>
                          <td><b>Plan adquirido:</b></td>
                          <td>{{ $historia->titulo_plan }}</td>
                        </tr>

                        <tr>
                          <td><b>Periodo del Plan:</b></td>
                          <td>{{ $historia->frecuencia }} Mes(es).</td>
                        </tr>

                        <tr>
                          <td><b>Fecha de Inicio del Plan:</b></td>
                          <td>{{ $historia->fecha_inicio }}</td>
                        </tr>

                        <tr>
                          <td><b>Fecha de Finalización del Plan:</b></td>
                          <td>{{ $historia->fecha_finalizacion }}</td>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                  <!-- /.card-body -->
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
    @endforeach
@endsection

@section('extras-js')
    <!-- DataTables -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

    <script>
    $(function () {
      $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
      });
    });
  </script>
@endsection
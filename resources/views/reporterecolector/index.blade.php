@extends('layouts.main')

@section('extras-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@inject('rutaClienteLista', 'App\RutaClienteLista')
@inject('user', 'App\User')

@section('content')

<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Reporte') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">

        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  Parametros del Reporte</b>
                </h3>                
              </div>
              
              <div class="card-body">
              <form method="GET" action="{{ route('reporte.recolector') }}" enctype="multipart/form-data">
    			    @csrf
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
		                	<label>Fecha de Inicio *</label>
		                  <input type="date" class="form-control " name="fecha_inicio" id="fecha_inicio" value="{{ date('Y-m-d', strtotime($fecha_inicio)) }}" required="">
		                </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
		                	<label>Fecha de Fin *</label>
		                  <input type="date" class="form-control " name="fecha_fin" id="fecha_fin" max="{{ date('Y-m-d',strtotime($fecha_maxima)) }}" value="{{ date('Y-m-d', strtotime($fecha_fin)) }}" required="">
		                </div>
                  </div>

                  <div class="col-md-6">
                    <input type="submit" class="btn btn-primary" value="Generar Reporte">
                  </div>
                </div>
              </form>
              </div>
              
            </div>
            <!-- /.card -->
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  Reporte del <b>{{ date('d/m/Y', strtotime($fecha_inicio)) }}</b> al <b>{{ date('d/m/Y', strtotime($fecha_fin)) }}</b>
                </h3>                
              </div>
              
              <div class="card-body">
                <table class="table table-striped">
                  <tr>
                    <td><b>Rutas Asignadas:</b> {{ $rutasAsignadas }}</td>
                    <td><b>Total de Clientes en rutas:</b> {{ $totalClientes }}</td>
                  </tr>
                  <tr>
                    <td><b>Recolecciones exitosas:</b> {{ $totalRecogidasExitosas }}</td>
                    <td><b>Recolecciones no realizadas:</b> {{ $totalRecogidasNoCompletadas }}</td>
                  </tr>
                  <tr>
                    <td><b>Monto Cobrado:</b> ${{ number_format($totalPagosRealizados) }}</td>
                    <td><b>Monto por Cobrar:</b> ${{ number_format($totalPagosPorCobrar) }}</td>
                  </tr>
                </table>
              </div>
              
            </div>
            <!-- /.card -->
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  Listado de Recolecciones Cobradas
                </h3>                
              </div>
              
              <div class="card-body">
                <table id="example1" class="table table-striped">
                <thead>
	                <tr>
	                  <th>Ruta</th>
	                  <th>Fecha</th>
	                  <th>Monto</th>
	                  <th>Cliente</th>
                    <th>Tipo de Servicio</th>
	                </tr>
	              </thead>
                <tbody>
                @php
                $cuentaRecoleccion = 0;
                foreach($recolectorRoutes as $ruta) {
                  $recoleccionesCobradas = $rutaClienteLista::where('status', '=', 'Recogida')->where('pagado', 1)->where('id_ruta', '=', $ruta->id)->get();
                  foreach($recoleccionesCobradas as $recoleccion) {
                    $cuentaRecoleccion++;
                    $cliente = $user::find($recoleccion->id_cliente);
                @endphp
                    <tr>
                      <td>{{ $ruta->titulo }}</td>
                      <td>{{ date('d/m/Y', strtotime($ruta->fecha)) }}</td>
                      <td>${{ number_format($recoleccion->pago_recolector) }}</td>
                      <td>{{ $cliente->name }}</td>
                      <td>{{ $recoleccion->tipo_servicio }}</td>
                    </tr>
                @php
                  }
                }
                @endphp
                </tbody>
                </table>
              </div>
              
            </div>
            <!-- /.card -->
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  Listado de Recolecciones por Cobrar
                </h3>                
              </div>
              
              <div class="card-body">
                <table id="example2" class="table table-striped">
                <thead>
	                <tr>
	                  <th>Ruta</th>
	                  <th>Fecha</th>
	                  <th>Monto</th>
	                  <th>Cliente</th>
                    <th>Tipo de Servicio</th>
	                </tr>
	              </thead>
                <tbody>
                @php
                $cuentaRecoleccion = 0;
                foreach($recolectorRoutes as $ruta) {
                  $recoleccionesPorCobrar = $rutaClienteLista::where('status', '=', 'Recogida')->where('pagado', 0)->where('id_ruta', '=', $ruta->id)->get();
                  foreach($recoleccionesPorCobrar as $recoleccion) {
                    $cuentaRecoleccion++;
                    $cliente = $user::find($recoleccion->id_cliente);
                @endphp
                    <tr>
                      <td>{{ $ruta->titulo }}</td>
                      <td>{{ date('d/m/Y', strtotime($ruta->fecha)) }}</td>
                      <td>${{ number_format($recoleccion->pago_recolector) }}</td>
                      <td>{{ $cliente->name }}</td>
                      <td>{{ $recoleccion->tipo_servicio }}</td>
                    </tr>
                @php
                  }
                }
                @endphp
                </tbody>
                </table>
              </div>
              
            </div>
            <!-- /.card -->
          </div>
        </div>


        

      </div>
    </section>
    <!-- /.content -->
@endsection

@section('extras-js')
    <!-- DataTables -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

    <script>
	  $(function () {
	    $("#example1").DataTable();
      $("#example2").DataTable();
	  });
	</script>
@endsection
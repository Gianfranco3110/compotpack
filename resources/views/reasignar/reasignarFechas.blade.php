@extends('layouts.main')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8" style="height:600px !important;">
      <div class="card">
        <div class="card-body">
          <div id='calendar'></div>

        </div>
      </div>
    </div>
  </div>

  <div class="row justify-content-center mt-5">
    <div class="col-md-10">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">{{ __('Listado de Festividades') }}</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <div id="loading" class="spinner-border text-primary d-none" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
          <div id="container-recoletores">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Festividad</th>
                  <th>Dia</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($festividadesParaFecha as $festividadParaFecha)
                <tr>
                  <td><input type="text" id="event-{{$festividadParaFecha->id }}" value="{{ $festividadParaFecha->event }}" name="event" placeholder="Describa festividad"></td>
                  <td><input type="date" id="date-{{$festividadParaFecha->id }}" value="{{$dia}}" onchange="onChangeDate(this, '{{$festividadParaFecha->id }}')" min="{{date('Y-m-d')}}"></td>
                  <td>
                    <div class="btn-group">
                      <button onclick="deleteDateEvent({{$festividadParaFecha->id }})" class="btn btn-primary mr-1">Eliminar</button>
                      <button onclick="updateDate({{$festividadParaFecha->id }})" class="btn btn-primary">Actualizar</button>
                      <div id="loading-update-event-{{ $festividadParaFecha->id }}" class="spinner-border ml-2 text-primary d-none" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                    </div>
                  </td>
                </tr>
                @endforeach
             </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row justify-content-center mt-5">
    <div class="col-md-10">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">{{ __('Agregar dia Festivo') }}</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <div id="container-recoletores">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Festividad</th>
                  <th>Dia</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><input type="text" id="new-event" name="event" placeholder="Describa festividad"></td>
                  <td><input type="date" id="new-event-day" name="date" value="{{$dia}}" onchange="onChangeNewDate(this)" min="{{date('Y-m-d')}}"></td>
                  <td>
                    <div class="btn-group">
                      <button id="save-new-event" onclick="setNewDate()" class="btn btn-primary">Guardar</button>
                      <div id="loading-save-new-event" class="spinner-border ml-2 text-primary d-none" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                    </div>
                  </td>
                </tr>
             </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>

    function formatoFecha(fecha, formato) {
      const mes = fecha.getMonth()+1;
      const map = {
          dd: fecha.getDate(),
          // Si el mes es menor que 10 complementamos con 0 para siempre tener 2 digitos
          mm: (mes<10) ? "0"+mes.toString(): mes,
          yyyy: fecha.getFullYear()
      }
      return formato.replace(/dd|mm|yyyy/gi, matched => map[matched]);
    }

    function updateDate(idFechaFestivividad){
      const myUrl = "{{route('ajax.updateDateEvent')}}";
      const newDate = $("#date-"+idFechaFestivividad).attr('value');
      const newEvent = document.getElementById("event-"+idFechaFestivividad).value;

      $("#loading-update-event-"+idFechaFestivividad).removeClass('d-none');

      $.ajax({
        url:myUrl,
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
        type:"POST",
        data:{
          date:newDate,
          event:newEvent,
          id:idFechaFestivividad
        },
        success:function(data){
          $("#loading-update-event-"+idFechaFestivividad).addClass("d-none");
          if(data.status){
            alert(data.mensaje);
            location.reload();
          }else{
            alert("Error al actualizar fecha:\n"+data.mensaje);
          }
        },
        error:function(data){
          alert("Error: "+data.responseJSON.message);
          console.log(data);
          $("#loading-update-event-"+idFechaFestivividad).addClass("d-none");
        }
      });

    }

    function setNewDate(){
      const myUrl = "{{route('ajax.setNewDateEvent')}}";
      const newDate = $("#new-event-day").attr('value');
      const newEvent = document.getElementById("new-event").value;

      $("#loading-save-new-event").removeClass('d-none');
      $.ajax({
        url:myUrl,
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
        type:"POST",
        data:{
          date:newDate,
          event:newEvent
        },
        success:function(data){
          $("#loading-save-new-event").addClass("d-none");
          if(data.status){
            alert(data.mensaje);
            location.reload();
          }else{
            alert("Error al guardar fecha:\n"+data.mensaje);
          }
        },
        error:function(data){
          alert("Error: "+data.responseJSON.message);
          console.log(data);
          $("#loading-save-new-event").addClass("d-none");
        }
      });

    }

    function deleteDateEvent(idFechaFestivividad){
      const myUrl = "{{route('ajax.deleteDateEvent')}}";
      $("#loading-update-event-"+idFechaFestivividad).removeClass('d-none');
      $.ajax({
        url:myUrl,
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
        type:"POST",
        data:{
          id:idFechaFestivividad
        },
        success:function(data){
          $("#loading-update-event-"+idFechaFestivividad).addClass("d-none");
          if(data.status){
            alert(data.mensaje);
            location.reload();
          }else{
            alert("Error al eliminar fecha:\n"+data.mensaje);
          }
        },
        error:function(data){
          alert("Error: "+data.responseJSON.message);
          console.log(data);
          $("#loading-update-event-"+idFechaFestivividad).addClass("d-none");
        }
      });
    }

    function dateClicked (date){
      console.log('date:', date.dateStr);
      const myUrl = "{{route('reasignar')}}/"+date.dateStr;
      window.location = myUrl;
    }

    function onChangeDate (obj, idRecolector){
      $("#cambiar-"+idRecolector).removeClass("disabled");
      $("#date-"+idRecolector).attr('value', obj.value);
    }

    function onChangeNewDate(obj){
      $("#new-event-day").attr('value', obj.value);
    }

    $(document).ready(function() {

      var calendarEl = document.getElementById('calendar');
      var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth',
        dateClick: function(info){ dateClicked(info) },
        initialDate: '{{$dia}}',
        events: [
          @foreach($diasFestivos as $diaFestivo)
          {
            title : 'Dia festivo',
            start : '{{$diaFestivo->year."-".$diaFestivo->month."-".$diaFestivo->day}}'
          },
          @endforeach
        ]
      });
      calendar.render();

      $(function () {
        $("#example1").DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
          }
        });
      });

    });

  </script>

</div>
@endsection

@section('extras-js')
    <!-- DataTables -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <!-- calendar -->
    <!--script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script-->
    <script src="{{ asset('fullcalendar-5.6.0/lib/main.js')}}"></script>
@endsection

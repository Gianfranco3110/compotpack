@extends('layouts.main')

@section('extras-css')

@endsection

@section('content')

<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Mi Ruta') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  Ruta de hoy {{ date('d/m/Y') }} - <b>Nota:</b> Las distancias se calculan con punto de partida la sede CompostPack.
                </h3>

                <!--div class="card-tools">
                  <ul class="pagination pagination-sm">
                    <li class="page-item"><a href="#" class="page-link">«</a></li>
                    <li class="page-item"><a href="#" class="page-link">1</a></li>
                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                    <li class="page-item"><a href="#" class="page-link">3</a></li>
                    <li class="page-item"><a href="#" class="page-link">»</a></li>
                  </ul>
                </div-->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <ul class="todo-list" data-widget="todo-list">

                @empty($rutaClientes)
                <h4>No posees rutas asignadas para hoy.</h4>
                @endempty
                @foreach($rutaClientes as $key =>  $cliente)
                  <li style="margin-bottom:10px;">
                    <!-- drag handle -->
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <!-- todo text -->
                    <span class="text">{{ $key+1 }} - {{ $cliente->name}} </span>
                    <!-- Emphasis label -->

                    <table class="table table-striped">
                    <tr>
                      <td><b>Estado: </b>
                      <span class="badge badge-success" id="statusTable{{ $datosInfo[$cliente->id]->id }}">{{ $datosInfo[$cliente->id]->status }}</span></td>
                    </tr>
                    <tr>
                      <td><b>Distancia:</b>
                      <small class="badge badge-info"><i class="fa fa-truck"></i> {{ round($cliente->distance, 2) }} Kms.</small></td>
                    </tr>
                    <tr>
                      <td><b>Tipo de Servicio:</b>
                      <small class="badge badge-info"><i class="fa fa-box"></i> {{ $datosInfo[$cliente->id]->tipo_servicio }}.</small></td>
                    </tr>
                    <tr>
                      <td>
                        <a class="btn btn-primary" href="javascript:void(0)" title="Detalles del cliente" data-toggle="modal" data-target="#clienteModal{{ $datosInfo[$cliente->id]->id }}">Detalles de Recolección</a>
                      </td>
                    </tr>
                  </table>

                    <!-- General tools such as edit or delete-->
                    <!--div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div-->
                  </li>
                @endforeach
                </ul>
              </div>
              <!-- /.card-body -->
              <!--div class="card-footer clearfix">
                <button type="button" class="btn btn-info float-right"><i class="fas fa-plus"></i> Add item</button>
              </div-->
            </div>
            <!-- /.card -->
          </div>
        </div>

        @php
        if(!empty($rutaClientes)) {
        @endphp
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Mapa
                </h3>
              </div>

              <div class="card-body">
              	<div id="map" style="min-height: 400px; width: 100%;">
                </div>
              </div>
            </div>
          </div>
    	</div>
    	@php
    	}
    	@endphp
      </div>
    </section>
    <!-- /.content -->

    @foreach($rutaClientes as $key =>  $cliente)
    <!-- Modal -->
	<div class="modal fade" id="clienteModal{{ $datosInfo[$cliente->id]->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Detalles del Cliente</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<meta name="csrf-token" content="{{ csrf_token() }}" />
	      	<div class="row">
	      		<div class="col-md-12">
	      			<div class="card bg-light">
		                <div class="card-body pt-0">
		                  <div class="row">
		                    <div class="col-9">
		                      <h2 class="lead"><b>{{ $cliente->name }}</b></h2>
		                      <ul class="ml-4 mb-0 fa-ul text-muted">
		                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Dirección: {{ $cliente->direccion }}</li>
		                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Teléfono: {{ $cliente->telefono }}</li>
		                      </ul>

		                      <br>
		                      <div class="row">
		                      	<div class="col-md-8">
				                      <div class="form-group">
				                        <label>Estado de la recolección</label>

				                        <select class="form-control" id="estado{{ $datosInfo[$cliente->id]->id }}">
				                        @foreach($estados as $estado)
                                  @if(trim($datosInfo[$cliente->id]->status) == trim($estado->titulo))
				                            <option selected value='{{ $estado->id }}'>{{ $estado->titulo }}</option>
                                  @else
                                    <option value='{{ $estado->id }}'>{{ $estado->titulo }}</option>
                                  @endif
				                         @endforeach
				                        </select>
				                      </div>

                              <div class="form-group">
				                        <label>Comentarios</label>
				                        <textarea class="form-control" id="comentario{{ $datosInfo[$cliente->id]->id }}">{{ $datosInfo[$cliente->id]->comentarios }}</textarea>
				                      </div>
				                  </div>
				              </div>

		                    </div>
		                    <div class="col-3 text-center">
		                      <img src="{{ asset('uploads/usuarios/'.$cliente->image) }}" alt="" class="img-circle img-fluid">
		                    </div>
		                  </div>
		                </div>
		                <div class="card-footer">
		                  <div class="text-right">
		                    <a href="https://wa.me/{{ $cliente->telefono }}"  target="_blank" class="btn btn-sm bg-teal">
		                      <i class="fab fa-whatsapp"></i> Whatsapp
		                    </a>
		                    <a href="tel:{{ $cliente->telefono }}" class="btn btn-sm btn-primary">
		                      <i class="fas fa-phone"></i> Llamar
		                    </a>

                        @if($datosInfo[$cliente->id]->status != 'Recogida')
		                    <a href="javascript:void(0);" id="saveRecoleccion" data-id="{{ $datosInfo[$cliente->id]->id }}" class="saveRecoleccion btn btn-sm btn-primary">
		                      <i class="fas fa-save"></i> Guardar
		                    </a>
                        @endif
		                  </div>
		                </div>
		              </div>
	      		</div>
	      	</div>
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="card bg-light">
	        			<div class="card-header">
	        				Ubicación en Mapa
	        			</div>
		                <div class="card-body pt-0">
	        				<div id="map{{ $key }}" style="min-height: 400px; width: 100%;"></div>
	        			</div>
	        		</div>
	        	</div>
	        </div>
	      </div>
	      <!--div class="modal-footer">
	      	<button type="button" class="btn btn-primary" id="btnUpdateRecolector">Asignar</button>
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
	      </div-->
	    </div>
	  </div>
	</div>
	@endforeach
@endsection

@section('extras-js')
	<!-- jQuery Maps -->
    <script type="text/javascript" src="{{ asset('/maps/gmaps.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCF9LXYgTkSgr5QOu9OJelPqvJB-1Ij9BE" type="text/javascript"></script>

    <script type="text/javascript">

    	var latLocation = {{ 4.60971 }};
      	var lngLocation = {{ -74.08175 }};

    	var map = new GMaps({
          el: '#map',
          lat: latLocation,
          lng: lngLocation,
          zoomControl : true,
          zoomControlOpt: {
              style : 'SMALL',
              position: 'TOP_LEFT'
          },
          zoom : 17,
          panControl : false,
          streetViewControl : false,
          mapTypeControl: false,
          overviewMapControl: false
        });

        @foreach($rutaClientes as $key =>  $cliente)

        var map{{ $key }} = new GMaps({
          el: '#map{{ $key }}',
          lat: {{ $cliente->latitud }},
          lng: {{ $cliente->longitud }},
          zoomControl : true,
          zoomControlOpt: {
              style : 'SMALL',
              position: 'TOP_LEFT'
          },
          zoom : 17,
          panControl : false,
          streetViewControl : false,
          mapTypeControl: false,
          overviewMapControl: false
        });

        map.addMarker({
          lat: {{ $cliente->latitud }},
          lng: {{ $cliente->longitud }},
          title: '{{ $cliente->name }}',
          click: function(e) {
            //alert('You clicked in this marker');
          },
          icon: {
              url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
          },
          title: '{{ $cliente->name }}' ,
          infoWindow: {
              content: '{{ $cliente->name }} <br> Dirección: {{ $cliente->direccion }}'
          }
        });

		map{{ $key }}.addMarker({
		  lat: {{ $cliente->latitud }},
		  lng: {{ $cliente->longitud }},
		  title: '{{ $cliente->name }}',
		  click: function(e) {
		    //alert('You clicked in this marker');
		  },
      icon: {
          url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
      },
      title: '{{ $cliente->name }}' ,
      infoWindow: {
          content: '{{ $cliente->name }} <br> Dirección: {{ $cliente->direccion }}'
      }
		});

		//map{{ $key }}.fitZoom();
		map{{ $key }}.setZoom(17);

		@endforeach

		map.fitZoom();

    $('.saveRecoleccion').click(function() {
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var idClientRoute = $(this).attr('data-id');
      console.log("comentario "+$('#comentario'+idClientRoute).val()+" Estado "+$('#estado'+idClientRoute).val());

		  	$.ajax({
                url: '{{ route('ajax.saveRecolectionStatus') }}',
                type: 'POST',
                data: {
                	_token : CSRF_TOKEN,
                	comentarios : $('#comentario'+idClientRoute).val(),
                	status : $('#estado'+idClientRoute).val(),
                	recolectionId : idClientRoute
                },
                dataType: 'JSON',
                success: function (data) {
                    //console.log("TERMINO "+rutaId+" "+dia+" "+$('#recolectorSelect').val());
                    $('#statusTable'+idClientRoute).empty().append(data.status);
                    $('#clienteModal'+idClientRoute).modal('hide');
                    alert(data.msg);
                }
            });
		});

    </script>


@endsection

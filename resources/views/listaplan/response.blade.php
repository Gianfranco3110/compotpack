@extends('layouts.main')

@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Respuesta de la transacción') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Detalles de la transacción') }}</h3>
              </div>
              <div class="card-body">

				<div class='container'>
					<div class='row' style='margin-top:20px'>
						<div class='col-lg-8 col-lg-offset-2 '>
						<h4 style='text-align:left'> Respuesta de la Transacción </h4>
						<hr >
						</div >
						<div class='col-lg-8 col-lg-offset-2' >
							<div class='table-responsive'>
								<table class='table table-bordered'>
								<tbody>
								<tr>
								<td>Referencia </td>
								<td id='referencia'> </td>
								</tr>
								<tr>
								<td class='bold'> Fecha </td>
								<td id='fecha' class=''>  </td>
								</tr>
								<tr>
								<td> Respuesta </td>
								<td id='respuesta'>  </td>
								</tr>
								<tr>
								<td> Motivo </td>
								<td id='motivo'>  </td>
								</tr>
								<tr>
								<td class='bold'> Banco </td>
								<td class='' id='banco'></td>
								</tr>
								<tr>
								<td class='bold'> Recibo </td>
								<td id='recibo'>  </td>
								</tr>
								<tr>
								<td class='bold'> Total </td>
								<td class='' id='total'></td>
								</td>
								</tr>
								</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<div class='row'>
					<div class='container'>
						<div class='col-lg-8 col-lg-offset-2'>
							<img src='https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/epayco/pagos_procesados_por_epayco_260px.png' style='margin-top:10px; float:left'>  
							<img src='https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/epayco/credibancologo.png'height='40px' style='margin-top:10px; float:right'>
						</div>
					</div>
				</div>

				<div class='row'>
					<div class='col-md-12'>
						<a href="{{ route('listado.planes') }}" class='btn btn-danger'>Volver a Planes</a>
					</div>
				</div>

				</div>
			</div>
		</div>
	</div>
</div>
</section>
@endsection

@section('extras-js')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js'> </script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'> </script>
<script>
function getQueryParam(param) {
location.search.substr(1)
.split('&')
.some(function(item) { // returns first occurence and stops
return item.split('=')[0] == param && (param = item.split('=')[1])
})
return param
}
$(document).ready(function() {
 //llave publica del comercio
 //Referencia de payco que viene por url
var ref_payco = getQueryParam('ref_payco');
 //Url Rest Metodo get, se pasa la llave y la ref_payco como paremetro
 var urlapp = 'https://secure.epayco.co/validation/v1/reference/' + ref_payco;
$.get(urlapp, function(response) {
if (response.success) {
if (response.data.x_cod_response == 1) {
//Codigo personalizado
//alert('Transaccion Aprobada');
//console.log('transacción aceptada');
} 
//Transaccion Rechazada
if (response.data.x_cod_response == 2) {
//console.log('transacción rechazada');
}
//Transaccion Pendiente
if (response.data.x_cod_response == 3) {
//console.log('transacción pendiente');
} 
//Transaccion Fallida
if (response.data.x_cod_response == 4) {
//console.log('transacción fallida');
}
$('#fecha').html(response.data.x_transaction_date);
$('#respuesta').html(response.data.x_response);
$('#referencia').text(response.data.x_id_invoice);
$('#motivo').text(response.data.x_response_reason_text);
$('#recibo').text(response.data.x_transaction_id);
$('#banco').text(response.data.x_bank_name);
$('#autorizacion').text(response.data.x_approval_code);
$('#total').text(response.data.x_amount + ' ' + response.data.x_currency_code);
} else {
alert('Error consultando la información');
}
});
});
</script>
@endsection
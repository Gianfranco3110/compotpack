@extends('layouts.main')

@section('extras-css')
@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Lista de Planes') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Planes a un solo pago') }}</h3>
              </div>
              <div class="card-body">   

              <div class="row">            	            

              	@foreach($planesOnce as $plan)
              	<div class="col-md-3">

		            <!-- Profile Image -->
		            <div class="card card-primary card-outline">
		              <div class="card-body box-profile">

		                <h3 class="profile-username text-center">{{ $plan->titulo }}</h3>

		                <p class="text-muted text-center">${{ number_format($plan->precio, 0, '.', ',') }}</p>

		                <ul class="list-group list-group-unbordered mb-3">
		                  <li class="list-group-item">
		                    <b>Servicio por</b> <a class="float-right">{{ $plan->frecuencia_cobro }} Mes(es).</a>
		                  </li>
		                  <li class="list-group-item">
		                    <b>Cobro Recurrente</b> <a class="float-right">No</a>
		                  </li>
		                  <li class="list-group-item">
		                    {{ $plan->descripcion }}
		                  </li>
		                </ul>

		                <form>
					        <script
					            src="https://checkout.epayco.co/checkout.js"
					            class="epayco-button"
					            data-epayco-key="787c4340a28c7c4cbfa029c0f948db33"
					            data-epayco-amount="{{ $plan->precio }}"
					            data-epayco-name="{{ $plan->titulo }}"
					            data-epayco-description="{{ $plan->titulo }}. {{ $plan->frecuencia_cobro }} Mes(es)."
					            data-epayco-currency="cop"
					            data-epayco-country="co"
					            data-epayco-test="true"
					            data-epayco-external="true"
					            data-epayco-response="{{ route('pago.response') }}"
					            data-epayco-confirmation="{{ route('pago.confirm') }}"
					            data-epayco-invoice="{{ Auth::user()->id.'_'.$plan->id.'_'.time() }}">
					        </script>
					    </form>

		              </div>
		              <!-- /.card-body -->
		            </div>
		            <!-- /.card -->
		          </div>
              	@endforeach
              	</div>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Planes Recurrentes') }}</h3>
              </div>
              <div class="card-body">   

              <div class="row">            	            

              	@foreach($planesRecurrente as $plan)
              	<div class="col-md-3">

		            <!-- Profile Image -->
		            <div class="card card-primary card-outline">
		              <div class="card-body box-profile">

		                <h3 class="profile-username text-center">{{ $plan->titulo }}</h3>

		                <p class="text-muted text-center">${{ number_format($plan->precio, 0, '.', ',') }}</p>

		                <ul class="list-group list-group-unbordered mb-3">
		                  <li class="list-group-item">
		                    <b>Servicio por</b> <a class="float-right">{{ $plan->frecuencia_cobro }} Mes(es).</a>
		                  </li>
		                  <li class="list-group-item">
		                    <b>Cobro Recurrente</b> <a class="float-right">No</a>
		                  </li>
		                  <li class="list-group-item">
		                    {{ $plan->descripcion }}
		                  </li>
		                </ul>
		                
	                	<form>
					        <script
					            src="https://checkout.epayco.co/checkout.js"
					            class="epayco-button"
					            data-epayco-key="491d6a0b6e992cf924edd8d3d088aff1"
					            data-epayco-amount="{{ $plan->precio }}"
					            data-epayco-name="{{ $plan->titulo }}"
					            data-epayco-description="{{ $plan->titulo }}. {{ $plan->frecuencia_cobro }} Mes(es)."
					            data-epayco-currency="cop"
					            data-epayco-country="co"
					            data-epayco-test="true"
					            data-epayco-external="true"
					            data-epayco-response="https://ejemplo.com/respuesta.html"
					            data-epayco-confirmation="https://ejemplo.com/confirmacion">
					        </script>
					    </form>
		                
		              </div>
		              <!-- /.card-body -->
		            </div>
		            <!-- /.card -->
		          </div>
              	@endforeach
              	</div>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection

@section('extras-js')
@endsection
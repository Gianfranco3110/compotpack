<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/styles.css') }}">

  <script src="{{ asset('js/app.js')}}"></script>

  <title>{{ config('app.name', 'CompostPack') }}</title>

</head>

<body>

  <div class="animation" id="animation">
    @if(Route::currentRouteName()=='frontend.account')
    <div class="box p-3">
      <h1 class="text-center d-block">Bienvenido</h1>
      <img src="{{ asset('img/logo-3.png') }}" class="logo">
    </div>
    @endif
    @if(Route::currentRouteName()=='frontend.planes' || Route::current()->hasParameter('planId'))
    <div class="box p-3">
      <img src="{{ asset('img/logo-1.png') }}" class="logo">
    </div>
    @endif
    @if(Route::currentRouteName()=='frontend.pago')
    <div class="box p-3">
      <h2 class="text-center text-white d-block">"¡Gracias! <br> Al WhatsApp y correo te escribiremos"</h2>
      <img src="{{ asset('img/planeta.png') }}" class="logo-planet d-block mx-auto">
    </div>
    @endif
    @if(Route::currentRouteName()=='frontend.tarjeta' || Route::currentRouteName()=='frontend.home')
    <div class="box">
      <h2 class="text-center d-block">Bienvenido</h2>
      <img src="{{ asset('img/planeta.png') }}" class="logo-planet">
    </div>
    @endif
    @if(Route::currentRouteName()=='frontend.index')
    <img src="{{ asset('img/light.png') }}" class="light">
    <div class="box p-3">
        <img src="{{ asset('img/logo-1.png') }}" class="logo">
    </div>
    @endif

  </div>

  <!--
    Si el cliente esta autenticado mostrados el menus de navegacion para todas
    las rutas excluyendo las que se definen abajo
  -->
  @if(Auth::check())
    @if(Route::currentRouteName()!='frontend.index' && Route::currentRouteName()!='frontend.home')
      <header class="header d-flex align-items-center justify-content-center">
          <a href="{{route('frontend.home')}}"><img src="{{ asset('img/logo-2.png') }}" class="logo2 d-block"></a>
          <img src="{{ asset('img/icono-menu.png') }}" class="icon-menu d-block" id="btn-menu">
      </header>

      <nav class="nav d-flex flex-column text-center" id="nav">
        <a href="{{route('frontend.account')}}">Mi cuenta</a>
        <a href="{{route('frontend.planes')}}">Planes</a>
        <a href="{{route('frontend.pago')}}">Historial pagos</a>
        <a href="{{route('frontend.tarjeta')}}">Targeta Registrada</a>
        <a href="{{route('frontend.logout')}}">Salir</a>
      </nav>
    @endif
  @endif

  <main class="home">
    @yield('content')
  </main>

  <script src="{{ asset('js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('js/scrollreveal.js')}}"></script>
  <script src="{{ asset('js/main.js')}}"></script>
  <script src="{{ asset('js/menu.js')}}"></script>

</body>

</html>

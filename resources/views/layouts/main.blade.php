<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!--Other Resources-->
  @yield('extras-css')
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- Calendar-->
  <!--link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' /-->
  <link rel='stylesheet' href="{{ asset('fullcalendar-5.6.0/lib/main.css')}}"/>
  <!-- jQuery -->
  <!-- NOTA:
    jQuery esta aqui para que sea cargado previamente y poder lograr la configuración
    del calendario en la seccion de Reasignar Fechas
  -->
  <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/jquery/jquery.js')}}"></script>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-cog"></i> {{ __('Opciones') }}
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <i class="fa fa-user"></i> {{ __('Perfil') }}
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item" data-toggle="dropdown" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out-alt"></i> {{ __('Salir') }}
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </a>
        </div>
      </li>

    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/dist/img/AdminLTELogo.png') }}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">{{ config('app.name', 'Laravel') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('uploads/usuarios/'.Auth::user()->image) }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        @switch(Auth::user()->roles()->first()->id)
          @case(1) {{--ADMIN--}}
          <li class="nav-item">
            <a href="{{ route('cliente.index') }}" class="nav-link {{ (request()->is('cliente*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                {{ __('Clientes') }}
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('recolector.index') }}" class="nav-link {{ (request()->is('recolector*')) || (request()->is('pagosrecolector*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-truck"></i>
              <p>
                {{ __('Recolectores') }}
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('plan.index') }}" class="nav-link {{ (request()->is('plan*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-money-bill-alt"></i>
              <p>
                {{ __('Planes') }}
              </p>
            </a>
          </li>

          <!--li class="nav-item">
            <a href="{{ route('zona.index') }}" class="nav-link {{ (request()->is('zona*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-globe"></i>
              <p>
                {{ __('Zonas') }}
              </p>
            </a>
          </li-->

          <li class="nav-item">
            <a href="{{ route('mapa.index') }}" class="nav-link {{ (request()->is('mapa*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-globe"></i>
              <p>
                {{ __('Zonas y Rutas Config.') }}
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('ruta.index', ['daySelected' => 0, 'weekSelected' => 0]) }}" class="nav-link {{ (request()->is('ruta*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-globe"></i>
              <p>
                {{ __('Rutas') }}
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('reasignar', ['date' => date('Y-m-d')])}}" class="nav-link {{ (request()->is('reasignar*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-calendar"></i>
              <p>
                {{ __('Reasignar Fechas') }}
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('historial.clientes') }}" class="nav-link {{ (request()->is('historial/clientes*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-list"></i>
              <p>
                {{ __('Hist. Pagos Suscripciones') }}
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('inventarios.index') }}" class="nav-link {{ (request()->is('inventarios*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-box"></i>
              <p>
                {{ __('Inventarios') }}
              </p>
            </a>
          </li>


          <li class="nav-item">
            <a href="{{ route('reporte.general') }}" class="nav-link {{ (request()->is('reportegeneral*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                {{ __('Reportes') }}
              </p>
            </a>
          </li>
          @break
          @case(2) {{--RECOLECTOR--}}
          <li class="nav-item">
            <a href="{{ route('perfil.recolector') }}" class="nav-link {{ (request()->is('perfil*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-user"></i>
              <p>
                {{ __('Mi perfil') }}
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('ruta.recolector') }}" class="nav-link {{ (request()->is('ruta*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-globe"></i>
              <p>
                {{ __('Mi Ruta') }}
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('reporte.recolector') }}" class="nav-link {{ (request()->is('reporterecolector*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                {{ __('Reportes') }}
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('pagos.historial') }}" class="nav-link {{ (request()->is('historial*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-list"></i>
              <p>
                {{ __('Hist. Pagos') }}
              </p>
            </a>
          </li>

          @break

          @case(3) {{--CLIENTE--}}
            <li class="nav-item">
              <a href="{{ route('perfil.cliente') }}" class="nav-link {{ (request()->is('perfil*')) ? 'active' : '' }}">
                <i class="nav-icon fas fa-user"></i>
                <p>
                  {{ __('Mi perfil') }}
                </p>
              </a>
            </li>

            @if(Auth::user()->perfil_complete != 0)
            <li class="nav-item">
              <a href="{{ route('listado.planes') }}" class="nav-link {{ (request()->is('listado/planes*')) ? 'active' : '' }}">
                <i class="nav-icon fas fa-list"></i>
                <p>
                  {{ __('Planes ') }}
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a href="{{ route('historial.cliente') }}" class="nav-link {{ (request()->is('historial/cliente*')) ? 'active' : '' }}">
                <i class="nav-icon fas fa-list"></i>
                <p>
                  {{ __('Hist. Pagos') }}
                </p>
              </a>
            </li>
            @endif
          @break
        @endswitch

          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                {{ __('Salir') }}
              </p>
            </a>
          </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1
    </div>
    <strong>Copyright &copy; 2020 <a href="#">{{ config('app.name', 'Laravel') }}</a>.</strong> Todos los derechos reservados.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- main.js Para utilizar funcion de validacionNumericos -->
<script src="{{ asset('js/main.js')}}"></script>

<!-- Bootstrap 4 -->
<script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!--Other Resources-->
@yield('extras-js')
<!-- AdminLTE App -->
<script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/dist/js/demo.js')}}"></script>



</body>
</html>

@extends('layouts.main')

@section('extras-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@inject('rutaClienteLista', 'App\RutaClienteLista')
@inject('user', 'App\User')

@section('content')

<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Zona de pagos de recolector')." - ".$recolector->name }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">

        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  Datos del Pago</b>
                </h3>                
              </div>
              
              <div class="card-body">

              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong>{{ $message }}</strong>
                </div>
                @endif

              <table class="table table-stripe">
                <tr>
                    <td>
                        <b>Carreras por pagar:</b> {{ $cuentaRutas }}
                    </td>
                    <td>
                        <b>Monto a pagar:</b> ${{ number_format($montoTotal) }}
                    </td>
                <tr>
              </table>
              <form method="GET" action="{{ route('pagos.saveHistory') }}" enctype="multipart/form-data">
    			    @csrf
                    <input type="hidden" name="id_recolector" value="{{ $recolector->id }}">
                    @if($cuentaRutas != 0)
                    <input type="submit" class="btn btn-primary" value="Guardar Pago">
                    @else
                    <p><b>Nota:</b> Este recolector no presenta carrares para pagar.</p>
                    @endif
              </form>
              </div>
              
            </div>
            <!-- /.card -->
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  Historial de Pagos Realizados
                </h3>                
              </div>
              
              <div class="card-body">
                <table id="example1" class="table table-striped">
                <thead>
	                <tr>
	                  <th>Id</th>
	                  <th>Fecha</th>
                      <th>Carreras</th>	                  
	                  <th>Monto</th>
	                </tr>
	              </thead>
                <tbody>
                @foreach($historico as $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ date('d/m/Y', strtotime($value->fecha)) }}</td>
                        <td>{{ $value->carreras_pagadas }}</td>
                        <td>${{ number_format($value->monto )}}</td>
                    </tr>
                @endforeach
               
                </tbody>
                </table>
              </div>
              
            </div>
            <!-- /.card -->
          </div>
        </div>        

      </div>
    </section>
    <!-- /.content -->
@endsection

@section('extras-js')
    <!-- DataTables -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

    <script>
	  $(function () {
	    $("#example1").DataTable();
      $("#example2").DataTable();
	  });
	</script>
@endsection
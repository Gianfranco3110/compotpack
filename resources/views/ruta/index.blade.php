
@extends('layouts.main')


@section('extras-css')
@endsection

@section('content')
@inject('rutaLista','App\RutaLista')
@inject('configuration','App\Configuration')

<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Administrador de Rutas') }}</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Semana del ') }} <b>{{ date('d/m/Y', strtotime($arraySemanas[$weekSelected]['Mon'])) }}</b> al <b>{{ date('d/m/Y', strtotime($arraySemanas[$weekSelected]['Fri'])) }}</b></h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">


	          <div class="row">
	          	<div class="col-md-6">
	          		<div class="form-group">
                        <label>Selector de semana</label>
                        <select class="form-control" id="weekSelector">
	                        @foreach ($arraySemanas as $key => $semana)
							    <option value="{{ $key }}" {{ $key==$weekSelected ? 'selected' : '' }} >{{ date('d/m/Y', strtotime($semana['Mon'])) . ' al ' . date('d/m/Y', strtotime($semana['Fri'])) }}</option>
							@endforeach
                        </select>
                    </div>
                    <input type="button" id="sendWeek" class="btn btn-primary" value="Ir a Semana">
	          	</div>
	          	<div class="col-md-6">
	          	</div>


	          	</div>
	          	<br>
	          	<div class="row">
	          	<div class="col-md-12" align="center">
	          		<div class="btn-group btn-group-toggle" data-toggle="buttons" >
	          		@php
	          			$fecha = date('Y-m-d', strtotime($arraySemanas[$weekSelected]['Mon']));
	          			$fechaFinal = date('Y-m-d', strtotime($arraySemanas[$weekSelected]['Fri']));
	          			$fechaFinal = date("Y-m-d",strtotime($fechaFinal."+ 1 day"));

	          			do {
	          			$dayToTranlate = date('D', strtotime($fecha));
	          		@endphp
	          			<label class="btn btn-secondary {{ $daySelected == $fecha ? 'active': '' }}" style="cursor:pointer;">
		                	<input type="radio" name="options" value="{{ $fecha }}" class="daySelected" name="daySelected" >{{ $semanaTranslate[$dayToTranlate] }} <br> {{ date('d/m/Y', strtotime($fecha)) }}
		                </label>
	          		@php
	          				$fecha = date("Y-m-d",strtotime($fecha."+ 1 day"));

	          			} while ($fecha != $fechaFinal);
	          		@endphp

	                </div>
	          	</div>
	          </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Rutas programadas para el día <b>{{$semanaTranslate[$dayAcron] }} {{ date('d/m/Y', strtotime($daySelected)) }}</b></h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>

              <div class="card-body">
      				@php
      				    $cuentaRutas = 0;
      				    $capacidadRuta = $configuration::find(1)->value;
              @endphp
      				@if(!$flagRutasEnDb)
      					<h4><b style="color:red;">Nota: </b> Las rutas mostradas a continuación son dinámicas y pueden variar al día de la fecha de ejecución.</h4>
      				@endif
              @if(sizeof($rutas)==0)

        					<h4><b style="color:red;">Nota: </b> No hay rutas disponibles para este dia</h4>

              @endif

              	@foreach($rutas as $ruta)
		            <div class="timeline zonaIdDiv{{ $cuentaRutas }} zonaIdDiv">
		              <div class="time-label">
		                <span class="bg-red">{{ $ruta['titulo'] }}</span>
		              </div>
		              @php
	              		$recolectorInfo = $flagRutasEnDb ? $rutaLista::getRouteAsignRecolector($ruta['id_recolector']):"n/a";
		              @endphp
		              <div>
		                <i class="fas fa-truck bg-blue"></i>
		                <div class="timeline-item">
		                  <!--span class="time"><i class="fas fa-clock"></i> 12:05</span-->
						  @php
						  	if($flagRutasEnDb){
								$clientesEnRutados = $rutaLista::relatedClientsToRoute($ruta['id'])->count();
							}
							else{
								$clientesEnRutados = count($ruta['listadoClientes']);
							}
							$flagRutaFull = $clientesEnRutados >= $capacidadRuta ? true : false;

						  @endphp
		                  <h3 class="timeline-header bg-orange"><b>Detalles de la {{ $ruta['titulo'] }} {{ $flagRutaFull ? '- Ruta Full': ' - Ruta con espacio disponible'}}</b></h3>

		                  <div class="timeline-body">

							<table class="table table-striped" style="max-width: 450px;">
								<tr>
									<td><b>Recolector asignado:</b></td>
									<td><span id="nameRecolector{{ $flagRutasEnDb ? $ruta['id'] : 'n/a' }}">{{  $flagRutasEnDb ? $recolectorInfo->name : 'Sin Asignar' }}</span></td>
								</tr>
								<tr>
									<td><b>Total de Clientes:</b></td>
									<td>{{ $clientesEnRutados }}</td>
								</tr>
                <tr>
									<td><b>Capacidad Clientes por Ruta:</b></td>
									<td>{{ $capacidadClientesPorRuta }}</td>
								</tr>
								<tr>
									<td><b>Beneficio total de Ruta:</b></td>
									<td>$ {{ $flagRutasEnDb ? number_format($ruta['pago_recolector']) : number_format($ruta['pagoRecolector']) }}</td>
								</tr>
								@if($flagRutasEnDb && $daySelected < date('Y-m-d'))
									@php
										$totalRecolectorReal = 0;
										foreach($rutaLista::relatedClientsToRoute($ruta['id']) as $parada) {
											if($parada->status == 'Recogida') {
                        if($parada->aprobada ==1 ){
												  $totalRecolectorReal += $parada->pago_recolector;
                        }
											}
										}
									@endphp
									<tr>

										<td><b>Beneficio obtenido:</b></td>
										<td>$ {{ number_format($totalRecolectorReal) }}</td>
									</tr>
								@endif

								@if ($flagRutasEnDb)
									<tr>
										<td><b>Inventario:</b></td>
										<td>{{ $ruta['inventario'] }}.</td>
									</tr>
								@endif

							</table>
		                    <br>
							@if ($flagRutasEnDb)
								@if($ruta['fecha'] > date('Y-m-d'))
									<button class='btn btn-secondary' data-ruta="{{ $ruta['id'] }}" data-dia='{{ $daySelected }}' data-recolector="{{ $ruta['id_recolector'] }}" data-toggle="modal" data-target="#recolectorModal">Modificar Recolector</button>

									@if (!$flagRutaFull)
									<button class='btn btn-secondary' data-ruta="{{ $ruta['id'] }}" data-toggle="modal" data-target="#novedadModal">Agregar Cliente Novedad</button>
									<button class='btn btn-secondary' data-ruta="{{ $ruta['id'] }}" data-toggle="modal" data-target="#diaFestivoModal">Agregar Cliente por Dia festivo</button>
									@endif

									<br>
								@endif
							@endif
                    <a href="javascript:void(0)" class="viewRoute" data-id="{{ $flagRutasEnDb ? $ruta['id'] : $cuentaRutas }}" data-show='false'>Ver lista de Clientes <i class="fas fa-plus"></i></a>
                  </div>
                </div>
              </div>

					  @if ($flagRutasEnDb)
						@foreach($rutaLista::relatedClientsToRoute($ruta['id']) as $parada)
							<div class="user{{ $ruta['id']}}" style="display:none;">
								<i class="fas fa-user bg-green"></i>
								<div class="timeline-item">
								<!--span class="time"><i class="fas fa-clock"></i> 5 mins ago</span-->
								<h3 class="timeline-header no-border">{{ $rutaLista::relatedClientsInfo($parada->id_cliente)->name }}
                  <br><b>Estado de recolección:</b>
                    <span class="badge @if($parada->status == 'Recogida') badge-success @else badge-info @endif">{{ $parada->status }}</span>
                  <br><b>Tipo de Servicio:</b> {{ $parada->tipo_servicio }}

                  @if($parada->aprobada==0 && $daySelected == date('Y-m-d'))
                    <br>
                    <br>
                    @if(date('H:i') == '23:00')
                      <button class='btn btn-secondary mt-2' onclick='validarEjecucion({{$parada->id}})'>Validar ejecucion</button>
                    @else
                      <span><strong>Después de las 6:00pm podrá validar la ejecución.</strong></span>
                    @endif
                  @elseif($parada->aprobada==0 && date('Y-m-d') > $daySelected)
                    <br>
                    <br>
                    <button class='btn btn-secondary mt-2' onclick='validarEjecucion({{$parada->id}})'>Validar ejecucion</button>
                  @else
                  <br>
                  <br>
                  <span><strong>Al final del día después de las 6:00pm podrá validar la ejecución.</strong></span>
                  @endif
                </h3>
								</div>
							</div>
						@endforeach
					  @else
					  	@foreach($ruta['listadoClientes'] as $cliente)
						  <div class="user{{ $cuentaRutas }}" style="display:none;">
								<i class="fas fa-user bg-green"></i>
								<div class="timeline-item">
								<!--span class="time"><i class="fas fa-clock"></i> 5 mins ago</span-->
								<h3 class="timeline-header no-border">{{ $cliente['nombre'] }}</h3>
								<h4 class="timeline-header no-border"><strong>Zona:</strong> {{ $cliente['zona'] }}</h3>
									<h4 class="timeline-header no-border"><strong>Tipo de recolecta:</strong> {{ $cliente['tipo'] }} </h3>
								</div>
							</div>
						@endforeach
					  @endif

		              <div>
		                <i class="fas fa-clock bg-gray"></i>
		              </div>
		            </div>
					@php
						$cuentaRutas++;
					@endphp
              	@endforeach
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->

    <!-- Modal -->
	<div class="modal fade" id="recolectorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Asignación de recolector</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<meta name="csrf-token" content="{{ csrf_token() }}" />
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
                @if (count($recolectores) != 0)
                  <label>Recolectores</label>
                  <select class="form-control" id="recolectorSelect">
    	        		@foreach($recolectores as $recolector)
    	        			<option value='{{ $recolector->id }}'>{{ $recolector->name }}</option>
    	        		@endforeach
                @else
        					<p>Por el momento no hay recolectores disponibles para este dia.</p>
        				@endif
	        			</select>
              </div>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
          @if (count($recolectores) != 0)
	      	  <button type="button" class="btn btn-primary" id="btnUpdateRecolector">Asignar</button>
          @endif
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Novedad Modal -->
	<div class="modal fade" id="novedadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Asignación de cliente novedad a la ruta</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<meta name="csrf-token" content="{{ csrf_token() }}" />
	        <div class="row">
	        	<div class="col-md-12">
    				@if ($clientesNovedad->count() != 0)
    	        <div class="form-group">
                <label>Listado de Clientes Novedad</label>
                <select class="form-control" id="novedadSelect">
    						@foreach($clientesNovedad as $cliente)
    							<option value='{{ $cliente->id }}'>{{ $cliente->name }}</option>
    						@endforeach
    	        	</select>
              </div>
    				@else
    					<p>Por el momento no hay clientes novedad.</p>
    				@endif
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
		  @if ($clientesNovedad->count() != 0)
	      	<button type="button" class="btn btn-primary" id="btnUpdateNovedad">Asignar</button>
		  @endif
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>

  <!-- Por Dia festivo Modal -->
  <div class="modal fade" id="diaFestivoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Asignación de cliente no atendido por dia festivo a la ruta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <meta name="csrf-token" content="{{ csrf_token() }}" />
          <div class="row">
            <div class="col-md-12">
        @if ($clientesDiaFestivo->count() != 0)
            <div class="form-group">
              <label>Listado de Clientes Novedad</label>
              <select class="form-control" id="porDiaFestivoSelect">
              @foreach($clientesDiaFestivo as $cliente)
                <option value='{{ $cliente->id }}'>{{ $cliente->name }}</option>
              @endforeach
              </select>
            </div>
        @else
          <p>Por el momento no hay clientes omitidos por dia festivo.</p>
        @endif
            </div>
          </div>
        </div>
        <div class="modal-footer">
      @if ($clientesDiaFestivo->count() != 0)
          <button type="button" class="btn btn-primary" id="btnUpdatePorDiaFestivo">Asignar</button>
      @endif
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>


@endsection

@section('extras-js')

  <script>
    function validarEjecucion(paradaId){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	  	$.ajax({
          url: '{{ route('ajax.validatePickup') }}',
          type: 'POST',
          data: {
          	_token : CSRF_TOKEN,
          	parada_id : paradaId
          },
          dataType: 'JSON',
          success: function (data) {
            console.log(data);
            if(data.status){
              alert(data.mensaje);
              location.reload();
            }else{
              alert(data.mensaje);
            }
          }
      });
		}

	  $(function () {

	  	var diaUr;
	  	var rutaId;
	  	var recolectorId;
	  	var invoker;

	  	$('#recolectorModal').on('show.bs.modal', function (e) {
	  		invoker = $(e.relatedTarget);
	  		recolectorId = invoker.attr('data-recolector');
	  		diaUr = invoker.attr('data-dia');
			  rutaId = invoker.attr('data-ruta');

		  	if(recolectorId !=0) { /*****Si posee recolector asociado se establece como selected****/
		  		$('#recolectorSelect').val(recolectorId);
		  	}
		});

		$('#novedadModal').on('show.bs.modal', function (e) {
	  		invoker = $(e.relatedTarget);
			  rutaId = invoker.attr('data-ruta');
		});

    $('#diaFestivoModal').on('show.bs.modal', function (e) {
	  		invoker = $(e.relatedTarget);
			  rutaId = invoker.attr('data-ruta');
		});

		$('#zonaFilterSelector').change( function() {
			console.log("cambio "+$(this).val());
			var zonaId = $(this).val();

			$('.zonaIdDiv').hide();
			if($(this).val()!=0) { /*****Mostramos todas las zonas******/
				$('.zonaIdDiv'+zonaId).show('slow');
			}
			else {
				$('.zonaIdDiv').show('slow');
			}

		});

		$('#btnUpdateRecolector').click(function() {
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

		  	$.ajax({
                url: '{{ route('ajax.updateRecolectorRoute') }}',
                type: 'POST',
                data: {
                	_token : CSRF_TOKEN,
                	routeId : rutaId,
                	dia : diaUr,
                	recolectorId : $('#recolectorSelect').val()
                },
                dataType: 'JSON',
                success: function (data) {
                    //console.log("TERMINO "+rutaId+" "+dia+" "+$('#recolectorSelect').val());
                    invoker.attr('data-recolector', $('#recolectorSelect').val());
                    $('#nameRecolector'+rutaId).empty().append(data.recolectorName);
                    $('#recolectorModal').modal('hide');
                    alert(data.msg);
                }
            });
		});

		$('#btnUpdateNovedad').click(function() {
			console.log("Click novedad update");
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

		  	$.ajax({
            url: '{{ route('ajax.updateNovedadRoute') }}',
            type: 'POST',
            data: {
            	_token : CSRF_TOKEN,
            	routeId : rutaId,
            	clienteId : $('#novedadSelect').val()
            },
            dataType: 'JSON',
            success: function (data) {
              $('#novedadModal').modal('hide');
              if(data.status){
                alert(data.msg);
                location.reload();
              }else{
                alert(data.toString());
              }
            }
        });
		});

    $('#btnUpdatePorDiaFestivo').click(function() {
			console.log("Click por dia festiv update");
      var cliente_Id = $('#porDiaFestivoSelect').val();

			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

		  	$.ajax({
            url: '{{ route('ajax.updateForEventDay') }}',
            type: 'POST',
            data: {
            	_token : CSRF_TOKEN,
            	routeId : rutaId,
            	clienteId :cliente_Id
            },
            dataType: 'JSON',
            success: function (data) {
              $('#diaFestivoModal').modal('hide');
              if(data.status){
                alert(data.msg);
                location.reload();
              }else{
                alert(data.toString());
              }
            },
            error: function (data){
              console.log(data);
            }
        });
		});

    $('.viewRoute').click(function() {

    	var id = $(this).attr('data-id');
    	var show = $(this).attr('data-show');
    	var text = '';

    	if(show=='false') {
    		$('.user'+id).show('slow');
    		show='true';
    		text = 'Ocultar lista de Clientes <i class="fas fa-minus"></i>';
    	}
    	else{
    		$('.user'+id).hide('slow');
    		show='false';
    		text = 'Ver lista de Clientes <i class="fas fa-plus"></i>';
    	}

    	$(this).attr('data-show', show);
    	$(this).empty().append(text);
    });

    $('#sendWeek').click(function() {
    	var week = $('#weekSelector').val();
    	var base = '{!! route('ruta.index') !!}';
    	var url = base+'/0/'+week;
    	window.location.href = url;
    });

    $('.daySelected').change(function() {
    	var day = $(this).val();
    	console.log("trae "+day);

    	var base = '{!! route('ruta.index') !!}';
    	var url = base+'/'+day+'/0';
    	window.location.href = url;
    });

	  });
	</script>
@endsection

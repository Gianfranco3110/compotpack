@extends('layouts.main')

@section('extras-css')
@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Administrador de Rutas') }}</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Semana del ') }} <b>{{ date('d/m/Y', strtotime($arraySemanas[$weekSelected]['Mon'])) }}</b> al <b>{{ date('d/m/Y', strtotime($arraySemanas[$weekSelected]['Fri'])) }}</b></h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                
	          <div class="row">
	          	<div class="col-md-6">
	          		<div class="form-group">
                        <label>Selector de semana</label>
                        <select class="form-control" id="weekSelector">
	                        @foreach ($arraySemanas as $key => $semana)
							    <option value="{{ $key }}" {{ $key==$weekSelected ? 'selected' : '' }} >{{ date('d/m/Y', strtotime($semana['Mon'])) . ' al ' . date('d/m/Y', strtotime($semana['Fri'])) }}</option>
							@endforeach
                        </select>
                    </div>
                    <input type="button" id="sendWeek" class="btn btn-primary" value="Ir a Semana">
	          	</div>
	          	<div class="col-md-6">
	          	</div>

	          	</div>
	          	<br>
	          	<div class="row">
	          	<div class="col-md-12" align="center">
	          		<div class="btn-group btn-group-toggle" data-toggle="buttons" >
	          		@php
	          			$fecha = date('Y-m-d', strtotime($arraySemanas[$weekSelected]['Mon']));
	          			$fechaFinal = date('Y-m-d', strtotime($arraySemanas[$weekSelected]['Fri']));
	          			$fechaFinal = date("Y-m-d",strtotime($fechaFinal."+ 1 day"));

	          			do {
	          			$dayToTranlate = date('D', strtotime($fecha));
	          		@endphp
	          			<label class="btn btn-secondary {{ $daySelected == $fecha ? 'active': '' }}" style="cursor:pointer;">
		                	<input type="radio" name="options" value="{{ $fecha }}" class="daySelected" name="daySelected" >{{ $semanaTranslate[$dayToTranlate] }} <br> {{ date('d/m/Y', strtotime($fecha)) }}
		                </label>
	          		@php
	          				$fecha = date("Y-m-d",strtotime($fecha."+ 1 day")); 
	          				
	          			} while ($fecha != $fechaFinal);
	          		@endphp
	                  
	                </div>
	          	</div>
	          </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Rutas para el día <b>{{$semanaTranslate[$dayAcron] }} {{ date('d/m/Y', strtotime($daySelected)) }}</b></h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
              	<div class="form-group">
	                <label>Filtrar por Zonas:</label>
	                <select class="form-control" id="zonaFilterSelector">
	                	<option value='0'>Todas</option>
	                	@foreach($zonas as $zona)
	                		<option value='{{ $zona->id }}'>{{ $zona->titulo }}</option>
	                	@endforeach
	                </select>
	            </select>

	            <br>

              	@foreach($zonas as $zona)
		            <div class="timeline zonaIdDiv{{ $zona->id }} zonaIdDiv">
		              <div class="time-label">
		                <span class="bg-red">{{ $zona->titulo }} {{ $zona->relatedRoutes->where('dia', '=', $dayAcron)->count() == 0 ? '- Sin rutas asignadas' : '' }}</span>
		              </div>

		              @foreach($zona->relatedRoutes->where('dia', '=', $dayAcron) as $ruta)

		              @php
	              		$recolectorInfo = $ruta->getRouteAsignRecolector($ruta->id, $daySelected);
		              @endphp
		              <div>
		                <i class="fas fa-truck bg-blue"></i>
		                <div class="timeline-item">
		                  <!--span class="time"><i class="fas fa-clock"></i> 12:05</span-->
		                  <h3 class="timeline-header bg-orange"><b>Ruta: {{ $ruta->titulo }}</b></h3>

		                  <div class="timeline-body">
		                    <b>Recolector asignado:</b> <span id="nameRecolector{{ $ruta->id }}">{{ !$recolectorInfo ? 'Sin Asignar': $recolectorInfo->relatedRecolector->name }}</span>
		                    <br>
		                    <button class='btn btn-secondary' data-ruta='{{ $ruta->id }}' data-dia='{{ $daySelected }}' data-recolector="{{ !$recolectorInfo ? 0 : $recolectorInfo->relatedRecolector->id }}" data-toggle="modal" data-target="#recolectorModal">Asignar Conductor</button>
		                    <br>
		                    <a href="javascript:void(0)" class="viewRoute" data-id="{{ $ruta->id }}" data-show='false'>Ver lista de Clientes <i class="fas fa-plus"></i></a>
		                  </div>
		                </div>
		              </div>
		              @foreach($ruta->relatedClientsManyToManyChecked($daySelected) as $cliente)
			              <div class='user{{ $ruta->id }}' style="display:none;">
			                <i class="fas fa-user bg-green"></i>
			                <div class="timeline-item">
			                  <!--span class="time"><i class="fas fa-clock"></i> 5 mins ago</span-->
			                  <h3 class="timeline-header no-border">{{ $cliente->name }}</h3>
			                </div>
			              </div>
			          @endforeach
		              @endforeach
		              
		              <div>
		                <i class="fas fa-clock bg-gray"></i>
		              </div>
		            </div>
              	@endforeach
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->

    <!-- Modal -->
	<div class="modal fade" id="recolectorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Asignación de recolector</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<meta name="csrf-token" content="{{ csrf_token() }}" />
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
                        <label>Recolectores</label>
                        <select class="form-control" id="recolectorSelect">
	        		@foreach($recolectores as $recolector)
	        			<option value='{{ $recolector->id }}'>{{ $recolector->name }}</option>
	        		@endforeach
	        			</select>
                    </div>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" id="btnUpdateRecolector">Asignar</button>
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>
@endsection

@section('extras-js')  

    <script>
	  $(function () {

	  	var diaUr;
	  	var rutaId;
	  	var recolectorId;
	  	var invoker;

	  	$('#recolectorModal').on('show.bs.modal', function (e) {
	  		invoker = $(e.relatedTarget);
	  		recolectorId = invoker.attr('data-recolector');
	  		diaUr = invoker.attr('data-dia');
			rutaId = invoker.attr('data-ruta');
		  	
		  	if(recolectorId !=0) { /*****Si posee recolector asociado se establece como selected****/
		  		$('#recolectorSelect').val(recolectorId);
		  	}		  	
		});

		$('#zonaFilterSelector').change( function() {
			console.log("cambio "+$(this).val());
			var zonaId = $(this).val();
			
			$('.zonaIdDiv').hide();
			if($(this).val()!=0) { /*****Mostramos todas las zonas******/
				$('.zonaIdDiv'+zonaId).show('slow');
			}
			else {
				$('.zonaIdDiv').show('slow');
			}
			
		});

		$('#btnUpdateRecolector').click(function() {
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');			
		  	
		  	$.ajax({
                url: '{{ route('ajax.updateRecolectorRoute') }}',
                type: 'POST',
                data: {
                	_token : CSRF_TOKEN, 
                	routeId : rutaId,
                	dia : diaUr,
                	recolectorId : $('#recolectorSelect').val()
                },
                dataType: 'JSON',
                success: function (data) { 
                    //console.log("TERMINO "+rutaId+" "+dia+" "+$('#recolectorSelect').val());
                    invoker.attr('data-recolector', $('#recolectorSelect').val());
                    $('#nameRecolector'+rutaId).empty().append(data.recolectorName);
                    $('#recolectorModal').modal('hide');
                    alert(data.msg);
                }
            }); 
		});

	    $('.viewRoute').click(function() {
	    	
	    	var id = $(this).attr('data-id');
	    	var show = $(this).attr('data-show');
	    	var text = '';

	    	if(show=='false') {
	    		$('.user'+id).show('slow');
	    		show='true';
	    		text = 'Ocultar lista de Clientes <i class="fas fa-minus"></i>';
	    	}
	    	else{
	    		$('.user'+id).hide('slow');
	    		show='false';
	    		text = 'Ver lista de Clientes <i class="fas fa-plus"></i>';
	    	}

	    	$(this).attr('data-show', show);
	    	$(this).empty().append(text);
	    });

	    $('#sendWeek').click(function() {
	    	var week = $('#weekSelector').val();
	    	var base = '{!! route('ruta.index') !!}';
	    	var url = base+'/0/'+week;
	    	window.location.href = url;
	    });

	    $('.daySelected').change(function() {
	    	var day = $(this).val();
	    	console.log("trae "+day);
	    	
	    	var base = '{!! route('ruta.index') !!}';
	    	var url = base+'/'+day+'/0';
	    	window.location.href = url;
	    });

	  });
	</script>
@endsection
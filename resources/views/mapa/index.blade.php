@extends('layouts.main')

@section('extras-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Zona de Cobertura') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- Default box -->
            <!--a href="{{ route('zona.create') }}" class="btn btn-primary">Agregar zona</a>
            <br><br-->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Zona de Cobertura') }}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">

              	@if ($message = Session::get('success'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>	
				        <strong>{{ $message }}</strong>
				</div>
				@endif
                
              		<div id="map" style="min-height: 540px; width: 100%;"></div>
              		<br>

              		<button class="btn btn-primary" id="showRadius">Ocultar Zonas</button>
					<button class="btn btn-primary" id="showClients">Ocultar Clientes</button>

              		<form method="POST" action="{{ route('mapa.store') }}" enctype="multipart/form-data">
              			@csrf
	              		<div class="row">
	              			<div class="col-md-12">
							  	<br>
							  	<h3>Parametros de rutas</h3>
								<div class="form-group">
									<label>Cantidad maxima de clientes por ruta*</label>
									<input type="number" min="1" value="{{ old('cantidad_maxima', $qMaxRoute->value) }}" class="form-control @error('cantidad_maxima') is-invalid @enderror" name="cantidad_maxima" id="cantidad_maxima">
								</div>

	              				<br>
	              				<h3>Listado de Zonas</h3>
	              				<table class="table table-striped" id="tablaZonas">
	              					
		              				@php
		              					$minimo = 0.1;
		              					$contadorZona = 0;
		              				@endphp

		              				@foreach ($zonas as $zona)
		              					<tr>
		              						<td>Zona #{{ $contadorZona+1 }}</td>
		              						<td><button class="btn" style="color:{{ $zona->color }}; background-color: {{ $zona->color }};"></button></td>
		              						<td><input type="number" mino="{{ $minimo }}" name="radio_{{ $contadorZona }}" value="{{ $zona->radio }}" data-id="{{ $contadorZona }}" class="form-control radioClass" required></td>
		              						<td><input type="number" name="precio_{{ $contadorZona }}" value="{{ $zona->precio }}" data-id="{{ $contadorZona }}" class="form-control" placeholder="Precio de zona" required></td>
		              						<td><button type="button" title="Eliminar" class="btn btn-primary deleteRow" onclick="deleteRow({{ $contadorZona }})" id="{{ $contadorZona }}" data-id="{{ $contadorZona }}">x</button></td>
		              					</tr>
		              					@php
		              						$minimo = $zona->radio;
		              						$ultimoRadio = $zona->radio;
		              						$ultimoId = $contadorZona;
		              						$contadorZona++;
		              					@endphp
	              					@endforeach
	              					
	              				<input type="hidden" name="minimo" id="minimo" value="{{ $minimo }}">
	              				<input type="hidden" name="ultimoRadio" id="ultimoRadio" value="{{ $ultimoRadio }}">
	              				<input type="hidden" name="ultimoId" id="ultimoId" value="{{ $ultimoId }}">
	              				<input type="hidden" name="totalZonas" id="totalZonas" value="{{ $contadorZona }}">
	              				</table>

	              				<button class="btn btn-primary" id="agregarZona" type="button">Agregar nueva Zona</button>
	              				<input class="btn btn-success" type="submit" name="submit" value="Guardar Datos">
	              			</div>
	              		</div>
	              	</form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection

@section('extras-js')
    <!-- DataTables -->
    <!--script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script-->

  	
  	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCF9LXYgTkSgr5QOu9OJelPqvJB-1Ij9BE" type="text/javascript"></script>
  	<script type="text/javascript" src="{{ asset('maps/gmaps.js') }}"></script>

<script>
	var latLocation = 4.6929142872859515;
	var lngLocation = -74.06966801297501;
	var radioUs = 3;

	$("#Sucursal_radio").val(radioUs);

	//console.log("ES "+radioUs);
	
	var map = new GMaps({
        el: '#map',
        lat: latLocation,
        lng: lngLocation,
        zoomControl : true,
        zoomControlOpt: {
            style : 'SMALL',
            position: 'TOP_LEFT'
        },
        zoom : 12,
        panControl : false,
        streetViewControl : false,
        mapTypeControl: false,
        overviewMapControl: false,
        click: function(e) {
		    //alert('click '+e.latitud);
		    console.log(e.latLng.lat());
		  },
      });
      
      
	var path = [[4.770928117510021, -74.04265239031186], [4.768514405885835, -74.0440889763219], [4.770086920195376, -74.05161471730683], [4.765520570614821, -74.05264647211928], [4.7724759254331826, -74.06384404675462], [4.763493329879006, -74.06973373342461], [4.720508073457822, -74.09427476213008], [4.717209837728416, -74.09272511207857], [4.711497532293983, -74.0934947391899], [4.678742079110091, -74.1198813070372], [4.626467500125243, -74.09451574130638], [4.624529109373487, -74.08309819323918], [4.620051787632438, -74.0866387502758], [4.603219082700261, -74.06236438227229] /*carreara 4 Este*/, [4.610027444116612, -74.06581683497656], [4.623711588338709, -74.05651986207768], [4.6382116916530425, -74.052425340121], [4.6382116916530425, -74.052425340121], [4.667446185526047, -74.04412931225994], [4.685594146626684, -74.03533813302084], [4.683265664738547, -74.02883506907601], [4.686530338277964, -74.01898413085281], [4.706214075556662, -74.01963443711685], [4.715599661879258, -74.0180447991568], [4.728057571428454, -74.01243289574812], [4.735042544131227, -74.01178258927786], [4.74274753529203, -74.01421521681955], [4.761757612746261, -74.0210554768808], [4.769294291108273, -74.02235608971907], [4.769798333514273, -74.02664329483824], [4.77018604607833, -74.02685351796043]];

	polygon = map.drawPolygon({
	  paths: path, // pre-defined polygon shape
	  strokeColor: '#000000',
	  strokeOpacity: 1,
	  strokeWeight: 3,
	  fillColor: '#E5FFD2',
	  fillOpacity: 0.2,
	  zIndex: 50000000000000
	});

	/*MARKER SEDE*/
	var marker = map.addMarker({
        lat: 4.74112408575684,
        lng: -74.04163608173303,
        draggable: false,
        dragend: function(event) {
           /* var lat = event.latLng.lat();
            var lng = event.latLng.lng();
            //alert('draggable '+lat+" - "+ lng);
            latLocation = lat;
  			lngLocation = lng;
  			latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
  			circle.setCenter(latlng);
  			$("#Sucursal_latitud").val(lat);
  			$("#Sucursal_longitud").val(lng);
  			getGeocode(latlng);
  			console.log("frada");  	
  			map.fitBounds(circle.getBounds());*/		
        },
        title: 'Sede CompostPack' ,
        infoWindow: {
            content: "Sede CompostPack"
        }
    	});	

	/*MARKER CLIENTES*/
	
	contadorClientes = 0;
	var clientes = [];
	var clientMarkers = [];

	@foreach ($clientes as $key => $cliente)
	
	var clienteMarked = map.addMarker({
        lat: {{ $cliente->latitud }},
        lng: {{ $cliente->longitud }},
        draggable: false,
		icon: {                             
  			url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"                           
		},
        title: '{{ $cliente->name }}' ,
        infoWindow: {
            content: '{{ $cliente->name }} <br> Dirección: {{ $cliente->direccion }}'
        }
    });	

	clientMarkers[contadorClientes] = clienteMarked;
	clientes[contadorClientes] = {"latitud": {{ $cliente->latitud }}, "longitud": {{ $cliente->longitud }}, "titulo": "{{ $cliente->name }}", "content": "{{ $cliente->name }} <br> Dirección: {{ $cliente->direccion }}"};
	console.log("gcliente"+contadorClientes);
	contadorClientes++;
	@endforeach

	/*CIRCULOS COBERTURA*/
	var latlng = {lat: parseFloat(4.74112408575684), lng: parseFloat(-74.04163608173303)};

	var circles = [];
	var zonas = [];
	var conta=2000000;
	var contador = 0;
	var colores = ["#DFFF00", "#FFBF00", "#FF7F50", "#DE3163", "#9FE2BF", "#40E0D0", "#6495ED", "#CCCCFF", "#DF8FF9", "#F98FDC", "#F98F9A", "#F6F98F", "#AEF98F", "#8FF9F4"];
	
	@foreach ($zonas as $key => $zona)
	conta--;
	var circle = map.drawCircle({
		    strokeColor: "#000000",
	        strokeOpacity: 0.8,
	        strokeWeight: 1,
	        fillColor: "{{ $zona->color }}",
	        fillOpacity: 0.35,
	        center: latlng,
	        radius: {{ $zona->radio }}*1000, // in meters
	        editable : false,
	        draggable: false,
	        zIndex: conta
		});

	circles[contador] = circle;
	zonas[contador] = {"Titulo": "{{ $zona->titulo }}", "Color": "{{ $zona->color }}", "Radio": "{{$zona->radio}}", "Precio": "{{$zona->precio}}"};
	//console.log("guarda en"+zonas);
	contador++;
	@endforeach

	console.log(circles);

	//console.log(circles);

	//console.log(circles[1]);

	var flagShowZonas = 1;

	$( "#showRadius" ).click(function() { 

		if(flagShowZonas==1) {
			removeAllcircles();
			flagShowZonas=0;
			$(this).empty().append("Mostrar Zonas");
		}
		else{
			showAllcircles();
			flagShowZonas=1;	
			$(this).empty().append("Ocultar Zonas");
		}
	});

	var flagShowClients = 1;

	$( "#showClients" ).click(function() { 

		if(flagShowClients==1) {
			removeAllclients();
			flagShowClients=0;
			$(this).empty().append("Mostrar Clientes");
		}
		else{
			showAllclients();
			flagShowClients=1;	
			$(this).empty().append("Ocultar Clientes");
		}
	});

	$('.radioClass').on('input', function() { 
		console.log("Cambio nuber "+$(this).val());
	});
	
	$('#agregarZona').click(function() {
		var ultimoRadio = parseInt($("#ultimoRadio").val()) + 3;
		$("#ultimoRadio").val(ultimoRadio);

		var ultimoId = parseInt($("#ultimoId").val()) + 1;
		$("#ultimoId").val(ultimoId);
		var ultimoId = zonas.length;

		console.log("va agregar "+(contador));

		$('#tablaZonas tbody').append('<tr><td>Zona #'+(ultimoId+1)+'</td><td><button class="btn" style="color:0; background-color: '+colores[contador]+';"></button></td><td><input type="number" mino="0" name="radio_'+contador+'" value="'+ultimoRadio+'" class="form-control radioClass" required data-id="'+contador+'"></td><td><input type="number" name="precio_'+contador+'" value="" data-id="'+contador+'" class="form-control" placeholder="Precio de zona" required></td><td><button button type="button" title="Eliminar" class="btn btn-primary" onclick="deleteRow('+contador+')" data-id="'+contador+'">x</button></td></tr>');
		
		console.log("Imprimir zoas ID "+ultimoId);
		console.log(zonas);
		zonas.push({"Titulo": "Zona #"+ultimoId+"", "Color": colores[contador], "Radio": ""+ultimoRadio+"" });
		console.log("2 Imprimir zoas");
		console.log(zonas);

		removeAllcircles();
		showAllcircles();
		$('#totalZonas').val(zonas.length);
	});

	//add Circles
	function showAllcircles() {
		//console.log("llega zonas "+circles);
		conta=2000000;
		contador = 0;
		circles = [];
		//zonas = zonas.reverse();
		$.each(zonas, function(i, item) {
			conta--;
		    //console.log(zonas[i].Radio+" Color "+zonas[i].Color);

		    var circle = map.drawCircle({
			    strokeColor: "#000000",
		        strokeOpacity: 0.8,
		        strokeWeight: 1,
		        fillColor: zonas[i].Color,
		        fillOpacity: 0.35,
		        center: latlng,
		        radius: zonas[i].Radio*1000, // in meters
		        editable : false,
		        draggable: false,
		        zIndex: conta
			});

			circles[contador] = circle;
			contador++;
		});

		console.log(circles);
	}

	//add Clientes
	function showAllclients() {
		//console.log("llega zonas "+circles);
		conta=2000000;
		contador = 0;
		clientMarkers = [];
		//zonas = zonas.reverse();
		$.each(clientes, function(i, item) {
			conta--;
		    //console.log(zonas[i].Radio+" Color "+zonas[i].Color);

			var clienteMarked = map.addMarker({
				lat: clientes[i].latitud,
				lng: clientes[i].longitud,
				draggable: false,
				icon: {                             
					url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"                           
				},
				title: clientes[i].titulo,
				infoWindow: {
					content: clientes[i].content
				}
			});	

			clientMarkers[contador] = clienteMarked;
			contador++;
		});

		console.log(clienteMarked);
	}

	//update Circles
	function updateAllcircles(toDelete) {
		console.log("llega zonas "+zonas);

		conta=2000000;
		contador = 0;
		circles = [];
		var zonaAux = zonas;
		zonas = [];

		//zonas[contador] = {"Titulo": "{{ $zona->titulo }}", "Color": "{{ $zona->color }}", "Radio": "{{$zona->radio}}", "Precio": "{{$zona->precio}}"};
		
		$.each(zonaAux, function(i, item) {
			console.log("Entra por "+i);
			
			if(i != toDelete) {
				conta--;
			    //console.log(zonas[i].Radio+" Color "+zonas[i].Color);
			    zonas[contador] = {"Titulo": "Zona #'+contador+1+'", "Color": colores[contador], "Radio": zonaAux[i].Radio, "Precio": zonaAux[i].Precio };

			    var circle = map.drawCircle({
				    strokeColor: "#000000",
			        strokeOpacity: 0.8,
			        strokeWeight: 1,
			        fillColor: colores[contador],
			        fillOpacity: 0.35,
			        center: latlng,
			        radius: zonaAux[i].Radio*1000, // in meters
			        editable : false,
			        draggable: false,
			        zIndex: conta
				});

				circles[contador] = circle;


				/*Genera la nueva tabla*/
				$('#tablaZonas tbody').append('<tr><td>Zona #'+(contador+1)+'</td><td><button class="btn" style="color:0; background-color: '+colores[contador]+';"></button></td><td><input type="number" mino="0" name="radio_'+contador+'" value="'+zonaAux[i].Radio+'" class="form-control radioClass" required data-id="'+contador+'"></td><td><input type="number" name="precio_'+contador+'" value="'+zonaAux[i].Precio+'" data-id="'+contador+'" class="form-control" placeholder="Precio de zona" required></td><td><button button type="button" title="Eliminar" class="btn btn-primary" onclick="deleteRow('+contador+')" data-id="'+contador+'">x</button></td></tr>');

				contador++;
			}
		});

		console.log("actualiza "+zonas);
		console.log(circles);
	}

	// remove All circles
	function removeAllcircles() {
	  for(var i in circles) {
	    circles[i].setMap(null);
	  }
	  circles = []; // this is if you really want to remove them, so you reset the variable.
	}

	// remove All clients
	function removeAllclients() {
	  for(var i in clientMarkers) {
	    clientMarkers[i].setMap(null);
	  }
	  clientMarkers = []; // this is if you really want to remove them, so you reset the variable.
	}

	$(document).on("change", ".radioClass", function(){

		removeAllcircles();
		updateRadioAllcircles($(this).val(), $(this).attr("data-id"));

	    console.log($(this).val()+" de zona "+$(this).attr("data-id"));
	});

	//update radios Circles
	function updateRadioAllcircles(radio, id) {
		console.log("llega zonas "+zonas);

		conta=2000000;
		contador = 0;
		circles = [];
		var zonaAux = zonas;
		zonas = [];

		//zonas[contador] = {"Titulo": "{{ $zona->titulo }}", "Color": "{{ $zona->color }}", "Radio": "{{$zona->radio}}" };
		
		$.each(zonaAux, function(i, item) {
			console.log("Entra por "+i);
			
			conta--;
			var radioInt;
			if(i == id) {
				zonas[contador] = {"Titulo": "Zona #'+contador+1+'", "Color": colores[contador], "Radio": radio };
				radioInt = radio;
			}
			else {
				zonas[contador] = {"Titulo": "Zona #'+contador+1+'", "Color": colores[contador], "Radio": zonaAux[i].Radio };
				radioInt = zonaAux[i].Radio;
			}
				
			    //console.log(zonas[i].Radio+" Color "+zonas[i].Color);

			    var circle = map.drawCircle({
				    strokeColor: "#000000",
			        strokeOpacity: 0.8,
			        strokeWeight: 1,
			        fillColor: colores[contador],
			        fillOpacity: 0.35,
			        center: latlng,
			        radius: radioInt*1000, // in meters
			        editable : false,
			        draggable: false,
			        zIndex: conta
				});

				circles[contador] = circle;
				contador++;
		});

		console.log("actualiza "+zonas);
		console.log(circles);
	}

	function deleteRow(id) {
		console.log("Eliminar "+id);
		
		var r = confirm("¿Estas seguro de querer eliminar esta Zona?");
		if (r == true) {
		  console.log("ACEPTO");
		  var totalZonas = $("#totalZonas").val();
		  if(totalZonas==1){
		      alert("No puedes borrar todas las zonas, debes tener al menos una en el sistema.");
		  }
		  else{
		   $("#tablaZonas tr").remove();
    		  removeAllcircles();
    		  updateAllcircles(id);
    		  $('#totalZonas').val(zonas.length);   
		  }
		} 
		else {
		    console.log("NO ACEPTO");
		}
	}

	/*var circle = map.drawCircle({
		    strokeColor: "#000000",
	        strokeOpacity: 0.8,
	        strokeWeight: 1,
	        fillColor: "#FFA96A",
	        fillOpacity: 0.35,
	        center: latlng,
	        radius: radioUs*2*1000, // in meters
	        editable : false,
	        draggable: false
		});

	var circle = map.drawCircle({
		    strokeColor: "#000000",
	        strokeOpacity: 0.8,
	        strokeWeight: 1,
	        fillColor: "#c3fc49",
	        fillOpacity: 0.35,
	        center: latlng,
	        radius: radioUs*1000, // in meters
	        editable : false,
	        draggable: false
		});*/

	console.log("center cicle "+latlng+" y "+latLocation+" y "+lngLocation);
	//console.log("ES "+map.getBounds());
	//map.fitBounds();

	if(map.checkGeofence(6.6929142872859515, -74.06966801297501, polygon)) { /***DETERMINAR SI ESTA DENTRO DEL POLIGONO UN PUNTO******/
		console.log("ESTA");
	}
	else{
		console.log("NO ESTA");
	}

</script> 
@endsection
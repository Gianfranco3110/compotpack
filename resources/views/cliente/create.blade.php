@extends('layouts.main')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Crear de Cliente') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

          	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Datos del Cliente') }}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">

              <form method="POST" action="{{ route('cliente.store') }}" enctype="multipart/form-data">
    			@csrf

                <div class="row">
                	<div class="col-md-6">
                		<div class="form-group">
		                	<label>Nombre completo</label>
		                    <input type="text" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Nombre Completo" required>
		                </div>

		                <div class="form-group">
		                	<label>Email</label>
		                    <input type="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Email" required>
		                </div>

		                <div class="form-group">
		                	<label>Dirección</label>
		                    <input type="text" value="{{ old('direccion') }}" class="form-control @error('direccion') is-invalid @enderror" name="direccion" id="direccion" placeholder="Dirección" required>
		                </div>

		                <!--div class="form-group">
	                      <label>Zona</label>
	                        <select class="form-control @error('id_zona') is-invalid @enderror" name="id_zona" required>
	                          <option value=''>Elija una zona</option>
	                          @foreach($zonas as $zona)
	                            <option value='{{ $zona->id }}' {{ old('id_zona') == $zona->id ? 'selected':'' }}>{{ $zona->titulo }}</option>
	                          @endforeach
	                        </select>
	                    </div-->

		                <div class="form-group">
		                    <label>Foto</label>
		                    <div class="input-group">
		                      <div class="custom-file">
		                        <input type="file" class="custom-file-input @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" accept="image/x-png,image/gif,image/jpeg">
		                        <label class="custom-file-label" for="exampleInputFile" data-browse="Seleccionar">Buscar Foto</label>
		                      </div>
		                    </div>
		                </div>
                	</div>

                	<div class="col-md-6">

                		<div class="form-group">
		                	<label>Teléfono</label>
		                    <input type="text" value="{{ old('telefono') }}" class="form-control @error('telefono') is-invalid @enderror" name="telefono" id="telefono" placeholder="Teléfono" required>
		                </div>

		                <div class="form-group">
		                	<label>Wahtsapp</label>
		                    <input type="text" value="{{ old('whatsapp') }}" class="form-control @error('whatsapp') is-invalid @enderror" name="whatsapp" id="whatsapp" placeholder="Whatsapp" required>
		                </div>

                		<div class="form-group">
		                	<label>Password</label>
		                    <input type="password" value="{{ old('password') }}" class="form-control @error('password') is-invalid @enderror" name="password" id="password" required>
		                </div>

		                <div class="form-group">
		                	<label>Confirmación de Password</label>
		                    <input type="password" value="{{ old('password_confirmation') }}" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="password_confirmation" required>
		                </div>
                	</div>


                	<div class="col-md-12">
                		<label>Ubicación en mapa</label>
                		<div id="map" style="min-height: 350px; width: 100%;">
						</div>
                	</div>

                	<div class="col-md-6">
                		<div class="form-group">
		                	<label>Latitud</label>
		                    <input type="text" value="{{ old('latitud') }}" class="form-control @error('latitud') is-invalid @enderror" name="latitud" id="latitud" required readonly>
		                </div>
                	</div>

                	<div class="col-md-6">
                		<div class="form-group">
		                	<label>Longitud</label>
		                    <input type="text" value="{{ old('longitud') }}" class="form-control @error('longitud') is-invalid @enderror" name="longitud" id="longitud" required readonly>
		                </div>
                	</div>

                	<br>
                	<br>

                	<div class="col-md-12">
	                    <label>Día de recolección</label>
	                    <div class="form-group">

	                      @foreach ($semana as $key => $dia)
						  <div class="form-check">
							<input class="form-check-input @error('dia_recoleccion') is-invalid @enderror" type="radio" name="dia_recoleccion" id="exampleRadios{{ $key }}" value="{{ $key }}" {{ $diasPermitidos[$key] == 1 ? '':'disabled' }} checked>
							<label class="form-check-label" for="exampleRadios{{ $key }}">
								{{ $dia }}
							</label>
							</div>
	                      @endforeach

	                    </div>

	            	</div>

                	<div class="col-md-6">
                		<button class="btn btn-primary" type="button" id="detectGeo">Detectar mi Ubicación</button>
                		<input class="btn btn-primary" type="submit" name="submit" id="goRegister" value="Registrar">
                		<a href="{{ url()->previous() }}" class="btn btn-danger" type="button">Regresar</a>
                	</div>
                </div>

              </form>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection

@section('extras-js')
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCF9LXYgTkSgr5QOu9OJelPqvJB-1Ij9BE" type="text/javascript"></script>
	<script type="text/javascript" src="{{ asset('/maps/gmaps.js') }}"></script>

  	<script type="text/javascript">

  		var latLocation = 4.60971;
		var lngLocation = -74.08175;
    $(function () {
      $('.custom-file-input').on('change', function(event) {
          var inputFile = event.currentTarget;
          $(inputFile).parent()
              .find('.custom-file-label')
              .html(inputFile.files[0].name);
      });
    });

    $(function () {
      $('.custom-file-input').on('change', function(event) {
          var inputFile = event.currentTarget;
          $(inputFile).parent()
              .find('.custom-file-label')
              .html(inputFile.files[0].name);
      });
    });

  		var map = new GMaps({
	        el: '#map',
	        lat: latLocation,
	        lng: lngLocation,
	        zoomControl : true,
	        zoomControlOpt: {
	            style : 'SMALL',
	            position: 'TOP_LEFT'
	        },
	        zoom : 17,
	        panControl : false,
	        streetViewControl : false,
	        mapTypeControl: false,
	        overviewMapControl: false
	      });

  		var marker = map.addMarker({
	        lat: latLocation,
	        lng: lngLocation,
	        draggable: true,
	        dragend: function(event) {
	            var lat = event.latLng.lat();
	            var lng = event.latLng.lng();
	            //alert('draggable '+lat+" - "+ lng);
	            latLocation = lat;
	  			lngLocation = lng;
	  			latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
	  			$("#latitud").val(lat);
	  			$("#longitud").val(lng);
	  			//getGeocode(latlng);

				if(!map.checkGeofence(parseFloat(lat), parseFloat(lng), polygon)) { /***DETERMINAR SI ESTA DENTRO DEL POLIGONO UN PUNTO******/
					$('#direccion').addClass('is-invalid');
					$('#goRegister').prop('disabled', true);
					alert("La dirección indicada no esta dentro de la zona de cobertura. Por favor modificar la dirección del cliente.");
				}
				else{
					$('#goRegister').prop('disabled', false);
					$('#direccion').removeClass('is-invalid');
				}
	        },
	        title: 'Marker #' ,
	        infoWindow: {
	            content: "Tu Dirección"
	        }
	    	});


		var path = [[4.770928117510021, -74.04265239031186], [4.768514405885835, -74.0440889763219], [4.770086920195376, -74.05161471730683], [4.765520570614821, -74.05264647211928], [4.7724759254331826, -74.06384404675462], [4.763493329879006, -74.06973373342461], [4.720508073457822, -74.09427476213008], [4.717209837728416, -74.09272511207857], [4.711497532293983, -74.0934947391899], [4.678742079110091, -74.1198813070372], [4.626467500125243, -74.09451574130638], [4.624529109373487, -74.08309819323918], [4.620051787632438, -74.0866387502758], [4.603219082700261, -74.06236438227229] /*carreara 4 Este*/, [4.610027444116612, -74.06581683497656], [4.623711588338709, -74.05651986207768], [4.6382116916530425, -74.052425340121], [4.6382116916530425, -74.052425340121], [4.667446185526047, -74.04412931225994], [4.685594146626684, -74.03533813302084], [4.683265664738547, -74.02883506907601], [4.686530338277964, -74.01898413085281], [4.706214075556662, -74.01963443711685], [4.715599661879258, -74.0180447991568], [4.728057571428454, -74.01243289574812], [4.735042544131227, -74.01178258927786], [4.74274753529203, -74.01421521681955], [4.761757612746261, -74.0210554768808], [4.769294291108273, -74.02235608971907], [4.769798333514273, -74.02664329483824], [4.77018604607833, -74.02685351796043]];

		polygon = map.drawPolygon({
			paths: path, // pre-defined polygon shape
			strokeColor: '#000000',
			strokeOpacity: 1,
			strokeWeight: 3,
			fillColor: '#E5FFD2',
			fillOpacity: 0.2,
			zIndex: 50000000000000
		});

  		$("#direccion").keyup(function(){
			var dirRequest = $("#direccion").val();
			GMaps.geocode({
				  address: dirRequest,
				  callback: function(results, status) {
				    if (status == 'OK') {
				      var latlng = results[0].geometry.location;
				      latLocation = parseFloat(latlng.lat());
	    			  lngLocation = parseFloat(latlng.lng());
	    			  var latlng = {lat: parseFloat(latLocation), lng: parseFloat(lngLocation)};
	    			  $("#latitud").val(latLocation);
		  			  $("#longitud").val(lngLocation);
				      map.setCenter(latlng);
				      marker.setPosition(latlng);

					  if(!map.checkGeofence(parseFloat(latlng.lat()), parseFloat(latlng.lng()), polygon)) { /***DETERMINAR SI ESTA DENTRO DEL POLIGONO UN PUNTO******/
						$('#direccion').addClass('is-invalid');
						$('#goRegister').prop('disabled', true);
						alert("La dirección indicada no esta dentro de la zona de cobertura. Por favor modificar la dirección del cliente.");
					}
					else{
						$('#goRegister').prop('disabled', false);
						$('#direccion').removeClass('is-invalid');
						console.log("SI ESTA");
					}

				    }
				  }
				});
			});

  		$("#detectGeo").click(function(){
		    GMaps.geolocate({
			  success: function(position) {
			    esLocation = true;
			    latLocation = position.coords.latitude;
	    		lngLocation = position.coords.longitude;
	    		var latlng = {lat: parseFloat(latLocation), lng: parseFloat(lngLocation)};
	    		$("#latitud").val(latLocation);
		  		$("#longitud").val(lngLocation);
	    		GMaps.geocode({
				  location: latlng,
				  callback: function(results, status) {
				    if (status == 'OK') {
				      var direc = results[0].formatted_address;
				      var res = direc.split(",");
		        	  $("#direccion").val(res[0]+","+res[1]+".");
				     map.setCenter(latlng);
				     marker.setPosition(latlng);
				    }
				    else{
				    	alert("No hemos podido detectar tu ubicación.");
				    }
				  }
				});

			  },
			  error: function(error) {
			    alert('Ha fallado la geolocalización, debes dar permisos al navegador para poder usar estar opción.');//+error.message);
			  },
			  not_supported: function() {
			    alert("Tu navegador no es compatible con geolacalización");
			  },
			  always: function() {
			    //alert("Done!");
			  }
			});
		});

  		function getGeocode (latlng) {
			GMaps.geocode({
			  location: latlng,
			  callback: function(results, status) {
			    if (status == 'OK') {
			      var direc = results[0].formatted_address;
				  $("#direccion").val(direc);
				}
			  }
		   });
		}
  	</script>
@endsection

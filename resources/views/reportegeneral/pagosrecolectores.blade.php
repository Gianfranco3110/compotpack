@extends('layouts.main')

@section('extras-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@inject('rutaLista', 'App\RutaLista')
@inject('rutaClienteLista', 'App\RutaClienteLista')
@inject('historialPagoRecolector', 'App\HistorialPagoRecolector')

@section('content')

<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Reporte Pagos a Recolectores') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">

        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                <i class="ion ion-clipboard mr-1"></i>
                  Reporte del <b>{{ date('d/m/Y', strtotime($fecha_inicio)) }}</b> al <b>{{ date('d/m/Y', strtotime($fecha_fin)) }}</b>
                </h3>                
              </div>
              
              <div class="card-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Fecha</th>
                    <th>Recolector</th>
                    <th>Carreras Pagadas</th>
                    <th>Monto</th>
                </tr>
                </thead>
                <tbody>
                @php 
                    
                @endphp
                @php 
                $totalPagos = $historial->count();
                $totalMonto = 0;
                @endphp
                @foreach($historial as $histi)
                    @php 
                    $totalMonto += $histi->monto;
                    @endphp
                    <tr>
                        <td>{{ $histi->id }}</td>
                        <td>{{ date('d/m/Y', strtotime($histi->fecha)) }}</td>
                        <td>{{ $histi->relatedRecolector->name }}</td>
                        <td>{{ $histi->carreras_pagadas }}</td>
                        <td>${{ number_format($histi->monto) }}</td>
                    </tr>
                @endforeach
                </tbody>
                </table>

                <p><b>Total Pagos Realizados:</b> {{ $totalPagos }}</p>
                <p><b>Monto Total Pagado:</b> ${{ number_format($totalMonto) }}</p>
                
                <br>
                <a href="{{ route('reporte.general') }}" class="btn btn-danger">Regresar</a>
              </div>
              
            </div>
            <!-- /.card -->
          </div>

          <div class="col-md-12">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="far fa-chart-bar"></i>
                  Gráfico Pagos realizados por día
                </h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div id="bar-chart" style="height: 300px;"></div>
              </div>
              <!-- /.card-body-->
            </div>
            <!-- /.card -->

            <div class="col-md-12">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="far fa-chart-bar"></i>
                  Gráfico monto total pagado por día
                </h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div id="bar-chart2" style="height: 300px;"></div>
              </div>
              <!-- /.card-body-->
            </div>
            <!-- /.card -->

        </div>        

      </div>
    </section>
    <!-- /.content -->
@endsection

@section('extras-js')
    <!-- DataTables -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

    <!-- FLOT CHARTS -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/flot/jquery.flot.js') }}"></script>
    <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/flot-old/jquery.flot.resize.min.js') }}"></script>
    <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/flot-old/jquery.flot.pie.min.js') }}"></script>

    <script>
	  $(function () {
	    $("#example1").DataTable();

      /*
      * BAR CHART
      * ---------
      */

      var start = new Date("{{ date('m/d/Y', strtotime($fecha_inicio)) }}");
      var end = new Date("{{ date('m/d/Y', strtotime($fecha_fin)) }}");

      var loop = new Date(start);
      while(loop <= end){
        //console.log(loop);   
        var formattedDate = new Date(loop);
        var d = formattedDate.getDate();
        var m =  formattedDate.getMonth();
        m += 1;  // JavaScript months are 0-11
        var y = formattedDate.getFullYear();

        console.log(d + "/" + m + "/" + y);

        var newDate = loop.setDate(loop.getDate() + 1);
        loop = new Date(newDate);
      }

      var bar_data = {
        data : [
          @php
          $firstDay = date("Y-m-d", strtotime($fecha_inicio));
          $nextFriday = date("Y-m-d", strtotime($fecha_fin));
          $countDias = 1;

          while($firstDay != $nextFriday) {
            $totalDia = $historialPagoRecolector::where('fecha', '>=', date('Y-m-d', strtotime($firstDay)))->where('fecha', '<=', date('Y-m-d', strtotime($firstDay)))->get()->count();
          @endphp
          [{{ $countDias }}, {{ $totalDia }}], 
          @php
          $firstDay = date('Y-m-d', strtotime( $firstDay . " +1 days"));
          $countDias++;
          }
          @endphp
          ],
        bars: { show: true }
      }
      $.plot('#bar-chart', [bar_data], {
        grid  : {
          borderWidth: 1,
          borderColor: '#f3f3f3',
          tickColor  : '#f3f3f3'
        },
        series: {
          bars: {
            show: true, barWidth: 0.5, align: 'center',
          },
        },
        colors: ['#3c8dbc'],
        xaxis : {
          ticks: [
            @php
            $firstDay = date("Y-m-d", strtotime($fecha_inicio));
            $nextFriday = date("Y-m-d", strtotime($fecha_fin));
            $countDias = 1;

            while($firstDay != $nextFriday) {
            @endphp
              [{{ $countDias }},'{{ date("d/m/Y", strtotime($firstDay))}}'],
            @php
            $firstDay = date('Y-m-d', strtotime( $firstDay . " +1 days"));
            $countDias++;
            }
            @endphp
            ]
        }
      })
      /* END BAR CHART */

      /*
      * BAR CHART
      * ---------
      */

      var start = new Date("{{ date('m/d/Y', strtotime($fecha_inicio)) }}");
      var end = new Date("{{ date('m/d/Y', strtotime($fecha_fin)) }}");

      var loop = new Date(start);
      while(loop <= end){
        //console.log(loop);   
        var formattedDate = new Date(loop);
        var d = formattedDate.getDate();
        var m =  formattedDate.getMonth();
        m += 1;  // JavaScript months are 0-11
        var y = formattedDate.getFullYear();

        console.log(d + "/" + m + "/" + y);

        var newDate = loop.setDate(loop.getDate() + 1);
        loop = new Date(newDate);
      }

      var bar_data = {
        data : [
          @php
          $firstDay = date("Y-m-d", strtotime($fecha_inicio));
          $nextFriday = date("Y-m-d", strtotime($fecha_fin));
          $countDias = 1;

          while($firstDay != $nextFriday) {
            $countDia = 0;            
            $totalDia = $historialPagoRecolector::where('fecha', '>=', date('Y-m-d', strtotime($firstDay)))->where('fecha', '<=', date('Y-m-d', strtotime($firstDay)))->get();
            foreach($totalDia as $dia) {
              $countDia += $dia->monto;
            }
          @endphp
          [{{ $countDias }}, {{ $countDia }}], 
          @php
          $firstDay = date('Y-m-d', strtotime( $firstDay . " +1 days"));
          $countDias++;
          }
          @endphp
          ],
        bars: { show: true }
      }
      $.plot('#bar-chart2', [bar_data], {
        grid  : {
          borderWidth: 1,
          borderColor: '#f3f3f3',
          tickColor  : '#f3f3f3'
        },
        series: {
          bars: {
            show: true, barWidth: 0.5, align: 'center',
          },
        },
        colors: ['#3c8dbc'],
        xaxis : {
          ticks: [
            @php
            $firstDay = date("Y-m-d", strtotime($fecha_inicio));
            $nextFriday = date("Y-m-d", strtotime($fecha_fin));
            $countDias = 1;

            while($firstDay != $nextFriday) {
            @endphp
              [{{ $countDias }},'{{ date("d/m/Y", strtotime($firstDay))}}'],
            @php
            $firstDay = date('Y-m-d', strtotime( $firstDay . " +1 days"));
            $countDias++;
            }
            @endphp
            ]
        }
      })
      /* END BAR CHART */
	  });
	</script>
@endsection
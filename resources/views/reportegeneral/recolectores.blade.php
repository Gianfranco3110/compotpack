@extends('layouts.main')

@section('extras-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@inject('rutaLista', 'App\RutaLista')
@inject('rutaClienteLista', 'App\RutaClienteLista')

@section('content')

<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Reporte de Recolectores') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">

        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                <i class="ion ion-clipboard mr-1"></i>
                  Reporte del <b>{{ date('d/m/Y', strtotime($fecha_inicio)) }}</b> al <b>{{ date('d/m/Y', strtotime($fecha_fin)) }}</b>
                </h3>                
              </div>
              
              <div class="card-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Recolector</th>
                    <th>Carreras Asignadas</th>
                    <th>Atendidas</th>
                    <th>Sin atender</th>
                    <th>Pago por Atendidas</th>
                </tr>
                </thead>
                <tbody>
                @php 
                    $totalesRecolectores = array();
                @endphp
                @foreach($recolectores as $recolector)
                    @php 
                    $totalAsignadas = 0;
                    $totalAtendidas = 0;
                    $totalSinAtender = 0;
                    $totalIngresos = 0;
                    $rutas = $rutaLista::where('id_recolector', $recolector->id)
                                        ->where('fecha', '>=', $fecha_inicio)
                                        ->where('fecha', '<=', $fecha_fin)
                                        ->get();

                        foreach($rutas as $ruta) {
                            $pagosRecolector = $rutaClienteLista::where('id_ruta', $ruta->id)->where('status','Recogida')->get();
                            
                            foreach($pagosRecolector as $pagos) {
                                $totalIngresos += $pagos->pago_recolector;
                            }

                            $totalSinAtender += $rutaClienteLista::where('id_ruta', $ruta->id)->where('status', '<>','Recogida')->get()->count();
                            $totalAtendidas +=  $pagosRecolector->count();
                        }

                        $totalAsignadas = $totalAtendidas + $totalSinAtender;

                        $totalesRecolectores[] = [
                          'recolector' => $recolector->name,
                          'exitosas' => $totalAtendidas,
                          'noExitosas' => $totalSinAtender
                        ];
                    @endphp
                    <tr>
                        <td>{{ $recolector->name }}</td>
                        <td>{{ $totalAsignadas }}</td>
                        <td>{{ $totalAtendidas }}</td>
                        <td>{{ $totalSinAtender }}</td>
                        <td>${{ number_format($totalIngresos) }}</td>
                    </tr>
                @endforeach
                </tbody>
                </table>
                
                <br>
                <a href="{{ route('reporte.general') }}" class="btn btn-danger">Regresar</a>
              </div>
              
            </div>
            <!-- /.card -->
          </div>

          <div class="col-md-12">
            <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="far fa-chart-bar"></i>
                Gráfico servicios éxitosos por recolector
              </h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div id="bar-chart" style="height: 300px;"></div>
            </div>
            <!-- /.card-body-->
          </div>
          <!-- /.card -->

          <div class="col-md-12">
            <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="far fa-chart-bar"></i>
                Gráfico servicios no completados por recolector
              </h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div id="bar-chart2" style="height: 300px;"></div>
            </div>
            <!-- /.card-body-->
          </div>
          <!-- /.card -->


        </div>        

      </div>
    </section>
    <!-- /.content -->
@endsection

@section('extras-js')
    <!-- DataTables -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

    <!-- FLOT CHARTS -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/flot/jquery.flot.js') }}"></script>
    <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/flot-old/jquery.flot.resize.min.js') }}"></script>
    <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/flot-old/jquery.flot.pie.min.js') }}"></script>

    <script>
	  $(function () {
	    $("#example1").DataTable();

      /*
      * BAR CHART
      * ---------
      */

      var bar_data = {
        data : [
          @php
          $cuentaRecolector = 1;
          foreach($totalesRecolectores as $recolector) {
            
          @endphp
            [{{ $cuentaRecolector }}, {{ $recolector["exitosas"] }}], 
          @php
            $cuentaRecolector++;
          }
          @endphp
          ],
        bars: { show: true }
      }
      $.plot('#bar-chart', [bar_data], {
        grid  : {
          borderWidth: 1,
          borderColor: '#f3f3f3',
          tickColor  : '#f3f3f3'
        },
        series: {
          bars: {
            show: true, barWidth: 0.5, align: 'center',
          },
        },
        colors: ['#39FF33'],
        xaxis : {
          ticks: [
            @php
            $cuentaRecolector = 1;
            foreach($totalesRecolectores as $recolector) {
            @endphp
              [{{ $cuentaRecolector }},'{{ $recolector["recolector"] }}'],
            @php            
            $cuentaRecolector++;
            }
            @endphp
            ]
        }
      })
      /* END BAR CHART */

      /*
      * BAR CHART
      * ---------
      */

      var bar_data = {
        data : [
          @php
          $cuentaRecolector = 1;
          foreach($totalesRecolectores as $recolector) {
            
          @endphp
            [{{ $cuentaRecolector }}, {{ $recolector["noExitosas"] }}], 
          @php
            $cuentaRecolector++;
          }
          @endphp
          ],
        bars: { show: true }
      }
      $.plot('#bar-chart2', [bar_data], {
        grid  : {
          borderWidth: 1,
          borderColor: '#f3f3f3',
          tickColor  : '#f3f3f3'
        },
        series: {
          bars: {
            show: true, barWidth: 0.5, align: 'center',
          },
        },
        colors: ['#F34444'],
        xaxis : {
          ticks: [
            @php
            $cuentaRecolector = 1;
            foreach($totalesRecolectores as $recolector) {
            @endphp
              [{{ $cuentaRecolector }},'{{ $recolector["recolector"] }}'],
            @php            
            $cuentaRecolector++;
            }
            @endphp
            ]
        }
      })
      /* END BAR CHART */
	  });
	</script>
@endsection
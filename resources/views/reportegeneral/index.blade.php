@extends('layouts.main')

@section('extras-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@inject('rutaClienteLista', 'App\RutaClienteLista')
@inject('user', 'App\User')

@section('content')

<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Reportes') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">

        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  Parametros del Reporte a generar</b>
                </h3>                
              </div>
              
              <div class="card-body">
              <form method="GET" action="{{ route('reporte.general') }}" enctype="multipart/form-data">
    			    @csrf
                <div class="row">
                    <div class="col-md-12">
                    <div class="form-group">
		                  <label>Tipo de Reporte *</label>
		                  <select class="form-control " name="tipo_reporte" required="">
                            <option value="1">Suscripciones de clientes</option>
                            <option value="2">Reporte Recolector/Servicios</option>
                            <option value="3">Reporte Pagos a recolectores</option>
                          </select>
		                </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
		                	<label>Fecha de Inicio *</label>
		                  <input type="date" class="form-control " name="fecha_inicio" id="fecha_inicio" value="{{ date('Y-m-d',strtotime($fecha_inicio)) }}" required="">
		                </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
		                	<label>Fecha de Fin *</label>
		                  <input type="date" class="form-control " name="fecha_fin" id="fecha_fin" max="{{ date('Y-m-d',strtotime($fecha_maxima)) }}" value="{{ date('Y-m-d',strtotime($fecha_fin)) }}" required="">
		                </div>
                  </div>

                  <div class="col-md-6">
                    <input type="submit" class="btn btn-primary" value="Generar Reporte">
                  </div>
                </div>
              </form>
              </div>
              
            </div>
            <!-- /.card -->
          </div>
        </div>        

      </div>
    </section>
    <!-- /.content -->
@endsection

@section('extras-js')
    <!-- DataTables -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

    <script>
	  $(function () {
	    $("#example1").DataTable();
      $("#example2").DataTable();
	  });
	</script>
@endsection
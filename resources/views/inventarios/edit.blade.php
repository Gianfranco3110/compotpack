@extends('layouts.main')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Editar Item de Inventario') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

          	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Datos del Item') }}. <b>Nota:</b> Cantidad expresada por cliente.</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
              
              <form method="POST" action="{{ route('inventarios.update', ['item' => $item->id]) }}" enctype="multipart/form-data">
    			@csrf
                
                <div class="row">
                	<div class="col-md-6">
                		<div class="form-group">
		                	<label>Nombre</label>
		                    <input type="text" value="{{ old('producto', $item->producto) }}" class="form-control @error('producto') is-invalid @enderror" name="producto" id="producto" placeholder="Nombre" required>
		                </div>

		                <div class="form-group">
	                      <label>Tipo de servicio asociado</label>
	                        <select class="form-control @error('tipo_servicio') is-invalid @enderror" name="tipo_servicio" required>
	                          <option value='Recogida Normal' {{ $item->tipo_servicio == 'Recogida Normal' ? 'selected':'' }}>Recogida Normal</option>
                              <option value='Semana Inicial' {{ $item->tipo_servicio == 'Recogida Normal' ? '':'selected' }}>Semana Inicial</option>
	                        </select>
	                    </div>
                	</div>

                	<div class="col-md-6">
                        <div class="form-group">
		                	<label>Cantidad por cliente</label>
		                    <input type="number" value="{{ old('cantidad', $item->cantidad) }}" class="form-control @error('cantidad') is-invalid @enderror" name="cantidad" id="cantidad" placeholder="Cantidad" required>
		                </div>
                	</div>

                	<div class="col-md-6">
                		<input class="btn btn-primary" type="submit" name="submit" value="Guardar">
                		<a href="{{ url()->previous() }}" class="btn btn-danger" type="button">Regresar</a>
                	</div>
                </div>

              </form>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection
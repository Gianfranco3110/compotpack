@extends('layouts.main')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Crear Plan') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

          	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Datos del Plan') }}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
              
              <form method="POST" action="{{ route('plan.store') }}" enctype="multipart/form-data">
    			   @csrf
                
                <div class="row">
                	<div class="col-md-6">
                		<div class="form-group">
		                	<label>Titulo</label>
		                    <input type="text" value="{{ old('titulo') }}" class="form-control @error('titulo') is-invalid @enderror" name="titulo" id="titulo" placeholder="Titulo" required>
		                </div>

		                <div class="form-group">
		                	<label>Descripción</label>
		                    <textarea value="{{ old('descripcion') }}" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" id="descripcion" required>{{ old('descripcion') }}</textarea>
		                </div>
                	</div>

                	<div class="col-md-6">

                		<div class="form-group">
		                	<label>Frecuencia de cobro (Meses)</label>
		                    <input type="text" value="{{ old('frecuencia_cobro') }}" class="form-control @error('frecuencia_cobro') is-invalid @enderror" name="frecuencia_cobro" id="frecuenci_cobro" placeholder="Ej: 1 para un mes." required>
		                </div>

		                <div class="form-group">
		                	<label>Precio</label>
		                    <input type="text" value="{{ old('precio') }}" class="form-control @error('precio') is-invalid @enderror" name="precio" id="precio" placeholder="Precio" required>
		                </div>

		                <div class="form-group">
	                      <div class="custom-control custom-checkbox">
	                        <input class="custom-control-input" type="checkbox" id="customCheckbox1" value="recurrente" name="recurrente">
	                        <label for="customCheckbox1" class="custom-control-label">Activar Cobro recurrentes.</label>
	                      </div>
	                    </div>
		                
                	</div>

                	<div class="col-md-6">
                		<input class="btn btn-primary" type="submit" name="submit" value="Registrar">
                		<a href="{{ url()->previous() }}" class="btn btn-danger" type="button">Regresar</a>
                	</div>
                </div>

              </form>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection
@extends('layouts.main')

@section('extras-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Administrador de Recolectores') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

          	@if ($message = Session::get('success'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
			        <strong>{{ $message }}</strong>
			</div>
			@endif

            <!-- Default box -->
            <a href="{{ route('recolector.create') }}" class="btn btn-primary">Agregar Recolector</a>
            <br><br>
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Listado de Recolectores') }}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">

	              <table id="example1" class="table table-bordered table-striped">
	                <thead>
	                <tr>
	                  <th>Nombre</th>
	                  <th>Email</th>
	                  <th>Estado</th>
	                  <th>Acciones</th>
	                </tr>
	                </thead>
	                <tbody>
	                @foreach ($recolectores as $recolector)
	                <tr>
	                  <td>{{ $recolector->name }} <br> <img style="max-width:20px;" src="{{ asset('uploads/usuarios/'.$recolector->image) }}"></td>
	                  <td>{{ $recolector->email }}</td>
	                  <td>{{ $recolector->status==1 ? 'Activo':'Oculto' }}</td>
	                  <td>
	                  	<div class="btn-group">
						  <a href="{{ route('pagos.recolector', ['recolector' => $recolector->id]) }}">
	                        	<button type="button" class="btn btn-default" title="Pagos"><i class="fas fa-money-bill-alt"></i></button>
	                        </a>
	                  		<a href="{{ route('recolector.edit', ['recolector' => $recolector->id]) }}">
	                        	<button type="button" class="btn btn-default" title="Editar"><i class="nav-icon fas fa-edit"></i></button>
	                        </a>
	                        <a href="{{ route('recolector.setStatus', ['recolector' => $recolector->id]) }}">
	                        	<button type="button" class="btn btn-default" title="{{ $recolector->status==1 ? 'Ocultar':'Activar' }}"><i class="nav-icon fas fa-{{ $recolector->status==1 ? 'ban':'check-circle' }}"></i></button>
	                        </a>
	                        <a href="{{ route('recolector.destroy', ['recolector' => $recolector->id]) }}" onclick="return confirm('¿Estas seguro de querer eliminar este registro?. Se perderan todos los datos relacionados al mismo.')">
	                        	<button type="button" class="btn btn-default" title="Eliminar"><i class="nav-icon fas fa-trash-alt"></i></button>
	                        </a>
	                      </div>
	                  </td>
	                </tr>
	                @endforeach
	                </tbody>
	                <tfoot>
	                <tr>
	                  <th>Nombre</th>
	                  <th>Email</th>
	                  <th>Estado</th>
	                  <th>Acciones</th>
	                </tr>
	                </tfoot>
	            </table>


              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection

@section('extras-js')
    <!-- DataTables -->
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('AdminLTE-3.0.2/AdminLTE-3.0.2/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

    <script>
	  $(function () {
	    $("#example1").DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        }
      });
	    $('#example2').DataTable({
	      "paging": true,
	      "lengthChange": false,
	      "searching": false,
	      "ordering": true,
	      "info": true,
	      "autoWidth": false,
	    });
	  });
	</script>
@endsection

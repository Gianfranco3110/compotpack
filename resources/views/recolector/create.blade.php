@extends('layouts.main')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('Crear de Recolector') }}</h1>
          </div>
          <div class="col-sm-6">
            <!--ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

          	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('Datos del Recolector') }}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">

              <form method="POST" action="{{ route('recolector.store') }}" enctype="multipart/form-data">
    			@csrf

                <div class="row">
                	<div class="col-md-6">
                		<div class="form-group">
		                	<label>Nombre completo</label>
		                    <input type="text" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Nombre Completo" required>
		                </div>

		                <div class="form-group">
		                	<label>Email</label>
		                    <input type="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Email" required>
		                </div>

		                <div class="form-group">
		                	<label>Dirección</label>
		                    <input type="text" value="{{ old('direccion') }}" class="form-control @error('direccion') is-invalid @enderror" name="direccion" id="direccion" placeholder="Dirección" required>
		                </div>

		                <div class="form-group">
		                    <label>Foto</label>
		                    <div class="input-group">
		                      <div class="custom-file">
		                        <input type="file" class="custom-file-input @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" accept="image/x-png,image/gif,image/jpeg">
		                        <label class="custom-file-label" for="exampleInputFile" data-browse="Seleccionar">Buscar Foto</label>
		                      </div>
		                    </div>
		                </div>

                    <div class="form-group">
		                    <label>Foto Licencia Conducción</label>
		                    <div class="input-group">
		                      <div class="custom-file">
		                        <input type="file" class="custom-file-input @error('image') is-invalid @enderror" name="image-licencia" value="{{ old('image-licencia') }}" accept="image/x-png,image/gif,image/jpeg">
		                        <label class="custom-file-label" for="exampleInputFile" data-browse="Seleccionar">Buscar Foto</label>
		                      </div>
		                    </div>
		                </div>

                    <div class="form-group">
		                    <label>Foto SOAT</label>
		                    <div class="input-group">
		                      <div class="custom-file">
		                        <input type="file" class="custom-file-input @error('image') is-invalid @enderror" name="image-soat" value="{{ old('image-soat') }}" accept="image/x-png,image/gif,image/jpeg">
		                        <label class="custom-file-label" for="exampleInputFile" data-browse="Seleccionar">Buscar Foto</label>
		                      </div>
		                    </div>
		                </div>

		                <div class="form-group">
		                	<label>Teléfono</label>
		                    <input type="text" value="{{ old('telefono') }}" class="form-control @error('telefono') is-invalid @enderror" name="telefono" id="telefono" placeholder="Teléfono" required>
		                </div>

						<div class="form-group">
		                	<label>Capacidad Diaria</label>
		                    <input type="number" value="{{ old('capacidad') }}" class="form-control @error('capacidad') is-invalid @enderror" name="capacidad" id="capacidad" placeholder="Capacidad semanal del recolector" required>
		                </div>


                	</div>

                	<div class="col-md-6">

						<div class="form-group">
		                	<label>Whatsapp</label>
		                    <input type="text" value="{{ old('whatsapp') }}" class="form-control @error('whatsapp') is-invalid @enderror" name="whatsapp" id="whatsapp" onkeypress='return validaNumericos(event)' maxlength="10" placeholder="Whatsapp" required>
		                </div>

		                <div class="form-group">
		                	<label>Vehiculo</label>
		                    <input type="text" value="{{ old('vehiculo') }}" class="form-control @error('vehiculo') is-invalid @enderror" name="vehiculo" id="vehiculo" placeholder="Vehiculo" required>
		                </div>

		                <div class="form-group">
		                	<label>Color del Vehiculo</label>
		                    <input type="text" value="{{ old('color') }}" class="form-control @error('color') is-invalid @enderror" name="color" id="color" placeholder="Color del Vehiculo" required>
		                </div>

                		<div class="form-group">
		                	<label>Placa</label>
		                    <input type="text" value="{{ old('placa') }}" class="form-control @error('placa') is-invalid @enderror" name="placa" id="placa" placeholder="Placa" required>
		                </div>

                		<div class="form-group">
		                	<label>Password</label>
		                    <input type="password" value="{{ old('password') }}" class="form-control @error('password') is-invalid @enderror" name="password" id="password" required>
		                </div>

		                <div class="form-group">
		                	<label>Confirmación de Password</label>
		                    <input type="password" value="{{ old('password_confirmation') }}" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="password_confirmation" required>
		                </div>
                	</div>

                  <div class="col-md-12">
                    <label>Días de recolección</label>
                    <div class="form-group">

                      @foreach ($semana as $key => $dia)
                      <div class="form-check">
                        <input class="form-check-input @error('dia_recoleccion') is-invalid @enderror" type="checkbox" name="dia_recoleccion[]" id="exampleRadios{{ $key }}" value="{{ $key }}" checked>
                        <label class="form-check-label" for="exampleRadios{{ $key }}">
                          {{ $dia }}
                        </label>
                      </div>
                      @endforeach
                    </div>
                  </div>

                	<div class="col-md-6">
                		<input class="btn btn-primary" type="submit" name="submit" value="Registrar">
                		<a href="{{ url()->previous() }}" class="btn btn-danger" type="button">Regresar</a>
                	</div>
                </div>

              </form>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
      <script>
        $(function () {
          $('.custom-file-input').on('change', function(event) {
              var inputFile = event.currentTarget;
              $(inputFile).parent()
                  .find('.custom-file-label')
                  .html(inputFile.files[0].name);
          });
        });
      </script>
    </section>
    <!-- /.content -->
@endsection

-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Servidor: dbg-rds-whm.crzp3cuufitp.us-east-1.rds.amazonaws.com:3306
-- Tiempo de generación: 09-08-2022 a las 08:00:44
-- Versión del servidor: 5.7.33-log
-- Versión de PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `compostp_act01302021`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuration`
--

CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `value` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `configuration`
--

INSERT INTO `configuration` (`id`, `title`, `value`) VALUES
(1, 'Cantidad maxima de clientes por ruta', '8'),
(2, 'Distancia minima para recoleccion', '100');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `control_estado_novedad`
--

CREATE TABLE `control_estado_novedad` (
  `user_id` int(11) NOT NULL,
  `fecha_inicio` varchar(50) NOT NULL,
  `fecha_final` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `control_estado_novedad`
--

INSERT INTO `control_estado_novedad` (`user_id`, `fecha_inicio`, `fecha_final`) VALUES
(100, '2021-09-14', '2021-09-21'),
(101, '2021-09-15', '2021-09-22'),
(102, '2021-09-16', '2021-09-23'),
(103, '2021-09-16', '2021-09-23'),
(104, '2021-09-16', '2021-09-23'),
(105, '2021-09-16', '2021-09-23'),
(106, '2021-09-16', '2021-09-23'),
(107, '2021-09-16', '2021-09-23'),
(108, '2021-09-16', '2021-09-23'),
(109, '2021-09-16', '2021-09-23'),
(110, '2021-09-15', '2021-09-22'),
(111, '2021-09-14', '2021-09-21'),
(112, '2021-09-14', '2021-09-21'),
(114, '2021-09-14', '2021-09-21'),
(115, '2021-09-14', '2021-09-21'),
(116, '2021-09-14', '2021-09-21'),
(117, '2021-09-15', '2021-09-22'),
(118, '2021-09-15', '2021-09-22'),
(119, '2021-09-15', '2021-09-22'),
(129, '2021-09-16', '2021-09-23'),
(130, '2021-09-16', '2021-09-23'),
(131, '2021-09-16', '2021-09-23'),
(132, '2021-09-16', '2021-09-23'),
(133, '2021-09-16', '2021-09-23'),
(134, '2021-09-16', '2021-09-23'),
(137, '2021-09-16', '2021-09-23'),
(138, '2021-09-16', '2021-09-23'),
(143, '2021-09-14', '2021-09-21'),
(154, '2021-09-16', '2021-09-23'),
(164, '2021-12-02', '2021-12-09'),
(166, '2021-09-23', '2021-09-30'),
(167, '2021-12-02', '2021-12-09'),
(169, '2021-12-02', '2021-12-09'),
(170, '2021-12-01', '2021-12-08'),
(172, '2021-12-02', '2021-12-09'),
(178, '2021-12-03', '2021-12-10'),
(181, '2021-12-04', '2021-12-11'),
(182, '2021-12-03', '2021-12-10'),
(183, '2021-12-04', '2021-12-11'),
(184, '2021-12-04', '2021-12-11'),
(186, '2021-12-01', '2021-12-08'),
(187, '2022-02-19', '2022-02-26'),
(190, '2022-02-09', '2022-02-16'),
(191, '2022-02-04', '2022-02-11'),
(192, '2022-02-04', '2022-02-11'),
(197, '2022-02-09', '2022-02-16'),
(202, '2022-02-09', '2022-02-16'),
(203, '2022-03-24', '2022-03-31'),
(205, '2022-02-09', '2022-02-16'),
(212, '2022-02-19', '2022-02-26'),
(228, '2022-03-24', '2022-03-31'),
(230, '2022-03-27', '2022-04-03'),
(232, '2022-03-27', '2022-04-03'),
(233, '2022-03-27', '2022-04-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `control_recoleccion_diario`
--

CREATE TABLE `control_recoleccion_diario` (
  `id` int(11) NOT NULL,
  `dia` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `capacidad_max_recolecta` int(10) UNSIGNED NOT NULL,
  `cantidad_clientes_subscritos` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `control_recoleccion_diario`
--

INSERT INTO `control_recoleccion_diario` (`id`, `dia`, `capacidad_max_recolecta`, `cantidad_clientes_subscritos`) VALUES
(1, 'lunes', 29, 3),
(2, 'martes', 29, 4),
(3, 'miercoles', 29, 29),
(4, 'jueves', 29, 4),
(5, 'viernes', 29, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_suscripciones_epayco`
--

CREATE TABLE `detalles_suscripciones_epayco` (
  `id_suscripcion` int(11) NOT NULL,
  `token` text NOT NULL,
  `customer_id` varchar(256) NOT NULL,
  `subscription_id` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `detalles_suscripciones_epayco`
--

INSERT INTO `detalles_suscripciones_epayco` (`id_suscripcion`, `token`, `customer_id`, `subscription_id`) VALUES
(1, '0b94a988ffd5b2cbd4fc6f3', '0b9352ebbc21d7016156fa7', '0b94a9a8ffd5b2cbd4fc6f4'),
(2, '0d0f5f2c9c641576270b575', '0d0f5f2e07314083615ee9c', '0d0f5f3e07314083615ee9d'),
(3, '0d3cbb0fcb9d94ca27b6cd3', '0b9352ebbc21d7016156fa7', '0b94a9a8ffd5b2cbd4fc6f4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dias_festivos`
--

CREATE TABLE `dias_festivos` (
  `id` int(11) NOT NULL,
  `year` varchar(4) NOT NULL,
  `month` varchar(2) NOT NULL,
  `day` varchar(2) NOT NULL,
  `event` varchar(256) NOT NULL,
  `fecha_reemplazo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `dias_festivos`
--

INSERT INTO `dias_festivos` (`id`, `year`, `month`, `day`, `event`, `fecha_reemplazo`) VALUES
(29, '2022', '01', '01', 'Año Nuevo', '2022-01-03'),
(31, '2022', '03', '21', 'Dia de San José', '2022-03-19'),
(33, '2022', '04', '15', 'Viernes Santo', '2022-04-16'),
(34, '2022', '05', '30', 'Dia de la Ascensión', '2022-05-28'),
(35, '2022', '06', '20', 'Corpus Cristi', '2022-06-18'),
(36, '2022', '06', '27', 'Sagrado Corazón', '2022-06-25'),
(37, '2022', '08', '15', 'La asunción de la Virgen', '2022-08-13'),
(38, '2022', '10', '17', 'Dia de la Raza', '2022-10-15'),
(39, '2022', '07', '04', 'San Pedro', '2022-07-02'),
(40, '2022', '07', '20', 'Dia de la Independencia', '2022-07-23'),
(41, '2022', '11', '07', 'Dia de los Difuntos', '2022-11-05'),
(42, '2022', '11', '14', 'Independencia de Cartagena', '2022-11-12'),
(43, '2022', '12', '08', 'Dia de Velitas', '2022-12-10'),
(44, '2022', '03', '03', 'PRUEBA', '2022-03-05'),
(45, '2022', '02', '28', 'PRUEBA 2', '2022-02-26'),
(46, '2022', '03', '09', 'PRUEBA 3', '2022-03-12'),
(47, '2022', '03', '09', 'PRUEBA 3', '2022-03-12'),
(48, '2022', '03', '25', 'PRUEBA 4', '2022-03-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_recoleccion`
--

CREATE TABLE `estados_recoleccion` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estados_recoleccion`
--

INSERT INTO `estados_recoleccion` (`id`, `titulo`) VALUES
(1, 'Recogida'),
(2, 'Novedad'),
(3, 'Programado para recolección');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_pagos`
--

CREATE TABLE `historial_pagos` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `fecha` varchar(50) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `x_ref_payco` varchar(100) NOT NULL,
  `x_transaction_id` varchar(100) NOT NULL,
  `x_amount` varchar(100) NOT NULL,
  `x_currency_code` varchar(100) NOT NULL,
  `x_response` varchar(100) NOT NULL,
  `x_response_reason_text` varchar(100) NOT NULL,
  `x_id_invoice` varchar(100) NOT NULL,
  `x_approval_code` varchar(100) NOT NULL,
  `id_plan` varchar(100) NOT NULL,
  `titulo_plan` varchar(100) DEFAULT NULL,
  `con_kit_inicial` tinyint(4) NOT NULL DEFAULT '0',
  `identifier` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historial_pagos`
--

INSERT INTO `historial_pagos` (`id`, `id_cliente`, `fecha`, `estado`, `x_ref_payco`, `x_transaction_id`, `x_amount`, `x_currency_code`, `x_response`, `x_response_reason_text`, `x_id_invoice`, `x_approval_code`, `id_plan`, `titulo_plan`, `con_kit_inicial`, `identifier`) VALUES
(4, 126, '2021-07-19 14:43:28', 'Rechazada', '55854915', '55854915', '21000', 'COP', 'Rechazada', '04-Tarjeta restringida por el centro de autorizaciones', '126_2_1626705677', '000000', '2', 'PLAN SEMESTRAL', 0, '1626705677'),
(5, 126, '2021-07-19 14:46:38', 'Rechazada', '55855617', '55855617', '21000', 'COP', 'Rechazada', '04-Tarjeta restringida por el centro de autorizaciones', '126_2_1626705846', '000000', '2', 'PLAN SEMESTRAL', 0, '1626705846'),
(6, 126, '2021-07-19 15:03:29', 'Rechazada', '55861031', '55861031', '21000', 'COP', 'Rechazada', '04-Tarjeta restringida por el centro de autorizaciones', '126_2_1626706942', '000000', '2', 'PLAN SEMESTRAL', 0, '1626706942'),
(7, 126, '2021-07-20 00:47:49', 'Rechazada', '55905024', '55905024', '21000', 'COP', 'Rechazada', '04-Tarjeta restringida por el centro de autorizaciones', '126_2_1626724027', '000000', '2', 'PLAN SEMESTRAL', 0, '1626724027'),
(9, 137, '2021-07-29 17:57:56', 'Aprobada', '70ce7188dd24cfc3f98f9669', '133374453', '78000', 'COP', 'Aceptada', '00-Aprobada', '137_1_1627581351', 'T08771', '1', 'PLAN TRIMESTRAL', 0, '1627581351'),
(10, 138, '2021-07-29 18:40:44', 'Aprobada', '9d2d332a8f84577e258e854d', '133383651', '20000', 'COP', 'Aceptada', '00-Aprobada', '138_2_1627583880', 'T01099', '2', 'PLAN SEMESTRAL', 0, '1627583880'),
(11, 138, '2021-07-29 19:00:59', 'Aprobada', '04459487d66db69a9336f215', '568889611627585165', '20000', 'COP', 'Aceptada', '00-Aprobada', '138_1_1627585125', '1075621513', '1', 'PLAN TRIMESTRAL', 0, '1627585125'),
(12, 138, '2021-08-02 21:39:42', 'Aprobada', 'ee97e0d25fc1de843d94190e', '332248413', '20000', 'COP', 'Aceptada', '00-Aprobada', '138_1_1627940334', 'T03337', '1', 'PLAN TRIMESTRAL', 0, '1627940334'),
(13, 143, '2021-08-04 04:59:31', 'Aprobada', '4a470f04d86fa8c35112d900', '57530144', '46000', 'COP', 'Aceptada', '00-Aprobada', '143_1_1628053137', '000000', '1', 'PLAN TRIMESTRAL', 0, '1628053137'),
(14, 138, '2021-08-06 21:37:21', 'Aprobada', '0a9b528fca20e74dc2b9abd8', '333357430', '20000', 'COP', 'Aceptada', '00-Aprobada', '138_1_1628285804', 'T03721', '1', 'PLAN TRIMESTRAL', 0, '1628285804'),
(15, 143, '2021-08-31 02:39:49', 'Aprobada', 'c27afad27af8697678495a4c', '60122862', '20000', 'COP', 'Aceptada', '00-Aprobada', '143_1_1630377538', '000000', '1', 'PLAN TRIMESTRAL', 0, '1630377538'),
(16, 158, '2021-08-31 15:04:50', 'Rechazada', '7dc40fd8bb0a3f882e210567', '60165515', '22000', 'COP', 'Rechazada', '04-Tarjeta restringida por el centro de autorizaciones', '158_1_1630422247', '000000', '1', 'PLAN TRIMESTRAL', 0, '1630422247'),
(17, 169, '2021-09-26 04:48:01', 'Aprobada', '31a42cfe1f56ce59dbab2f38', '62868744', '22000', 'COP', 'Aceptada', 'Aprobada', '169_1_1632631634', '000000', '1', 'PLAN TRIMESTRAL', 0, '1632631634'),
(18, 172, '2021-09-27 15:37:16', 'Aprobada', 'beef7019e006ed6ce2fc7e6b', '15124', '20000', 'COP', 'Aceptada', 'Aprobada', '172_1_1632756996', 'T06277', '1', 'PLAN TRIMESTRAL', 0, '1632756996'),
(19, 178, '2021-10-01 14:20:01', 'Aprobada', '958cc862b5069f5647d02c04', '343778373', '22000', 'COP', 'Aceptada', 'Aprobada', '178_2_1633097913', 'T09553', '2', 'PLAN SEMESTRAL', 0, '1633097913'),
(20, 180, '2021-10-05 14:46:33', 'Aprobada', '930fa9071c4dbebb33469242', '146749638', '22000', 'COP', 'Aceptada', 'Aprobada', '180_1_1633445150', 'T01203', '1', 'PLAN TRIMESTRAL', 0, '1633445150'),
(21, 184, '2021-10-08 15:44:20', 'Aprobada', '001ac7eb28cf9478d60c0036', '147469596', '20000', 'COP', 'Aceptada', 'Aprobada', '184_1_1633707805', 'T01927', '1', 'PLAN TRIMESTRAL', 0, '1633707805'),
(22, 185, '2021-10-08 15:48:43', 'Rechazada', 'bf0d172be909a63ce5ad4740', '643519091633708149', '22000', 'COP', 'Pendiente', 'Transacción en proceso de verificación', '185_2_1633708028', '000000', '2', 'PLAN SEMESTRAL', 0, '1633708028'),
(23, 186, '2021-10-22 15:23:36', 'Rechazada', '238ead04bd6667c145e7af0a', '658656211634916255', '22000', 'COP', 'Pendiente', 'Transacción en proceso de verificación', '186_2_1634916098', '000000', '2', 'PLAN SEMESTRAL', 0, '1634916098'),
(24, 186, '2021-10-22 15:26:05', 'Aprobada', '586fbd0a88d409e140acdfbe', '658658201634916284', '22000', 'COP', 'Aceptada', 'Aprobada', '186_2_1634916250', '1175028211', '2', 'PLAN SEMESTRAL', 0, '1634916250'),
(25, 187, '2021-12-09 16:54:26', 'Rechazada', '3d1d4c3b249c4ef79acfa3c2', '712414401639068914', '22000', 'COP', 'Pendiente', 'Transacción en proceso de verificación', '187_1_1639068593', '000000', '1', 'PLAN TRIMESTRAL', 0, '1639068593'),
(26, 187, '2021-12-09 19:03:45', 'Aprobada', 'acfeba8fc304ce12ee3e0df2', '712612441639076558', '22000', 'COP', 'Aceptada', 'Aprobada', '187_1_1639076390', '1238131592', '1', 'PLAN TRIMESTRAL', 0, '1639076390'),
(27, 190, '2021-12-09 20:02:36', 'Aprobada', '1b70708c91f6210b6b59d109', '712685971639080093', '22000', 'COP', 'Aceptada', 'Aprobada', '190_1_1639080049', '1238248025', '1', 'PLAN TRIMESTRAL', 0, '1639080049'),
(28, 200, '2022-01-16 15:20:09', 'Rechazada', '524410e5de3f41b817f7568c', '752694861642346467', '88000', 'COP', 'Pendiente', 'Transacción en proceso de verificación', '200_2_1642346218', '000000', '2', 'PLAN MENSUAL', 0, '1642346218'),
(29, 200, '2022-01-16 15:29:27', 'Rechazada', '3857d97f9eb55f46c7a9d6c5', '752700971642346898', '88000', 'COP', 'Pendiente', 'Redireccionando al banco', '200_2_1642346840', '1286560557', '2', 'PLAN MENSUAL', 0, '1642346840'),
(30, 200, '2022-01-16 15:36:55', 'Aprobada', '44542a20289c97c28687b924', '163040272', '88000', 'COP', 'Aceptada', 'Aprobada', '200_2_1642347333', '358623', '2', 'PLAN MENSUAL', 0, '1642347333'),
(31, 201, '2022-01-16 16:07:13', 'Aprobada', 'dd295cc54f5c37f27746a40b', '366766315', '205000', 'COP', 'Aceptada', 'Aprobada', '201_1_1642348892', '364209', '1', 'PLAN TRIMESTRAL', 0, '1642348892'),
(32, 209, '2022-02-09 21:54:59', 'Rechazada', '0ede512e809be60009598e97', '779322981644443689', '50000', 'COP', 'Pendiente', 'Transacción en proceso de verificación', '209_2_1644443559', '000000', '2', 'PLAN MENSUAL', 0, '1644443559'),
(33, 210, '2022-02-09 22:29:10', 'Rechazada', '6ad96506ad637caf2e41690b', '779359401644445772', '20000', 'COP', 'Pendiente', 'Transacción en proceso de verificación', '210_1_1644444508', '000000', '1', 'PLAN TRIMESTRAL', 0, '1644444508'),
(34, 211, '2022-02-09 22:41:11', 'Aprobada', '091eb5e0d3dc69cb9366a350', '779370981644446400', '20000', 'COP', 'Aceptada', 'Aprobada', '211_2_1644446366', '1320324194', '2', 'PLAN MENSUAL', 0, '1644446366'),
(35, 212, '2022-02-09 22:45:41', 'Aprobada', '13addcbd4315f03ee41a8eb2', '779375551644446672', '50000', 'COP', 'Aceptada', 'Aprobada', '212_1_1644446609', '1320330762', '1', 'PLAN TRIMESTRAL', 0, '1644446609'),
(36, 210, '2022-02-15 16:59:31', 'Aceptada', '1be18f43c2d69814a09d59fd', '48771723400989', '50000', 'COP', 'Aprobada', 'Esperando pago del cliente en punto de servicio Baloto', '210_1_1644944278', '000000', '1', 'PLAN TRIMESTRAL', 0, '1644944278'),
(37, 216, '2022-02-17 15:38:52', 'Aceptada', '3147c0adf9b2e2204497c3de', '787372831645112328', '20000', 'COP', 'Aprobada', 'Transacción en proceso de verificación', '216_1_1645112281', '000000', '1', 'PLAN TRIMESTRAL', 0, '1645112281'),
(38, 223, '2022-02-20 00:32:19', 'Aprobada', 'ba3715c7704363dc9672b52a', '790086171645317072', '50000', 'COP', 'Aceptada', 'Aprobada', '223_1_1645317038', '1334391556', '1', 'PLAN TRIMESTRAL', 0, '1645317038'),
(39, 226, '2022-02-20 01:03:13', 'Aceptada', '40393c3c40db95872a1448db', '790109101645318997', '20000', 'COP', 'Aprobada', 'Transacción en proceso de verificación', '226_2_1645318952', '000000', '2', 'PLAN MENSUAL', 0, '1645318952'),
(40, 227, '2022-02-20 01:55:28', 'Aprobada', '6aa4535008de422fef70e5b1', '790140771645322079', '20000', 'COP', 'Aceptada', 'Aprobada', '227_2_1645322014', '1334451881', '2', 'PLAN MENSUAL', 0, '1645322014'),
(41, 228, '2022-02-21 16:29:58', 'Aprobada', 'e23b4c4d742fbe53a4486bc4', '791225821645460926', '20000', 'COP', 'Aceptada', 'Aprobada', '228_1_1645460870', '1335663217', '1', 'PLAN TRIMESTRAL', 0, '1645460870'),
(42, 233, '2022-02-21 21:13:59', 'Aceptada', 'a1f7b71df46db149caada7ee', '791640651645478116', '20000', 'COP', 'Aprobada', 'Transacción en proceso de verificación', '233_2_1645478006', '000000', '2', 'PLAN MENSUAL', 0, '1645478006'),
(43, 234, '2022-02-21 22:08:54', 'Aceptada', '289ae3e0296c04572940b9cb', '791717771645481407', '20000', 'COP', 'Aprobada', 'Transacción en proceso de verificación', '234_2_1645480993', '000000', '2', 'PLAN MENSUAL', 0, '1645480993'),
(44, 235, '2022-03-29 18:18:41', 'Aprobada', 'bd4bc559078f36033c72d53c', '176015287', '50000', 'COP', 'Aceptada', 'Aprobada', '235_2_1648577735', '134668', '2', 'PLAN MENSUAL', 0, '1648577735'),
(45, 239, '2022-04-19 16:13:44', 'Aprobada', 'ac20825ae91e2fc97714ad95', '179174241', '20000', 'COP', 'Aceptada', 'Aprobada', '239_1_1650384771', '099969', '1', 'PLAN TRIMESTRAL', 0, '1650384771');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_pago_recolector`
--

CREATE TABLE `historial_pago_recolector` (
  `id` int(11) NOT NULL,
  `id_recolector` int(11) NOT NULL,
  `fecha` varchar(50) NOT NULL,
  `carreras_pagadas` int(11) NOT NULL,
  `monto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_recoleccion`
--

CREATE TABLE `historial_recoleccion` (
  `id` int(11) NOT NULL,
  `id_ruta_cliente_lista` int(11) NOT NULL,
  `distancia_recoleccion` varchar(50) NOT NULL DEFAULT '0',
  `latitud_recoleccion` varchar(126) DEFAULT NULL COMMENT 'Latitud del recolector',
  `longitud_recoleccion` varchar(126) DEFAULT NULL COMMENT 'Longitud del recolector',
  `id_estado_recoleccion` int(11) NOT NULL,
  `dia` varchar(50) NOT NULL COMMENT 'Fecha cuando se cambio el estado de esta ruta de cliente',
  `hora` varchar(50) NOT NULL COMMENT 'Hora del servidor cuando se cambio el estado de esta ruta de cliente'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historial_recoleccion`
--

INSERT INTO `historial_recoleccion` (`id`, `id_ruta_cliente_lista`, `distancia_recoleccion`, `latitud_recoleccion`, `longitud_recoleccion`, `id_estado_recoleccion`, `dia`, `hora`) VALUES
(1, 7, '0', NULL, NULL, 2, '2021-05-28', '18:31:29'),
(2, 4, '395809.97', NULL, NULL, 1, '2021-05-28', '20:38:18'),
(3, 17, '0', NULL, NULL, 1, '2021-05-29', '15:01:38'),
(4, 20, '5831.06', NULL, NULL, 1, '2021-06-03', '18:28:05'),
(5, 21, '84.86', NULL, NULL, 2, '2021-06-03', '18:28:50'),
(6, 35, '0', NULL, NULL, 1, '2021-06-22', '16:16:49'),
(7, 79, '0', NULL, NULL, 2, '2021-07-12', '15:12:55'),
(8, 81, '0', NULL, NULL, 2, '2021-07-12', '15:14:14'),
(9, 149, '0', NULL, NULL, 1, '2021-07-31', '13:07:46'),
(10, 236, '0', NULL, NULL, 1, '2021-08-25', '14:21:48'),
(11, 235, '0', NULL, NULL, 2, '2021-08-25', '14:22:12'),
(12, 256, '0', NULL, NULL, 1, '2021-08-27', '15:21:33'),
(13, 257, '0', NULL, NULL, 1, '2021-08-27', '15:21:44'),
(14, 258, '0', NULL, NULL, 1, '2021-08-27', '15:21:53'),
(15, 259, '0', NULL, NULL, 2, '2021-08-27', '15:22:12'),
(16, 307, '0', NULL, NULL, 1, '2021-09-07', '15:22:10'),
(17, 312, '0', NULL, NULL, 1, '2021-09-08', '15:32:04'),
(18, 400, '790.4', '4.71112408575684', '-74.0481356', 1, '2021-10-06', '03:32:03'),
(19, 406, '314.5', '4.7411353', '-74.0416562', 1, '2021-10-11', '13:47:26'),
(20, 407, '4305.6', '4.7411351', '-74.0416452', 1, '2021-10-11', '13:52:03'),
(21, 408, '4800.9', '4.7411351', '-74.0416452', 2, '2021-10-11', '13:52:56'),
(22, 422, '3778.3', '4.7411367', '-74.0416497', 1, '2021-10-11', '14:31:36'),
(23, 411, '7450.4', '4.7411268', '-74.0416496', 1, '2021-10-12', '16:36:26'),
(24, 423, '5220.8', '4.6972418', '-74.0921181', 1, '2021-10-13', '14:47:38'),
(25, 415, '3844.3', '4.7411364', '-74.0416416', 2, '2021-10-13', '20:36:54'),
(26, 413, '3778.1', '4.7411364', '-74.0416416', 2, '2021-10-13', '20:37:13'),
(27, 414, '3881.4', '4.7411364', '-74.0416416', 1, '2021-10-13', '20:37:20'),
(28, 416, '0', NULL, NULL, 2, '2021-10-13', '20:41:46'),
(29, 418, '5189.6', '4.7411388', '-74.0416412', 2, '2021-10-14', '14:09:20'),
(30, 419, '1344.7', '4.7411404', '-74.0416415', 2, '2021-10-15', '19:12:08'),
(31, 420, '2336.2', '4.7411444', '-74.0416399', 2, '2021-10-15', '19:12:23'),
(32, 424, '3845.4', '4.7411463', '-74.041641', 1, '2021-10-15', '19:12:37'),
(33, 433, '3807.4', '4.7402676', '-74.0407088', 2, '2021-10-20', '15:00:09'),
(34, 435, '445.9', '4.7017439', '-74.0467198', 1, '2021-10-20', '14:59:48'),
(35, 434, '577.0', '4.7017439', '-74.0467198', 1, '2021-10-20', '15:00:12'),
(36, 432, '1574.6', '4.7017439', '-74.0467198', 1, '2021-10-20', '15:42:45'),
(37, 441, '2101.9', '4.6986315', '-74.0310917', 1, '2021-10-21', '14:49:54'),
(38, 436, '0', NULL, NULL, 1, '2021-10-23', '00:34:09'),
(39, 450, '2027.5', '4.7065366', '-74.0298637', 1, '2021-10-27', '14:27:23'),
(40, 452, '21.6', '4.6979085', '-74.0450052', 1, '2021-10-27', '14:45:59'),
(41, 451, '63.7', '4.7071833', '-74.0464733', 1, '2021-10-27', '15:04:43'),
(42, 449, '542.4', '4.7126474', '-74.032378', 1, '2021-10-27', '15:26:27'),
(43, 453, '0', NULL, NULL, 2, '2021-10-28', '13:19:31'),
(44, 454, '0', NULL, NULL, 1, '2021-10-28', '14:24:13'),
(45, 568, '0', NULL, NULL, 1, '2021-12-21', '15:23:57'),
(46, 569, '0', NULL, NULL, 1, '2021-12-21', '16:41:14'),
(47, 563, '999.4', '4.7411119', '-74.0416395', 1, '2021-12-21', '19:17:45'),
(48, 564, '3737.0', '4.7411069', '-74.0416357', 1, '2021-12-21', '19:18:02'),
(49, 565, '3841.1', '4.7411069', '-74.0416357', 1, '2021-12-21', '19:18:23'),
(50, 648, '34.2', '4.7189885', '-74.0479839', 1, '2022-02-02', '13:09:17'),
(51, 649, '2379.9', '4.7189885', '-74.0479839', 1, '2022-02-02', '13:09:28'),
(52, 663, '5881.2', '4.7120084', '-74.0827015', 1, '2022-02-09', '23:15:51'),
(53, 664, '4083.4', '4.7120113', '-74.0826998', 1, '2022-02-09', '23:16:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventarios`
--

CREATE TABLE `inventarios` (
  `id` int(11) NOT NULL,
  `tipo_servicio` varchar(50) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `producto` varchar(50) NOT NULL,
  `precio` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `inventarios`
--

INSERT INTO `inventarios` (`id`, `tipo_servicio`, `cantidad`, `producto`, `precio`) VALUES
(1, 'Recogida Normal', 1, 'Bolsas M', '2000'),
(2, 'Recogida Normal', 1, 'Bolsa especial', '10000'),
(3, 'Semana Inicial', 1, 'Kit Inicial', '33000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('quirkjnthn@gmail.com', '$2y$10$yksvgrmRksL.tRTAe8I6QuGPwYFsAxyebVQ10zbG3x0Rve3an4jnC', '2020-09-09 22:57:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan`
--

CREATE TABLE `plan` (
  `id` int(11) NOT NULL,
  `id_plan_epayco` varchar(256) DEFAULT NULL COMMENT 'Referenci del id_plan que se guarda al crear un plan recurrente en epayco. Esto es solo para los planes recurrente',
  `titulo` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` int(100) NOT NULL,
  `frecuencia_cobro` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `recurrente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `plan`
--

INSERT INTO `plan` (`id`, `id_plan_epayco`, `titulo`, `descripcion`, `precio`, `frecuencia_cobro`, `status`, `recurrente`) VALUES
(1, NULL, 'PLAN TRIMESTRAL', 'Recogeremos tus residuos compostables durante doce (12) semanas.', 20000, 3, 1, 0),
(2, NULL, 'PLAN MENSUAL', 'Recogeremos tus residuos compostables durante cuatro (4) semanas.', 69900, 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recolector_dia_asignado`
--

CREATE TABLE `recolector_dia_asignado` (
  `id` int(11) NOT NULL,
  `id_ruta` int(11) NOT NULL,
  `id_recolector` int(11) NOT NULL,
  `dia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recolector_dia_asignado`
--

INSERT INTO `recolector_dia_asignado` (`id`, `id_ruta`, `id_recolector`, `dia`) VALUES
(1, 18, 15, '2020-09-23'),
(2, 17, 19, '2021-06-12'),
(3, 19, 19, '2020-09-25'),
(4, 32, 19, '2020-09-25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `titulo`) VALUES
(1, 'Administrador'),
(2, 'Recolector'),
(3, 'Cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ruta_cliente_lista`
--

CREATE TABLE `ruta_cliente_lista` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_ruta` int(11) NOT NULL,
  `zona` varchar(100) NOT NULL,
  `pago_recolector` varchar(50) NOT NULL,
  `tipo_servicio` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'Programado para recolección',
  `comentarios` text,
  `ejecutada` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: si el admin aprobo la ejecucion de esta parada, 0: falta por abrobacion de admin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ruta_cliente_lista`
--

INSERT INTO `ruta_cliente_lista` (`id`, `id_cliente`, `id_ruta`, `zona`, `pago_recolector`, `tipo_servicio`, `status`, `comentarios`, `ejecutada`) VALUES
(656, 205, 185, 'Zona #2', '3000', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(668, 207, 188, 'Zona #2', '3000', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(750, 239, 224, 'Zona #1', '2800', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(751, 239, 225, 'Zona #1', '2800', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(752, 239, 226, 'Zona #1', '2800', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(753, 239, 227, 'Zona #1', '2800', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(754, 239, 229, 'Zona #1', '2800', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(755, 239, 230, 'Zona #1', '2800', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(756, 239, 231, 'Zona #1', '2800', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(757, 239, 232, 'Zona #1', '2800', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(758, 239, 233, 'Zona #1', '2800', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(759, 239, 234, 'Zona #1', '2800', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(760, 239, 235, 'Zona #1', '2800', 'Recogida Normal', 'Programado para recolección', NULL, 0),
(761, 239, 236, 'Zona #1', '2800', 'Recogida Normal', 'Programado para recolección', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ruta_lista`
--

CREATE TABLE `ruta_lista` (
  `id` int(11) NOT NULL,
  `id_recolector` int(11) DEFAULT NULL,
  `fecha` varchar(50) NOT NULL,
  `pago_recolector` varchar(50) NOT NULL,
  `total_clientes` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `pagado` tinyint(4) NOT NULL DEFAULT '0',
  `inventario` text,
  `aprobada` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: Ruta con paradas aun pendientes 1:Ruta con todas las paradas aprobadas'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ruta_lista`
--

INSERT INTO `ruta_lista` (`id`, `id_recolector`, `fecha`, `pago_recolector`, `total_clientes`, `titulo`, `pagado`, `inventario`, `aprobada`) VALUES
(185, 0, '2022-02-08', '3000', 1, 'Ruta #1', 0, '3', 0),
(188, 0, '2022-02-11', '3000', 1, 'Ruta #1', 0, '2', 0),
(193, 0, '2022-03-05', '0', 0, 'Ruta #1', 0, '0', 0),
(194, 199, '2022-03-05', '0', 0, 'Ruta #2', 0, '0', 0),
(195, 0, '2022-04-16', '0', 0, 'Ruta #1', 0, '0', 0),
(201, 0, '2022-02-26', '0', 0, 'Ruta #1', 0, '0', 0),
(202, 199, '2022-03-19', '0', 0, 'Ruta #1', 0, '0', 0),
(203, 0, '2022-02-28', '0', 0, 'Ruta #1', 0, '0', 0),
(204, 0, '2022-03-12', '0', 0, 'Ruta #1', 0, '0', 0),
(224, 199, '2022-04-27', '2800', 1, 'Ruta #1', 0, '0', 0),
(225, 199, '2022-05-04', '2800', 1, 'Ruta #1', 0, '0', 0),
(226, 199, '2022-05-11', '2800', 1, 'Ruta #1', 0, '0', 0),
(227, 199, '2022-05-18', '2800', 1, 'Ruta #1', 0, '0', 0),
(228, 199, '2022-05-18', '0', 0, 'Ruta #2', 0, '0', 0),
(229, 199, '2022-05-25', '2800', 1, 'Ruta #1', 0, '0', 0),
(230, 199, '2022-06-01', '2800', 1, 'Ruta #1', 0, '0', 0),
(231, 199, '2022-06-08', '2800', 1, 'Ruta #1', 0, '0', 0),
(232, 199, '2022-06-15', '2800', 1, 'Ruta #1', 0, '0', 0),
(233, 199, '2022-06-22', '2800', 1, 'Ruta #1', 0, '0', 0),
(234, 199, '2022-06-29', '2800', 1, 'Ruta #1', 0, '0', 0),
(235, 199, '2022-07-06', '2800', 1, 'Ruta #1', 0, '0', 0),
(236, 199, '2022-07-13', '2800', 1, 'Ruta #1', 0, '0', 0),
(237, 199, '2022-01-03', '0', 0, 'Ruta #1', 0, '0', 0),
(238, 199, '2022-06-18', '0', 0, 'Ruta #1', 0, '0', 0),
(239, 199, '2022-06-18', '0', 0, 'Ruta #2', 0, '0', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `suscripciones`
--

CREATE TABLE `suscripciones` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `id_pago` int(11) NOT NULL DEFAULT '0',
  `id_plan` varchar(100) NOT NULL,
  `recurrente` tinyint(1) NOT NULL,
  `fecha_inicio` varchar(100) NOT NULL,
  `fecha_vencimiento` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `suscripciones`
--

INSERT INTO `suscripciones` (`id`, `user_id`, `id_pago`, `id_plan`, `recurrente`, `fecha_inicio`, `fecha_vencimiento`, `status`) VALUES
(44, 93, 1626963857, '1', 0, '2021-07-27', '2021-10-27', 0),
(58, 141, 0, '1', 0, '2021-08-10', '2021-11-02', 0),
(59, 142, 0, '1', 0, '2021-08-10', '2021-11-02', 0),
(109, 205, 0, '1', 0, '2022-02-08', '2022-04-26', 0),
(110, 206, 0, '1', 0, '2022-02-10', '2022-04-28', 0),
(111, 207, 0, '2', 0, '2022-02-11', '2022-03-04', 0),
(114, 213, 0, '2', 0, '2022-02-18', '2022-03-11', 0),
(136, 239, 1650384771, '1', 0, '2022-04-27', '2022-07-13', 0),
(137, 241, 0, '2', 0, '2022-08-17', '2022-09-07', 1),
(138, 242, 0, '1', 0, '2022-08-17', '2022-11-02', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `cedula` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_rol` int(11) NOT NULL DEFAULT '3',
  `image` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT 'no-photo.png',
  `licencia` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT 'no-photo.png',
  `soat` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT 'no-photo.png',
  `status` int(11) NOT NULL DEFAULT '1',
  `telefono` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitud` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '4.74112408575684',
  `longitud` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '-74.04163608173303',
  `placa` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehiculo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perfil_complete` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `casa_apartamento` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `torre_apto` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `pregunta_1` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `pregunta_2` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `dias_recoleccion` text COLLATE utf8mb4_unicode_ci,
  `id_zona` int(11) DEFAULT NULL,
  `fecha_vencimiento_plan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capacidad` int(11) DEFAULT NULL,
  `semana_inicial` int(11) NOT NULL DEFAULT '1',
  `novedad` int(11) NOT NULL DEFAULT '0',
  `dia_festivo` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Clientes que no pudieron atenderse por dia festivo',
  `acepto_tyc` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Si el usuario acepto los TyC de servicios',
  `ultima_modificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `apellido`, `cedula`, `email`, `email_verified_at`, `password`, `remember_token`, `id_rol`, `image`, `licencia`, `soat`, `status`, `telefono`, `whatsapp`, `latitud`, `longitud`, `placa`, `vehiculo`, `color`, `perfil_complete`, `created_at`, `updated_at`, `direccion`, `casa_apartamento`, `torre_apto`, `pregunta_1`, `pregunta_2`, `dias_recoleccion`, `id_zona`, `fecha_vencimiento_plan`, `capacidad`, `semana_inicial`, `novedad`, `dia_festivo`, `acepto_tyc`, `ultima_modificacion`) VALUES
(1, 'Jonathan Martinez', '', NULL, 'admin@compostpack.com', NULL, 'eyJpdiI6IlwvYlJjMXpxVmRlTjhlZzFSdDRlZ2NBPT0iLCJ2YWx1ZSI6IjVhSXFONGlcLytPamRicVZPcXV2YytRPT0iLCJtYWMiOiI4MzAxMjQ4NTM1MTJkMDU3ZjNkY2ViNjkzZDI5MjJhMGZkNGQwM2IyYjI5YjMyMzEzM2RlYzYwYjJlMmEzNjMyIn0', 'n1O3wHphD25uzhIPeirEvlRaxz3bBq9IVnYtGRPP1dmdnEheArL2azhcSeLR', 1, 'no-photo.png', 'no-photo.png', 'no-photo.png', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-09-08 00:20:38', '2020-09-08 00:20:38', NULL, '', '', '', '', '', NULL, '2021-12-12', NULL, 0, 0, 0, 1, '2022-08-08 18:35:10'),
(199, 'Freddy', NULL, NULL, 'ferdi@prueba.com', NULL, 'eyJpdiI6Ik9MelwvUlh3WUZ2dkQrTEE1UEhjaGd3PT0iLCJ2YWx1ZSI6InZuc1VrWVVLQ01zejczdGxQdkk5c3c9PSIsIm1hYyI6IjNhNGViM2QwNmQ3NDM4Y2M0YTFhOTE0ZTllMzc0YjE3Mzc1NDBiM2M0M2M0MzI0N2M3MTQ2NGNiMGQ3NjY2M2MifQ==', NULL, 2, 'no-photo.png', 'no-photo.png', 'no-photo.png', 1, '3000248556', '3000248556', '4.74112408575684', '-74.04163608173303', 'ccc222', 'moto', 'azul', 0, '2021-12-21 18:30:09', '2022-03-29 18:26:30', 'calle 70b # 111 85', NULL, NULL, NULL, NULL, '{\"Mon\":0,\"Tue\":0,\"Wed\":1,\"Thu\":0,\"Fri\":0,\"Sat\":0,\"Any\":0}', NULL, NULL, 10, 1, 0, 0, 1, '2022-03-29 18:26:30'),
(239, 'Pepe', 'Perex', '1020799302', 'peieman@compostpack.com', NULL, '$2y$10$W0HJ2YR/ZTKjzH.Fzpo24.wjvR7DAEm5NyJ/7mpph6Hv5tGbFL9vm', NULL, 3, 'no-photo.png', 'no-photo.png', 'no-photo.png', 1, NULL, '3172847425', '4.7021704', '-74.0415203', NULL, NULL, NULL, 1, '2022-04-19 16:09:06', '2022-07-14 00:01:03', 'unicentro', 'Apartamento', '303', '2', '10', '{\"Mon\":0,\"Tue\":0,\"Wed\":1,\"Thu\":0,\"Fri\":0,\"Any\":0}', NULL, '2022-07-13', NULL, 0, 0, 0, 1, '2022-07-14 00:01:03'),
(240, 'Laura Tatiana', 'Sarmiento', NULL, '1tatianasarmiento5@gmail.com', NULL, '$2y$10$X4OL27OJcbIBMxxEdyU9veRz8r0wMFMYY2Q1YQh9Tlr7sjPaAvjqu', NULL, 3, 'no-photo.png', 'no-photo.png', 'no-photo.png', 1, NULL, '3057872910', '4.7021704', '-74.0415203', NULL, NULL, NULL, 0, '2022-04-20 13:15:23', '2022-04-20 13:15:23', 'Unicentro bogota', NULL, NULL, NULL, NULL, '{\"Mon\":0,\"Tue\":0,\"Wed\":0,\"Thu\":0,\"Fri\":0,\"Any\":0}', NULL, NULL, NULL, 1, 0, 0, 1, '2022-04-20 13:15:23'),
(241, 'Gianfranco', 'Rojas', NULL, 'gianfranco@compostpack.com', NULL, '$2y$10$71q/VhASK051hjhot0g3Mem9LJB.HW5YFO.gk8uuBUvCGVkhfoJEa', NULL, 3, 'no-photo.png', 'no-photo.png', 'no-photo.png', 1, '5841415786', '5841415786', '4.7021704', '-74.0415203', NULL, NULL, NULL, 1, '2022-08-08 15:46:00', '2022-08-08 15:46:01', 'Unicentro bogota', 'Casa', 'casa', '3', '1', '{\"Mon\":0,\"Tue\":0,\"Wed\":1,\"Thu\":0,\"Fri\":0,\"Any\":0}', NULL, '2022-09-07', NULL, 0, 0, 0, 1, '2022-08-08 15:46:01'),
(242, 'Carlos', 'Perez', NULL, 'gian@compostpack.com', NULL, '$2y$10$Nr4CCobGyBg0dRFes8/7e.wPeWlzg33yI5adr9h76UeC3HlsMoIIe', NULL, 3, 'no-photo.png', 'no-photo.png', 'no-photo.png', 1, '4141578602', '4141578602', '4.7021704', '-74.0415203', NULL, NULL, NULL, 1, '2022-08-08 18:27:35', '2022-08-08 18:27:36', 'Unicentro bogota', 'Casa', 'casa', '1', '1', '{\"Mon\":0,\"Tue\":0,\"Wed\":1,\"Thu\":0,\"Fri\":0,\"Any\":0}', NULL, '2022-11-02', NULL, 0, 0, 0, 1, '2022-08-08 18:27:36'),
(243, 'Gianfranco', 'Rojas', NULL, 'Gianfrancor442@gmail.com', NULL, '$2y$10$xHw8RNgsxzVeYIMwLXz7K.XKVy681dlJPOkl14WQlBjfcAeVL7JSy', NULL, 3, 'no-photo.png', 'no-photo.png', 'no-photo.png', 1, NULL, '3175213793', '4.7030981', '-74.0431116', NULL, NULL, NULL, 1, '2022-08-08 22:58:26', '2022-08-08 22:59:17', 'Av 15 # 124 65', 'Casa', 'casa', '3', '3', '{\"Mon\":0,\"Tue\":0,\"Wed\":1,\"Thu\":0,\"Fri\":0,\"Any\":0}', NULL, NULL, NULL, 1, 0, 0, 1, '2022-08-08 22:59:17'),
(244, 'Gianfranc', 'Rperez', NULL, 'gian@rojas.com', NULL, '$2y$10$FyahyzSaBkRdLi8Cln2yLe31FfQkUTystymk57A.U4IgtMetn0SNO', NULL, 3, 'no-photo.png', 'no-photo.png', 'no-photo.png', 1, NULL, '4141578602', '4.7021704', '-74.0415203', NULL, NULL, NULL, 1, '2022-08-09 02:07:39', '2022-08-09 02:08:03', 'Unicentro bogota', 'Casa', 'casa', '3', '3', '{\"Mon\":0,\"Tue\":0,\"Wed\":1,\"Thu\":0,\"Fri\":0,\"Any\":0}', NULL, NULL, NULL, 1, 0, 0, 1, '2022-08-09 02:08:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zona`
--

CREATE TABLE `zona` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `zona`
--

INSERT INTO `zona` (`id`, `titulo`, `status`) VALUES
(1, 'Suba', 1),
(2, 'Chapinero', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zona_mapa`
--

CREATE TABLE `zona_mapa` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `color` varchar(50) NOT NULL,
  `radio` int(11) NOT NULL,
  `precio` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `zona_mapa`
--

INSERT INTO `zona_mapa` (`id`, `titulo`, `color`, `radio`, `precio`) VALUES
(1, '-', '#DFFF00', 5, '2800'),
(2, '-', '#FFBF00', 10, '3000');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `control_estado_novedad`
--
ALTER TABLE `control_estado_novedad`
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indices de la tabla `control_recoleccion_diario`
--
ALTER TABLE `control_recoleccion_diario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalles_suscripciones_epayco`
--
ALTER TABLE `detalles_suscripciones_epayco`
  ADD PRIMARY KEY (`id_suscripcion`);

--
-- Indices de la tabla `dias_festivos`
--
ALTER TABLE `dias_festivos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estados_recoleccion`
--
ALTER TABLE `estados_recoleccion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historial_pagos`
--
ALTER TABLE `historial_pagos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historial_pago_recolector`
--
ALTER TABLE `historial_pago_recolector`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historial_recoleccion`
--
ALTER TABLE `historial_recoleccion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventarios`
--
ALTER TABLE `inventarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `recolector_dia_asignado`
--
ALTER TABLE `recolector_dia_asignado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ruta_cliente_lista`
--
ALTER TABLE `ruta_cliente_lista`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ruta_lista`
--
ALTER TABLE `ruta_lista`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `suscripciones`
--
ALTER TABLE `suscripciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `zona_mapa`
--
ALTER TABLE `zona_mapa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `control_recoleccion_diario`
--
ALTER TABLE `control_recoleccion_diario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `dias_festivos`
--
ALTER TABLE `dias_festivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `estados_recoleccion`
--
ALTER TABLE `estados_recoleccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `historial_pagos`
--
ALTER TABLE `historial_pagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `historial_pago_recolector`
--
ALTER TABLE `historial_pago_recolector`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `historial_recoleccion`
--
ALTER TABLE `historial_recoleccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `inventarios`
--
ALTER TABLE `inventarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `plan`
--
ALTER TABLE `plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ruta_cliente_lista`
--
ALTER TABLE `ruta_cliente_lista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=762;

--
-- AUTO_INCREMENT de la tabla `ruta_lista`
--
ALTER TABLE `ruta_lista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT de la tabla `suscripciones`
--
ALTER TABLE `suscripciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;

--
-- AUTO_INCREMENT de la tabla `zona_mapa`
--
ALTER TABLE `zona_mapa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

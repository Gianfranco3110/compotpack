<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosRecoleccion extends Model
{
    protected $table = 'estados_recoleccion';
    public $timestamps = false;
}

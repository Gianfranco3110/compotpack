<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecolectorDiaAsignado extends Model
{
    protected $table = 'recolector_dia_asignado';
    public $timestamps = false;

    public function relatedRecolector()
    {
    	return $this->belongsTo('App\User', 'id_recolector', 'id');
    }

    public function relatedRoutes()
    {
    	return $this->belongsTo('App\Ruta', 'id_ruta', 'id');
    }
}

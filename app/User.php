<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'dias_recoleccion'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsTo('App\Role', 'id_rol');
    }

    public static function getClientsForRoute($arrayClientes) {
        
        $clients = array();
        if(!empty($arrayClientes)) {
        
        $clients = \DB::select("SELECT *, (( 3959 * acos( cos( radians('4.7410673') ) * cos( radians( latitud ) ) * cos( radians( longitud ) - radians('-74.0438427') ) + sin( radians('4.7410673') ) * sin( radians( latitud ) ) ) ) ) * 1.609 AS distance FROM users  WHERE id IN (". implode(',', array_map('intval', $arrayClientes)) .") HAVING distance < 100000 ORDER BY distance");

        }

        return $clients;
    }

    public static function getRecolectionStatus() {
        exit("HOLA");
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class RutaLista extends Model
{
    protected $table = 'ruta_lista';
    public $timestamps = false;

    public static function getRouteAsignRecolector($recolectorId) /************Metodo para chequeo de recolector asignado******/
    {
    	$getRouteRecolector = User::find($recolectorId);
    	return $getRouteRecolector;
    }

    public static function relatedClientsToRoute($id) /******Relacion clientes******/
    {
        return RutaClienteLista::where('id_ruta', $id)->get();
    }

    public static function relatedClientsInfo($clientId) /******Info Cliente******/
    {
        return User::find($clientId);
    }

    public function relatedRecolector()
    {
    	return $this->belongsTo('App\User', 'id_recolector', 'id');
    }

    public function relatedRoutes()
    {
    	return $this->belongsTo('App\Ruta', 'id_ruta', 'id');
    }

    public function relatedRoutesAsigned()
    {
    	return $this->hasMany('App\RutaClienteLista', 'id_ruta');
    }
}

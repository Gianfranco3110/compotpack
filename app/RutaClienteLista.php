<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RutaClienteLista extends Model
{
    protected $table = 'ruta_cliente_lista';
    public $timestamps = false;

    public function relatedRoutes()
    {
    	return $this->belongsTo('App\RutaLista', 'id_ruta');
    }
}

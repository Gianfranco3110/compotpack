<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{
    protected $table = 'zona';
    public $timestamps = false;

    public function relatedRoutes()
    {
    	return $this->hasMany('App\Ruta', 'id_zona', 'id');
    }

    public function relatedRoutesByDay($day)
    {
    	#exit("llega ".$day);
    	return $this->hasMany('App\Ruta', 'id_zona', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialRecoleccion extends Model
{
    protected $table = 'historial_recoleccion';
    public $timestamps = false;
}

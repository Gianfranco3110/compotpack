<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventarios extends Model
{
    protected $table = 'inventarios';
    public $timestamps = false;
}

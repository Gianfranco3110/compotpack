<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\RutaLista;
use App\RutaClienteLista;
use App\User;

class getClientsNovedad extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'list:clientsNovedad';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron encargado de marcar como novedad a los clientes no atentidos.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = date('Y-m-d');
        $daySearch = date('Y-m-d', strtotime('-1 day'));
        $rutas = RutaLista::where('fecha', $daySearch)->get();

        foreach($rutas as $ruta){
            $rutaClientes = RutaClienteLista::where('id_ruta', $ruta->id)->where('status', '<>', 'Recogida')->get();
            foreach ($rutaClientes as $key => $cliente) {
                $clienteUsuario = User::find($cliente->id_cliente);
                $clienteUsuario->novedad = 1;
                $clienteUsuario->save();
            }
        }
    }
}

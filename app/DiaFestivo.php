<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiaFestivo extends Model
{
    protected $table = 'dias_festivos';
    public $timestamps = false;
}

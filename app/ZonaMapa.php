<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZonaMapa extends Model
{
    protected $table = 'zona_mapa';
    public $timestamps = false;
}

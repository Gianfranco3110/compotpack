<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RutaCliente extends Model
{
    protected $table = 'ruta_cliente';
    public $timestamps = false;

    public function relatedClients()
    {
    	return $this->belongsTo('App\User', 'id_cliente', 'id');
    }

    public function relatedClientsCheked()
    {
    	return $this->relatedClients()->where('status', 1)->get();
    }

    public function relatedRoute()
    {
    	return $this->belongsTo('App\RutaCliente', 'id', 'id_ruta');
    }
}

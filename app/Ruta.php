<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RecolectorDiaAsignado;

class Ruta extends Model
{
    protected $table = 'ruta';
    public $timestamps = false;

    public function relatedClients()
    {
    	return $this->hasMany('App\RutaCliente', 'id_ruta', 'id');
    }

    public function relatedClientsActive()
    {
    	#exit("AQUI");
    	return $this->relatedClients()->get();
    }

    public function relatedClientsManyToMany() /******Relacion mucho a mucho con tabla clientes******/
    {
    	return $this->belongsToMany('App\User', 'ruta_cliente', 'id_ruta', 'id_cliente');
    }

    public function relatedClientsManyToManyChecked($daySelected) /************Metodo para chequeo de clientes activos y con suscripcion al dia******/
    {
    	return $this->relatedClientsManyToMany()->where('status',1)->where('fecha_vencimiento_plan','>=', $daySelected)->get();
    }

    public function getRouteAsignRecolector($routeId, $date) /************Metodo para chequeo de recolector asignado******/
    {
    	$getRouteRecolector = RecolectorDiaAsignado::where('dia', $date)->where('id_ruta', $routeId)->first();
    	return $getRouteRecolector;
    }

    public static function createRuta($zona, $semana, $day, $identifier)
    {
    	$ruta = new Ruta;
        $ruta->id_zona = $zona->id;
        $ruta->titulo = $zona->titulo.' '.$semana[$day].' #'.$identifier;
        $ruta->dia = $day;
        $ruta->status = 1;
        $ruta->save();

        return $ruta->id;
    }
}

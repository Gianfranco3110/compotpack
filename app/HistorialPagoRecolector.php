<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialPagoRecolector extends Model
{
    protected $table = 'historial_pago_recolector';
    public $timestamps = false;

    public function relatedRecolector()
    {
        return $this->belongsTo('App\User', 'id_recolector');
    }
}

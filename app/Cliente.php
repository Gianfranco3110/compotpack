<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RutaCliente;

class Cliente extends Model
{
	protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password', 'image',
    ];

    public static function generateJsonDaysSelected($semana, $daysSelected)
	{
		$diasRecoleccion = array();
        
        foreach ($semana as $key => $value) {            
            #$diasRecoleccion[$key] = in_array($key, $daysSelected) ? 1:0;
            $diasRecoleccion[$key] = $key == $daysSelected ? 1:0;
        }

        return json_encode($diasRecoleccion);
	}

	public static function cleanClientRoutesRegister($userId)
	{
		$rutasCliente = RutaCliente::where('id_cliente', $userId)->get();
        foreach ($rutasCliente as $rutaCliente) {
            $rutaEspecifica = Ruta::find($rutaCliente->id_ruta);
            if($rutaEspecifica->relatedClients->count() == 1) { /******VERIFICA QUE ES EL ULTIMO CLIENTE, si es asi elimina la ruta******/
                $rutaEspecifica->delete();
            }
            $rutaCliente->delete();
        }

        return true;
	}

	public static function setRoute($rutaId, $userId)
	{
		$rutaCliente = new RutaCliente;
        $rutaCliente->id_ruta = $rutaId;
        $rutaCliente->id_cliente = $userId;
        $rutaCliente->save();
	}
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;
use App\HistorialPagos;
use App\User;

class ListaPlanController extends Controller
{
    public function index()
    {
        $planesOnce = Plan::where('status', 1)->where('recurrente', 0)->orderBy('precio', 'asc')->get();
        $planesRecurrente = Plan::where('status', 1)->where('recurrente', 1)->orderBy('precio', 'asc')->get();
        return view('listaplan/index' , 
        		[
        		'planesOnce' => $planesOnce,
        		'planesRecurrente' => $planesRecurrente,
        		]
        	);
    }

    public function response()
    {
        return view('listaplan/response');
    }

    public function confirm(Request $request)
    {
    	\Log::debug('Confirmacion recibida '.$request->x_id_factura);
    	$idFactura = $request->x_id_factura;

    	$datos = explode('_', $idFactura);
    	$idCliente = $datos[0];
    	$idPlan = $datos[1];
    	$idIdentifier = $datos[2];

    	\Log::debug('Cliente '.$idCliente." Plan ".$idPlan." Ident ".$idIdentifier);

    	$p_cust_id_cliente = '88213';
		$p_key             = '1e3f00490cde6c7800ef0d748602d8474c983824';

		$x_ref_payco      = $request->x_ref_payco;
		$x_transaction_id = $request->x_transaction_id;
		$x_amount         = $request->x_amount;
		$x_currency_code  = $request->x_currency_code;
		$x_signature      = $request->x_signature;


		$signature = hash('sha256', $p_cust_id_cliente . '^' . $p_key . '^' . $x_ref_payco . '^' . $x_transaction_id . '^' . $x_amount . '^' . $x_currency_code);

		$x_response     = $request->x_response;
		$x_motivo       = $request->x_response_reason_text;
		$x_id_invoice   = $request->x_id_invoice;
		$x_autorizacion = $request->x_approval_code;

		if ($x_signature == $signature) {

		  $historyPayments = HistorialPagos::where('id_cliente', $idCliente)->where('id_plan', $idPlan)->where('identifier', $idIdentifier)->first();

		  if(!$historyPayments) { /*******Si no existe mconfirmacion previa se procede a crear el registro *****/
		  	\Log::debug('----> No existe');
		  	$historyPayments = new HistorialPagos;
		  	$historyPayments->id_cliente = $idCliente;
		  	$historyPayments->id_plan = $idPlan;		  	
		  	$historyPayments->identifier = $idIdentifier;
		  }
		  else{
		  	\Log::debug('----> SI existe');
		  }

		  $plan = Plan::where('id', $idPlan)->first();

		  $historyPayments->fecha = date('Y-m-d H:i:s');
		  $historyPayments->x_amount = $x_amount;
		  $historyPayments->x_ref_payco = $x_ref_payco;
		  $historyPayments->x_transaction_id = $x_transaction_id;
		  $historyPayments->x_currency_code = $x_currency_code;
		  $historyPayments->x_response = $x_response;
		  $historyPayments->x_response_reason_text = $x_motivo;
		  $historyPayments->x_id_invoice = $x_id_invoice;
		  $historyPayments->x_approval_code = $x_autorizacion;
		  $historyPayments->titulo_plan = $plan->titulo;
		  $historyPayments->frecuencia = $plan->frecuencia_cobro;	

		  $x_cod_response = $request->x_cod_response;

			switch ((int) $x_cod_response) {

				case 1:
				  # code transacción aceptada
				 //echo 'transacción aceptada';
				$estado = 'Aprobada';
				
				/***************PROCeso de asignacion de nueva fecha de vencimiento del cliente******/
				$cliente = User::find($idCliente);

				if(is_null($cliente->fecha_vencimiento_plan)) {
					$fechaVencimiento = date('Y-m-d');
					\Log::debug('Entra a null '. $fechaVencimiento);
				}elseif($cliente->fecha_vencimiento_plan < date('Y-m-d')) {
					$fechaVencimiento = date('Y-m-d');
					\Log::debug('Entra a nmenor '. $fechaVencimiento);
				}
				else{
					$fechaVencimiento = $cliente->fecha_vencimiento_plan;	
					\Log::debug('Entra a mayor '. $fechaVencimiento);
				}

				$historyPayments->fecha_inicio = $fechaVencimiento;
				$nuevafecha = strtotime ( '+'.$plan->frecuencia_cobro.' month' , strtotime ( $fechaVencimiento ) ) ;
		  		$historyPayments->fecha_finalizacion = date('Y-m-d', $nuevafecha);

		  		$ultima = $cliente->fecha_vencimiento_plan = date('Y-m-d', $nuevafecha);
		  		$cliente->save();
		  		\Log::debug('vencimiento '. $ultima);

				break;

				case 2:
				  # code transacción rechazada
				//echo 'transacción rechazada';
				$estado = 'Rechazada';
				  break;

				case 3:
				 # code transacción pendiente
				//echo 'transacción pendiente';
				$estado = 'Pendiente';
				break;

				case 4:
				# code transacción fallida
				//echo 'transacción fallida';
				$estado = 'Fallida';
				break;

			}

			$historyPayments->estado = $estado;
			$historyPayments->save();

		} else {
			\Log::debug('Firma no valida ');
		}

    	\Log::debug('Termina confirmacion '.$request->x_id_factura);
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use App\Recolector;

class RecolectorController extends Controller
{
    protected $customMessages = [
            'required' => 'El campo :attribute es obligatorio.',
            'email' => 'El campo :attribute debe ser un Email valido.',
            'regex' => 'El campo :attribute debe contener solo letras.',
            'unique' => 'El Email indicado ya se encuentra registrado en la base de datos.',
            'confirmed' => 'El password debe coincidir con la confirmación del password.',
            'numeric' => 'El campo :attribute debe ser numerico.',
            'image' => 'La foto debe ser de un formato valido (jpeg, png, bmp, gif, svg, or webp).'
        ];

    protected $niceNames = [
            'name' => 'Nombre Completo',
            'email' => 'Email',
            'color' => 'Color del Vehiculo',
            'placa' => 'Placa del Vehiculo',
            'vehiculo' => 'Vehiculo del Vehiculo',
        ];

    public function index()
    {
        $recolectores = recolector::where('status', '<>', 2)->where('id_rol', 2)->get();
        return view('recolector/index' , ['recolectores' => $recolectores]);
    }

    public function create()
    {
        $semana = Config::get('weekDays.week');
        return view('recolector/create', ['semana' => $semana]);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[a-zA-ZÑñ\s]+$/',
            'placa' => 'required',
            'color' => 'required|regex:/^[a-zA-ZÑñ\s]+$/',
            'vehiculo' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed',
            'telefono' => 'required|numeric',
            'whatsapp' => 'required|numeric',
            'capacidad' => 'required|numeric',
            'image' => 'mimes:jpeg,bmp,png,jpg',
            'image-licencia' => 'mimes:jpeg,bmp,png,jpg',
            'image-soat' => 'mimes:jpeg,bmp,png,jpg'
        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);

        $recolector = new Recolector;
        $recolector->name = $request->input('name');
        $recolector->email = $request->input('email');
        $recolector->password = Hash::make($request->input('password'));
        $recolector->telefono = $request->input('telefono');
        $recolector->whatsapp = $request->input('whatsapp');
        $recolector->direccion = $request->input('direccion');
        $recolector->color = $request->input('color');
        $recolector->placa = $request->input('placa');
        $recolector->vehiculo = $request->input('vehiculo');
        $recolector->capacidad = $request->input('capacidad');
        $recolector->id_rol = 2;

        if($request->hasfile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/usuarios', $filename);
            $recolector->image = $filename;
        }
        if($request->hasfile('image-licencia')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'-licencia.'.$extension;
            $file->move('uploads/usuarios', $filename);
            $recolector->licencia = $filename;
        }
        if($request->hasfile('image-soat')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'-soat.'.$extension;
            $file->move('uploads/usuarios', $filename);
            $recolector->soat = $filename;
        }

        else {
            $recolector->image = 'no-photo.png';
        }

        $semana = Config::get('weekDays.week');
        $daysSelected = $request->input('dia_recoleccion');
        $daysJson = array();
        foreach($semana as $key => $dia) {
          if(in_array($key, $daysSelected)){
            $daysJson[$key] = 1;
          }else{
            $daysJson[$key] = 0;
          }
        }
        $recolector->dias_recoleccion = json_encode($daysJson);

        $recolector->save();

        return redirect('recolector')->with('success','Registro guardado con éxito.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $recolector = Recolector::findOrFail($id);
        $semana = Config::get('weekDays.week');
        $diasRecoleccion = json_decode($recolector->dias_recoleccion, true);

        return view('recolector/edit',[
                                        'recolector' => $recolector,
                                        'semana' => $semana,
                                        'diasRecoleccion' => $diasRecoleccion
                                       ]);
    }

    public function update(Request $request, $id)
    {
        $recolector = Recolector::findOrFail($id);

        $rules = [
            'name' => 'required|regex:/^[a-zA-ZÑñ\s]+$/',
            'email' => 'required|email',
            'placa' => 'required',
            'color' => 'required|regex:/^[a-zA-ZÑñ\s]+$/',
            'vehiculo' => 'required',
            'telefono' => 'required|numeric',
            'capacidad' => 'required|numeric',
            'whatsapp' => 'required|numeric',
            'image' => 'mimes:jpeg,bmp,png,jpg',
            'image-licencia' => 'mimes:jpeg,bmp,png,jpg',
            'image-soat' => 'mimes:jpeg,bmp,png,jpg'

        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);

        $recolector->name = $request->input('name');
        $recolector->telefono = $request->input('telefono');
        $recolector->whatsapp = $request->input('whatsapp');
        $recolector->direccion = $request->input('direccion');
        $recolector->color = $request->input('color');
        $recolector->placa = $request->input('placa');
        $recolector->vehiculo = $request->input('vehiculo');
        $recolector->capacidad = $request->input('capacidad');

        if($request->hasfile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/usuarios', $filename);
            $recolector->image = $filename;
        }

        if($request->hasfile('image-licencia')) {
            $file = $request->file('image-licencia');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'-licencia.'.$extension;
            $file->move('uploads/usuarios', $filename);
            $recolector->licencia = $filename;
        }
        if($request->hasfile('image-soat')) {
            $file = $request->file('image-soat');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'-soat.'.$extension;
            $file->move('uploads/usuarios', $filename);
            $recolector->soat = $filename;
        }

        $semana = Config::get('weekDays.week');
        $daysSelected = $request->input('dia_recoleccion');
        $daysJson = array();
        foreach($semana as $key => $dia) {
          if(in_array($key, $daysSelected)){
            $daysJson[$key] = 1;
          }else{
            $daysJson[$key] = 0;
          }
        }
        $recolector->dias_recoleccion = json_encode($daysJson);
        $recolector->save();
        return redirect('recolector')->with('success','Registro editado con éxito.');
    }

    public function destroy($id)
    {
        $recolector = Recolector::findOrFail($id);
        $recolector->status = 2;
        $recolector->save();
        return redirect('recolector')->with('success','Registro eliminado con éxito.');
    }

    public function setStatus($id)
    {
        $recolector = Recolector::findOrFail($id);
        $recolector->status = $recolector->status==1?0:1;
        $recolector->save();
        return redirect('recolector')->with('success','Estado modificado con éxito.');
    }

    public function messages()
    {
        return [
            'required' => 'ES UN title is required'
        ];
    }
}

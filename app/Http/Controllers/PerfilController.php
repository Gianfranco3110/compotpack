<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Perfil;
use App\Cliente;
use App\Recolector;
use App\Zona;
use App\Ruta;
use App\RutaCliente;
use App\HistorialPagos;
use App\User;

class PerfilController extends Controller
{
    protected $customMessages = [
            'required' => 'El campo :attribute es obligatorio.',
            'email' => 'El campo :attribute debe ser un Email valido.',
            'regex' => 'El campo :attribute debe contener solo letras.',
            'unique' => 'El Email indicado ya se encuentra registrado en la base de datos.',
            'confirmed' => 'El password debe coincidir con la confirmación del password.',
            'numeric' => 'El campo :attribute debe ser numerico.',
            'image' => 'La foto debe ser de un formato valido (jpeg, png, bmp, gif, svg, or webp).',
            'days.required' => 'Debes escoger al menos un día para la recolección.'
        ];

    protected $niceNames = [
            'name' => 'Nombre Completo',
            'email' => 'Email',
            'id_zona' => 'Zona'
        ];

    public function cliente()
    {
        $semana = Config::get('weekDays.week');
        $cliente = Cliente::findOrFail(Auth::user()->id);
        $diasRecoleccion = json_decode($cliente->dias_recoleccion,true);
        $diasPermitidos = array();

        foreach($diasRecoleccion as $key => $dia) {
          //Primero determinamos la capacidad de recolección maxima para el dia sumando
    			//la capacidad de recoleccion de cada recolector que este disponible para ese dia
    			$recolectores = User::where('id_rol', 2)->where('status', 1)->whereJsonContains('dias_recoleccion', [''.$key => 1])->get();
    			$capacidadMaxDia = 0;
    			foreach ($recolectores as $value) {
    					$capacidadMaxDia += $value->capacidad;
    			}
          //Determinanos el total de clientes asignados para ese dia con el fin de validar
    			//Si aun no se alcanza la capacidadMaxDia
    			$totalClientesAsignados = Cliente::where('status', 1) /*****Activo */
                      ->where('id_rol', 3) /*****Cliente */
                      ->whereJsonContains('dias_recoleccion', [''.$key => 1])
                      ->where('fecha_vencimiento_plan', '>=', date('Y-m-d', strtotime('next friday')))
                      ->get()->count();
          #echo "<br>".$key." ".$clientesRecoleccion." ".$capacidadDia;
          $diasPermitidos[$key] = $totalClientesAsignados < $capacidadMaxDia ? true : false;
        }

        return view('perfil/cliente' , [
                                        'cliente' => $cliente,
                                        'semana' => $semana,
                                        'diasRecoleccion' => $diasRecoleccion,
                                        'diasPermitidos' => $diasPermitidos
                                        ]);
    }

    public function recolector()
    {

      $recolector = Recolector::findOrFail(Auth::user()->id);
      $semana = Config::get('weekDays.week');
      $diasRecoleccion = json_decode($recolector->dias_recoleccion, true);

      return view('perfil/recolector',[
                                      'recolector' => $recolector,
                                      'semana' => $semana,
                                      'diasRecoleccion' => $diasRecoleccion
                                     ]);
    }

    public function clienteUpdate(Request $request)
    {
        $cliente = Cliente::findOrFail(Auth::user()->id);

        $rules = [
            'name' => 'required|regex:/^[a-zA-ZÑñ\s]+$/',
            'email' => 'required|email',
            'telefono' => 'required|numeric',
            'whatsapp' => 'required|numeric',
            'image' => 'mimes:jpeg,bmp,png,jpg',
            'dia_recoleccion' => 'required|in:Mon,Tue,Wed,Thu,Fri'
        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);

        $cliente->name = $request->input('name');
        $cliente->telefono = $request->input('telefono');
        $cliente->whatsapp = $request->input('whatsapp');
        $cliente->direccion = $request->input('direccion');
        $cliente->latitud = $request->input('latitud');
        $cliente->longitud = $request->input('longitud');
        $cliente->id_zona = $request->input('id_zona');
        $cliente->perfil_complete = 1;

        if($request->hasfile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/clientes', $filename);
            $cliente->image = $filename;
        }

        /****************GENERACION DE JSON PARA LISTA DE DIAS EL CLIENTE********/
        $semana = Config::get('weekDays.week');
        $daysSelected = $request->input('dia_recoleccion');
        $daysJson = Cliente::generateJsonDaysSelected($semana, $daysSelected);
        $cliente->dias_recoleccion = $daysJson;
        $cliente->save();

        /*******************LIMPIEZA DE REGISTROS EXISTENTES DEL CLIENTE Version 1 Omitido**************/
        #Cliente::cleanClientRoutesRegister(Auth::user()->id);

        /****************ASIGNACION DE RUTA********/
        /*foreach ($daysSelected as $key => $day) {

            $rutasExistentes = Ruta::where('id_zona', $request->input('id_zona'))->where('dia', $day)->where('status', 1)->get()->sortBy(function($ruta)
                                {
                                    return $ruta->relatedClients->count();
                                }, true, true);

            $zona = Zona::where('id', $request->input('id_zona'))->first();

            if($rutasExistentes->isEmpty()) {
                $rutaId = Ruta::createRuta($zona, $semana, $day, 1);
            }
            else{
                $banderaAsignacion = false;
                foreach ($rutasExistentes as $key2 => $ruta) {
                    if($ruta->relatedClients->count() < env('MAX_ROUTE')) {
                        $banderaAsignacion = true;
                        $rutaId = $ruta->id;
                        break;
                    }
                }

                if(!$banderaAsignacion) {
                    $rutaId = Ruta::createRuta($zona, $semana, $day, $rutasExistentes->count() + 1);
                }
            }

            Cliente::setRoute($rutaId, Auth::user()->id);
        }*/

        return redirect('perfil/cliente')->with('success','Perfil editado con éxito.');
    }

    public function historialPayments()
    {
        $historial = HistorialPagos::where('id_cliente', Auth::user()->id)->get();
        return view('perfil/historialPayments' , [
                                        'historial' => $historial
                                        ]);
    }

    public function historialPaymentsAdmin()
    {
        $historial = HistorialPagos::all();
        return view('perfil/historialPayments' , [
                                        'historial' => $historial
                                        ]);
    }
}

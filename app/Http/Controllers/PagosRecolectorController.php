<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\RecolectorRuta;
use App\RutaLista;
use App\RutaClienteLista;
use App\RecolectorDiaAsignado;
use App\EstadosRecoleccion;
use App\User;
use App\HistorialPagos;
use App\HistorialPagoRecolector;

class PagosRecolectorController extends Controller
{
    protected $customMessages = [
            'required' => 'El campo :attribute es obligatorio.',
            'regex' => 'El campo :attribute debe contener solo letras.',
            'numeric' => 'El campo :attribute debe ser numerico.',
        ];

    protected $niceNames = [
            'titulo' => 'Titulo'
        ];

    public function index(Request $request)
    {
        #print_r($request);
        $recolector = User::find($request->input('recolector'));
        $rutas = RutaLista::where('id_recolector', $recolector->id)->get();
        
        $cuentaRutas = 0;
        $montoTotal = 0;

        foreach ($rutas as $key => $ruta) {            
            $carreras = RutaClienteLista::where('id_ruta', $ruta->id)->where('status', 'Recogida')->where('pagado',0)->get();
            $cuentaRutas += $carreras->count();
            foreach($carreras as $carrera) {
                $montoTotal += $carrera->pago_recolector;
                #$carrera->relatedRoutes->titulo
            }
        }

        $historico = HistorialPagoRecolector::where('id_recolector', $recolector->id)->get();
        
        return view('pagosrecolector/index', [
                                    'cuentaRutas' => $cuentaRutas,
                                    'rutas' => $rutas,
                                    'recolector' => $recolector,
                                    'carreras' => $carreras,
                                    'montoTotal' => $montoTotal,
                                    'historico' => $historico
                                ]);
    }

    public function historial(Request $request)
    {
        #print_r($request);
        $recolector = User::find(Auth::user()->id);
        $rutas = RutaLista::where('id_recolector', $recolector->id)->get();
        
        $cuentaRutas = 0;
        $montoTotal = 0;

        foreach ($rutas as $key => $ruta) {            
            $carreras = RutaClienteLista::where('id_ruta', $ruta->id)->where('status', 'Recogida')->where('pagado',0)->get();
            $cuentaRutas += $carreras->count();
            foreach($carreras as $carrera) {
                $montoTotal += $carrera->pago_recolector;
                #$carrera->relatedRoutes->titulo
            }
        }

        $historico = HistorialPagoRecolector::where('id_recolector', $recolector->id)->get();
        
        return view('pagosrecolector/historial', [
                                    'cuentaRutas' => $cuentaRutas,
                                    'rutas' => $rutas,
                                    'recolector' => $recolector,
                                    'carreras' => $carreras,
                                    'montoTotal' => $montoTotal,
                                    'historico' => $historico
                                ]);
    }

    public function saveHistory(Request $request)
    {
        $rutas = RutaLista::where('id_recolector', $request->input('id_recolector'))->get();
        
        $cuentaRutas = 0;
        $montoTotal = 0;

        foreach ($rutas as $key => $ruta) {            
            $carreras = RutaClienteLista::where('id_ruta', $ruta->id)->where('status', 'Recogida')->where('pagado',0)->get();
            $cuentaRutas += $carreras->count();
            foreach($carreras as $carrera) {
                $montoTotal += $carrera->pago_recolector;
                #$carrera->relatedRoutes->titulo
                $carrera->pagado = 1;
                $carrera->save();
            }
        }

        $historico = new HistorialPagoRecolector;
        $historico->id_recolector = $request->input('id_recolector');
        $historico->fecha = date('Y-m-d');
        $historico->carreras_pagadas = $cuentaRutas;
        $historico->monto = $montoTotal;
        $historico->save();
        
        return redirect('pagosrecolector?recolector='.$request->input('id_recolector'))->with('success','Pago registrado con éxito y agregado a la tabla de historial.');
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Zona;

class ZonaController extends Controller
{
    protected $customMessages = [
            'required' => 'El campo :attribute es obligatorio.',
            'regex' => 'El campo :attribute debe contener solo letras.',
            'numeric' => 'El campo :attribute debe ser numerico.',
        ];

    protected $niceNames = [
            'titulo' => 'Titulo'
        ];

    public function index()
    {
        $zonas = Zona::where('status', '<>', 2)->get();
        return view('zona/index' , ['zonas' => $zonas]);
    }

    public function create()
    {
        return view('zona/create');
    }

    public function store(Request $request)
    {
        $rules = [
            'titulo' => 'required',
        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);
        
        $zona = new Zona;
        $zona->titulo = $request->input('titulo');
        $zona->save();

        return redirect('zona')->with('success','Registro guardado con éxito.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $zona = Zona::findOrFail($id);
        return view('zona/edit' , ['zona' => $zona]);
    }

    public function update(Request $request, $id)
    {
        $zona = Zona::findOrFail($id);

        $rules = [
            'titulo' => 'required',
        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);

        $zona->titulo = $request->input('titulo');
        $zona->save();

        return redirect('zona')->with('success','Registro editado con éxito.');
    }

    public function destroy($id)
    {
        $zona = Zona::findOrFail($id);
        $zona->status = 2;
        $zona->save();
        return redirect('zona')->with('success','Registro eliminado con éxito.');
    }

    public function setStatus($id)
    {
        $zona = Zona::findOrFail($id);
        $zona->status = $zona->status==1?0:1;
        $zona->save();
        return redirect('zona')->with('success','Estado modificado con éxito.');
    }

    public function messages()
    {
        return [
            'required' => 'ES UN title is required'
        ];
    }
}
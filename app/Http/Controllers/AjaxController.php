<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\DiaFestivo;
use App\User;
use App\Cliente;
use App\RutaLista;
use App\RutaClienteLista;
use App\ZonaMapa;
use App\EstadosRecoleccion;
//use App\ControlRecoleccionDiaria;

class AjaxController extends Controller
{
	public function saveRecolectionStatus(Request $request){

		#echo $request->routeId." ".$request->dia." ".$request->recolectorId;
		$estado = EstadosRecoleccion::find($request->status);

		$recolectionData = RutaClienteLista::find($request->recolectionId);
		$recolectionData->status = $estado->titulo;
		$recolectionData->comentarios = $request->comentarios;
		$recolectionData->save();

		$cliente = User::find($recolectionData->id_cliente);

		/*Si es recolectado se procede a marcar como cliente NO novedad*/
		if($request->status == 2) {
			$cliente->novedad = 0;
			$cliente->semana_inicial = 0;
		}
		elseif($request->status != 3) { /* Si es distinto a en camino se marca como cliente novedad */
			$cliente->novedad = 1;
		}

		$cliente->save();

    	$message = 'Estado guardado con éxito.';
      	#echo $request->routeId." ".$request->dia." ".$request->recolectorId;
      	$response = array(
          'status' => 'success',
		  'msg' => $message,
		  'status' => $estado->titulo
      	);
      	return response()->json($response);
   }

    public function updateRecolectorRoute(Request $request){

    	#echo $request->routeId." ".$request->dia." ".$request->recolectorId;

    	$getRouteRecolector = User::find($request->recolectorId);

		$ruta = RutaLista::find($request->routeId) ;
		$ruta->id_recolector = $request->recolectorId;
    	$ruta->save();

    	$message = 'Recolector asignado de exitosamente al recolector '.$getRouteRecolector->name.'.';
      	#echo $request->routeId." ".$request->dia." ".$request->recolectorId;
      	$response = array(
          'status' => 'success',
          'msg' => $message,
          'recolectorName' => $getRouteRecolector->name
      	);
      	return response()->json($response);
   }

	public function updateNovedadRoute(Request $request){
		 $ruta = RutaLista::find($request->routeId);
		$cliente = User::find($request->clienteId);
		$cliente->novedad = 0;
		$cliente->save();

		$clienteRuta = new RutaClienteLista;

		$zonasLista = array();
		$zonasLista[0] = array("radio"=>0);
		$cuentaZonas = 1;
		$totalRuta=0;

		/* Traemos el listado de zonas*/
		$zonas = ZonaMapa::all();
		foreach($zonas as $key => $zona) {
			$zonasLista[$zona->id] = array(
				"radio" => $zona->radio,
				"titulo" => "Zona #".$cuentaZonas,
				"valor" => $zona->precio
			);
			$cuentaZonas++;
		}
		$zonasLista[$cuentaZonas] = array("radio"=>10000, "titulo" => "Zona otro");

		$distanciaCliente = $this->distance(4.74112408575684, -74.04163608173303, floatval($cliente->latitud), floatval($cliente->longitud));

		/*Verificamos a que zona pertenece el cliente*/
		foreach($zonasLista as $key => $zona) {
			if($cuentaZonas>$key){
				$nextZonaId = $key+1;
				$nextZona = $zonasLista[$nextZonaId];
				#print_r($zona);
				#print_r($nextZona);
				if($distanciaCliente >= $zona['radio'] && $distanciaCliente <= $nextZona['radio']){
					#echo "<br> es Zona ".$nextZona['titulo']." DISTANCIA ".$distanciaCliente." -> ".floatval($value->latitud).",".floatval($value->longitud);
					/*Asignamos el cliente a la ruta*/
					$clienteRuta->zona = $nextZona['titulo'];
					$clienteRuta->pago_recolector = $nextZona['valor'];

					$totalRuta += $nextZona['valor'];
					break;
				}
				//En caso de que cliente este en el radio de zona 1
				else if(($key == 1) && ($distanciaCliente < $zona['radio'])){
					//Asignamos el cliente a la ruta
					$clienteRuta->zona = $zona['titulo'];
					$clienteRuta->pago_recolector = $zona['valor'];

					$totalRuta += $zona['valor'];
					break;
				}
			}
		}

		//$ruta->pago_recolector = $ruta->pago_recolector + $nextZona['valor'];
		$ruta->pago_recolector = $ruta->pago_recolector + $totalRuta;
		$ruta->total_clientes = $ruta->total_clientes + 1;
		$ruta->save();

		$clienteRuta->id_cliente = $request->clienteId;
		$clienteRuta->id_ruta = $request->routeId;
		$clienteRuta->tipo_servicio = $cliente->semana_inicial == 0 ? "Recogida Normal" : "Semana Inicial";
		$clienteRuta->status = 'Reprogramado';
		$clienteRuta->save();

		$message = 'Cliente asignado exitosamente a la '.$ruta->titulo;
		#echo $request->routeId." ".$request->dia." ".$request->recolectorId;
		$response = array(
		'status' => true,
		'msg' => $message,
		'recolectorName' => $cliente->name
		);
		return response()->json($response);
	}

	public function updateForEventDay(Request $request){

	 $ruta = RutaLista::find($request->routeId);
	 $cliente = User::find($request->clienteId);
	 $cliente->dia_festivo = 0;
	 $cliente->save();

	 $clienteRuta = new RutaClienteLista;

	 $zonasLista = array();
	 $zonasLista[0] = array("radio"=>0);
	 $cuentaZonas = 1;
	 $totalRuta=0;

	 /* Traemos el listado de zonas*/
	 $zonas = ZonaMapa::all();
	 foreach($zonas as $key => $zona) {
		 $zonasLista[$zona->id] = array(
			 "radio" => $zona->radio,
			 "titulo" => "Zona #".$cuentaZonas,
			 "valor" => $zona->precio
		 );
		 $cuentaZonas++;
	 }
	 $zonasLista[$cuentaZonas] = array("radio"=>10000, "titulo" => "Zona otro");

	 $distanciaCliente = $this->distance(4.74112408575684, -74.04163608173303, floatval($cliente->latitud), floatval($cliente->longitud));

	 /*Verificamos a que zona pertenece el cliente*/
	 foreach($zonasLista as $key => $zona) {
		 if($cuentaZonas>$key){
			 $nextZonaId = $key+1;
			 $nextZona = $zonasLista[$nextZonaId];
			 #print_r($zona);
			 #print_r($nextZona);
			 if($distanciaCliente >= $zona['radio'] && $distanciaCliente <= $nextZona['radio']){
				 #echo "<br> es Zona ".$nextZona['titulo']." DISTANCIA ".$distanciaCliente." -> ".floatval($value->latitud).",".floatval($value->longitud);
				 /*Asignamos el cliente a la ruta*/
				 $clienteRuta->zona = $nextZona['titulo'];
				 $clienteRuta->pago_recolector = $nextZona['valor'];

				 $totalRuta += $nextZona['valor'];
				 break;
			 }
			 //En caso de que cliente este en el radio de zona 1
			 else if(($key == 1) && ($distanciaCliente < $zona['radio'])){
				 //Asignamos el cliente a la ruta
				 $clienteRuta->zona = $zona['titulo'];
				 $clienteRuta->pago_recolector = $zona['valor'];

				 $totalRuta += $zona['valor'];
				 break;
			 }
		 }
	 }

	 //$ruta->pago_recolector = $ruta->pago_recolector + $nextZona['valor'];
	 $ruta->pago_recolector = $ruta->pago_recolector + $totalRuta;
	 $ruta->total_clientes = $ruta->total_clientes + 1;
	 $ruta->save();

	 $clienteRuta->id_cliente = $request->clienteId;
	 $clienteRuta->id_ruta = $request->routeId;
	 $clienteRuta->tipo_servicio = $cliente->semana_inicial == 0 ? "Recogida Normal" : "Semana Inicial";
	 $clienteRuta->status = 'Reprogramado';
	 $clienteRuta->save();

	 $message = 'Cliente asignado exitosamente a la '.$ruta->titulo;
	 #echo $request->routeId." ".$request->dia." ".$request->recolectorId;
	 $response = array(
	 'status' => true,
	 'msg' => $message,
	 'recolectorName' => $cliente->name
	 );
	 return response()->json($response);
 	}

	public function deleteImage(Request $request){
		$id = $request->input("id");
		$fileToDelete = $request->input("fileToDelete");// image, licencia o soat
		if(!isset($id)){
			return response()->json([
				'status' => false,
				'Mensaje' => 'Parámetro invalido.'
			]);
		}
		else{
			$user = User::findOrFail($id);

			if(unlink("uploads/usuarios/".$user->$fileToDelete)){
				$user->$fileToDelete = "no-photo.png";
				$user->save();

				return response()->json([
					'status' => true,
					'mensaje' => "Foto eliminada correctamente."
				]);
			}else{
				return response()->json([
					'status' => false,
					'mensaje' => "No se pudo eliminar la foto."
				]);
			}
		}
	}

	private function distance($lat1, $lon1, $lat2, $lon2) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;

            return ($miles * 1.609344);
        }
    }

	public function setNewDateEvent(Request $request){
		$date = $request->input("date");
		$event = $request->input("event");
		if(!isset($date) && !isset($event)){
			return response()->json([
				'status' => false,
				'Mensaje' => 'Parámetros invalidos.'
			]);
		}
		else{
			$dateArray = explode("-", $date);
  		$diasFestivo = new DiaFestivo;
      $diasFestivo->year = $dateArray[0];
      $diasFestivo->month = $dateArray[1];
      $diasFestivo->day = $dateArray[2];
			$diasFestivo->event = $event;
			$diasFestivo->save();

			return response()->json([
				'status' => true,
				'mensaje' => "Fecha gurdada correctamente."
			]);
		}
	}

	public function updateDateEvent(Request $request){
		$date = $request->input("date");
		$event = $request->input("event");
		$id = $request->input("id");
		if(!isset($date) && !isset($event) && !isset($id)){

			return response()->json([
				'status' => false,
				'Mensaje' => 'Parámetros invalidos.'
			]);
		}
		else{
			$diaFestivo = DiaFestivo::findOrFail($id);
			//if($diaFestivo){
				$dateArray = explode("-", $date);
	      $diaFestivo->year = $dateArray[0];
	      $diaFestivo->month = $dateArray[1];
	      $diaFestivo->day = $dateArray[2];
				$diaFestivo->event = $event;
				$diaFestivo->save();

				return response()->json([
					'status' => true,
					'mensaje' => "Fecha actualizada correctamente."
				]);
			/*}
			else{
				return response()->json([
					'status' => false,
					'Mensaje' => 'No hay dia festivo asignado con ID:'.$id
				]);
			}*/
		}

	}

	public function deleteDateEvent(Request $request){
		$id = $request->input("id");
		if(!isset($id)){
			return response()->json([
				'status' => false,
				'Mensaje' => 'Parámetro invalido.'
			]);
		}
		else{
			$diaFestivo = DiaFestivo::findOrFail($id);
			$diaFestivo->delete();
			return response()->json([
				'status' => true,
				'mensaje' => "Fecha eliminada correctamente."
			]);
		}
	}

	public function validatePickup(Request $request){
		$id = $request->input("parada_id");
		if(!isset($id)){
			return response()->json([
				'status' => false,
				'mensaje' => 'Parámetro invalido.'
			]);
		}
		else{
			$parada = RutaClienteLista::find($id);
			if($parada){
				$parada->aprobada = 1;
				$parada->save();
				return response()->json([
					'status' => true,
					'mensaje' => "Ejecucion aprobada exitosamente."
				]);
			}else{
				return response()->json([
					'status' => false,
					'mensaje' => "No existe una parada con ID:".$id
				]);
			}

		}

	}


	public function getAvailableDays(){
		/*
		$controlDiario = ControlRecoleccionDiaria::all();
		$diasHabilitados = ['lunes' => true, 'martes' => true,'miercoles' => true, 'jueves' => true, 'viernes' => true];
		foreach ($controlDiario as $dia) {
			//Si para el dia la cantidad de clientes registrado es igual a la capacidad maxima,
			//Entoces ese dia ya esta completamente ocupado y se dehabilita.
			if($dia->capacidad_max_recolecta == $dia->cantidad_clientes_subscritos){
				$diasHabilitados[$dia->dia] = false;
			}
		}
		*/
		$diasRecoleccion = ["Mon", "Tue", "Wed", "Thu", "Fri"];
		$diasPermitidos = array();

		foreach($diasRecoleccion as $dia) {
			//Primero determinamos la capacidad de recolección maxima para el dia sumando
			//la capacidad de recoleccion de cada recolector que este disponible para ese dia
			$recolectores = User::where('id_rol', 2)->where('status', 1)->whereJsonContains('dias_recoleccion', [''.$dia => 1])->get();
			$capacidadMaxDia = 0;
			foreach ($recolectores as $value) {
					$capacidadMaxDia += $value->capacidad;
			}
			//Determinanos el total de clientes asignados para ese dia con el fin de validar
			//Si aun no se alcanza la capacidadMaxDia
			$totalClientesAsignados = Cliente::where('status', 1) /*****Activo */
									->where('id_rol', 3) /*****Cliente */
									->whereJsonContains('dias_recoleccion', [''.$dia => 1])
									->where('fecha_vencimiento_plan', '>=', date('Y-m-d', strtotime('next friday')))
									->get()->count();
			$diasPermitidos[$dia] = ($totalClientesAsignados < $capacidadMaxDia) ? true : false;
		}

		return response()->json([
			'status' => true,
			'mensaje' => "Operación exitosa",
			'data' => $diasPermitidos
		]);

	}

}

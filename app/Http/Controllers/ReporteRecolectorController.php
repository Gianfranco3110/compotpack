<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\RecolectorRuta;
use App\RutaLista;
use App\RutaClienteLista;
use App\RecolectorDiaAsignado;
use App\EstadosRecoleccion;
use App\User;

class ReporteRecolectorController extends Controller
{
    protected $customMessages = [
            'required' => 'El campo :attribute es obligatorio.',
            'regex' => 'El campo :attribute debe contener solo letras.',
            'numeric' => 'El campo :attribute debe ser numerico.',
        ];

    protected $niceNames = [
            'titulo' => 'Titulo'
        ];

    public function index(Request $request)
    {
        #print_r($request);
        $fecha_inicio;
        $fecha_fin;
        $today = date('Y-m-d');
        $fecha_maxima = date("d-m-Y",strtotime($today."-1 day"));

        if($request->exists('fecha_inicio') && $request->exists('fecha_fin')) {
            $fecha_inicio = $request->input('fecha_inicio');
            $fecha_fin = $request->input('fecha_fin');
        }
        else{
            $fecha_inicio = date("d-m-Y",strtotime($today."-1 week"));
            $fecha_fin = $fecha_maxima;
        }

        $recolectorRoutes = RutaLista::where('id_recolector', Auth::user()->id)->where('fecha', '>=', $fecha_inicio)->where('fecha', '<=', $fecha_fin)->get();

        $rutasAsignadas = 0;
        $totalClientes = 0;
        $totalRecogidasExitosas = 0;
        $totalRecogidasNoCompletadas = 0;
        $totalPagosPorCobrar = 0;
        $totalPagosRealizados=0;

        foreach ($recolectorRoutes as $key => $value) {
            $rutasAsignadas++;
            $clientes = RutaClienteLista::where('id_ruta', '=', $value->id)->get();
            foreach($clientes as $cliente) {
                $totalClientes++;
                if($cliente->status == 'Recogida') {
                    $totalRecogidasExitosas++;
                    if($cliente->pagado==1) {
                        $totalPagosRealizados += $cliente->pago_recolector;
                    }
                    else{
                        $totalPagosPorCobrar += $cliente->pago_recolector;;
                    }
                }
                else {
                    $totalRecogidasNoCompletadas++;
                }
            }
        }
        
        
        $recoleccionesFallidas = RutaClienteLista::where('status', '<>', 'Recogida')->get();
        
        return view('reporterecolector/index', [
                                    'rutasAsignadas' => $rutasAsignadas,
                                    'totalClientes' => $totalClientes,
                                    'totalRecogidasExitosas' => $totalRecogidasExitosas,
                                    'totalRecogidasNoCompletadas' => $totalRecogidasNoCompletadas,
                                    'totalPagosPorCobrar' => $totalPagosPorCobrar,
                                    'totalPagosRealizados' => $totalPagosRealizados,
                                    'fecha_inicio' => $fecha_inicio,
                                    'fecha_fin' => $fecha_fin,
                                    'fecha_maxima' => $fecha_maxima,
                                    'recolectorRoutes' => $recolectorRoutes
                                ]);
    }

   
}
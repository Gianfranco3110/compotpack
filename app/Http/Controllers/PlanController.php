<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Plan;

class PlanController extends Controller
{
    protected $customMessages = [
            'required' => 'El campo :attribute es obligatorio.',
            'regex' => 'El campo :attribute debe contener solo letras.',
            'numeric' => 'El campo :attribute debe ser numerico.',
        ];

    protected $niceNames = [
            'titulo' => 'Titulo'
        ];

    public function index()
    {
        $plans = Plan::where('status', '<>', 2)->get();
        return view('plan/index' , ['plans' => $plans]);
    }

    public function create()
    {
        return view('plan/create');
    }

    public function store(Request $request)
    {
        $rules = [
            'titulo' => 'required',
            'descripcion' => 'required',
            'precio' => 'required|numeric',
            'frecuencia_cobro' => 'required|numeric'
        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);
        
        $plan = new Plan;
        $plan->titulo = $request->input('titulo');
        $plan->descripcion = $request->input('descripcion');
        $plan->precio = $request->input('precio');
        $plan->frecuencia_cobro = $request->input('frecuencia_cobro');
        $plan->recurrente = $request->has('recurrente') ? 1 : 0;
        $plan->save();

        return redirect('plan')->with('success','Registro guardado con éxito.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $plan = Plan::findOrFail($id);
        return view('plan/edit' , ['plan' => $plan]);
    }

    public function update(Request $request, $id)
    {
        $plan = Plan::findOrFail($id);

        $rules = [
            'titulo' => 'required',
            'descripcion' => 'required',
            'precio' => 'required|numeric',
            'frecuencia_cobro' => 'required|numeric'
        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);

        $plan->titulo = $request->input('titulo');
        $plan->descripcion = $request->input('descripcion');
        $plan->precio = $request->input('precio');
        $plan->frecuencia_cobro = $request->input('frecuencia_cobro');
        $plan->recurrente = $request->has('recurrente') ? 1 : 0;
        $plan->save();

        return redirect('plan')->with('success','Registro editado con éxito.');
    }

    public function destroy($id)
    {
        $plan = Plan::findOrFail($id);
        $plan->status = 2;
        $plan->save();
        return redirect('plan')->with('success','Registro eliminado con éxito.');
    }

    public function setStatus($id)
    {
        $plan = Plan::findOrFail($id);
        $plan->status = $plan->status==1?0:1;
        $plan->save();
        return redirect('plan')->with('success','Estado modificado con éxito.');
    }

    public function messages()
    {
        return [
            'required' => 'ES UN title is required'
        ];
    }
}
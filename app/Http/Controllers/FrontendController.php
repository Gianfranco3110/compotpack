<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use App\Cliente;
use App\Zona;
use App\Ruta;
use App\HistorialPagos;
use App\Configuration;
use App\ZonaMapa;
use App\User;
use App\Plan;
use Auth;

class FrontendController extends Controller
{
    protected $customMessages = [
            'required' => 'El campo :attribute es obligatorio.',
            'email' => 'El campo :attribute debe ser un Email valido.',
            'regex' => 'El campo :attribute debe contener solo letras.',
            'unique' => 'El Email indicado ya se encuentra registrado en la base de datos.',
            'confirmed' => 'El password debe coincidir con la confirmación del password.',
            'numeric' => 'El campo :attribute debe ser numerico.',
            'image' => 'La foto debe ser de un formato valido (jpeg, png, bmp, gif, svg, or webp).',
            'dia_recoleccion.required' => 'Debes escoger al menos un día para la recolección.'
        ];

    protected $niceNames = [
            'name' => 'Nombre Completo',
            'email' => 'Email',
            'id_zona' => 'Zona',
            'dia_recoleccion' => 'Día de recolección'
        ];

    public function index()
    {
        return view('frontend/index');
    }

    public function loginForm()
    {
        return view('frontend/login');
    }

    public function registroForm()
    {
        return view('frontend/registro');
    }
    public function planes()
    {
      $planesOnce = Plan::where('status', 1)->where('recurrente', 0)->orderBy('precio', 'asc')->get();
      $planesRecurrente = Plan::where('status', 1)->where('recurrente', 1)->orderBy('precio', 'asc')->get();
      return view('frontend/planes', [
        'planesOnce' => $planesOnce,
        'planesRecurrente' => $planesRecurrente,
      ]);
    }
    public function plan($planId)
    {
      $plan = Plan::where('id', $planId)->first();
      if($plan){
        return view('frontend/plan', [
          'plan' => $plan
        ]);
      }else{
        return back()->with("error", "El plan seleccionado no existe.");
      }

    }
    public function mensual()
    {
        return view('frontend/mensual');
    }
    public function pago()
    {
        return view('frontend/pago');
    }
    public function tarjeta()
    {
        return view('frontend/tarjeta');
    }
    public function trimestral()
    {
        return view('frontend/trimestral');
    }

    public function logout(Request $request)
    {
      Auth::logout();

      $request->session()->invalidate();

      $request->session()->regenerateToken();

      return redirect()->route('frontend.home');
    }

    public function home()
    {
        return view('frontend/home');
    }
    public function account()
    {
        $user = Auth::user();
        $diasRecoleccion = json_decode($user->dias_recoleccion, true);
        $dia = "";

        foreach($diasRecoleccion as $key => $value) {
          if($value==1){
            $dia = $key;
          }
        }
        $user->dias_recoleccion = $dia;

        return view('frontend/account', ['user' => $user]);
    }

    public function authenticate(Request $request){
      $credentials = $request->only('email', 'password');

      if (Auth::attempt($credentials)) {
          $request->session()->regenerate();

          return redirect()->route('frontend.account');
      }
      return back()->withErrors([
              'email' => 'The provided credentials do not match our records.',
          ]);
    }

    public function signup(Request $request)
    {
      $rules = [
          'nombre' => 'required|regex:/^[a-zA-ZÑñ\s]+$/',
          'apellido' => 'required|regex:/^[a-zA-ZÑñ\s]+$/',
          'email' => 'required|email|unique:users,email',
          'password' => 'required|confirmed',
          'telefono' => 'required|numeric',
          'whatsapp' => 'required|numeric',
          'direccion' => 'required|string',
          'casa_apartamento' => 'required|in:Casa, Apartamento',
          'torre_apto' => 'required|string',
          'dia_recoleccion' => 'required|in:Mon,Tue,Wed,Thu,Fri',
          'pregunta_1' => 'required|string',
          'pregunta_2' => 'required|string',
          'acepto_TyC' => 'required'
          //'image' => 'mimes:jpeg,bmp,png,jpg',
      ];

      $this->validate($request, $rules, $this->customMessages, $this->niceNames);

      $cliente = new Cliente;
      $cliente->name = $request->input('nombre');
      $cliente->apellido = $request->input('apellido');// NUEVO
      $cliente->email = $request->input('email');
      $cliente->password = Hash::make($request->input('password'));
      $cliente->telefono = $request->input('telefono');
      $cliente->whatsapp = $request->input('whatsapp');
      $cliente->direccion = $request->input('direccion');
      $cliente->casa_apartamento = strtolower($request->input('casa_apartamento')); //NUEVO
      $cliente->torre_apto = $request->input('torre_apto'); //NUEVO
      $cliente->pregunta_1 = $request->input('pregunta_1'); //NUEVO
      $cliente->pregunta_2 = $request->input('pregunta_2'); //NUEVO
      $cliente->latitud = $request->input('latitud');
      $cliente->longitud = $request->input('longitud');
      $cliente->id_zona = $request->input('id_zona');
      $cliente->perfil_complete = 1;
      $cliente->id_rol = 3;

      if($request->hasfile('image')) {
          $file = $request->file('image');
          $extension = $file->getClientOriginalExtension();
          $filename = time().'.'.$extension;
          $file->move('uploads/usuarios', $filename);
          $cliente->image = $filename;
      }
      else {
          $cliente->image = 'no-photo.png';
      }

      /****************GENERACION DE JSON PARA LISTA DE DIAS EL CLIENTE********/
      $semana = Config::get('weekDays.week');
      $daysSelected = $request->input('dia_recoleccion');
      $daysJson = Cliente::generateJsonDaysSelected($semana, $daysSelected);
      $cliente->dias_recoleccion = $daysJson;
      $cliente->save();

      /****************ASIGNACION DE RUTA Primera version Omitido********/
      /*foreach ($daysSelected as $key => $day) {

          $rutasExistentes = Ruta::where('id_zona', $request->input('id_zona'))->where('dia', $day)->where('status', 1)->get()->sortBy(function($ruta)
                              {
                                  return $ruta->relatedClients->count();
                              }, true, true);

          $zona = Zona::where('id', $request->input('id_zona'))->first();

          if($rutasExistentes->isEmpty()) {
              $rutaId = Ruta::createRuta($zona, $semana, $day, 1);
          }
          else{
              $banderaAsignacion = false;
              foreach ($rutasExistentes as $key2 => $ruta) {
                  if($ruta->relatedClients->count() < env('MAX_ROUTE')) {
                      $banderaAsignacion = true;
                      $rutaId = $ruta->id;
                      break;
                  }
              }

              if(!$banderaAsignacion) {
                  $rutaId = Ruta::createRuta($zona, $semana, $day, $rutasExistentes->count() + 1);
              }
          }

          Cliente::setRoute($rutaId, $cliente->id);
      }*/

      return redirect()->route('frontend.account')->with('success','Registro guardado con éxito.');
    }

    public function updateAccount(Request $request)
    {
      $rules = [
          'nombre' => 'required|regex:/^[a-zA-ZÑñ\s]+$/',
          'apellido' => 'required|regex:/^[a-zA-ZÑñ\s]+$/',
          'cedula' => 'required',
          'password' => 'required',
          'whatsapp' => 'required|numeric',
          'direccion' => 'required|string',
          'casa_apartamento' => 'required|in:Casa, Apartamento',
          'torre_apto' => 'required|string',
          'dia_recoleccion' => 'required|in:Mon,Tue,Wed,Thu,Fri',
          'pregunta_1' => 'required|string',
          'pregunta_2' => 'required|string',
          //'image' => 'mimes:jpeg,bmp,png,jpg',
      ];

      $this->validate($request, $rules, $this->customMessages, $this->niceNames);

      $cliente = Cliente::findOrFail(Auth::user()->id);

      $cliente->name = $request->input('nombre');
      $cliente->apellido = $request->input('apellido');// NUEVO
      $cliente->cedula = $request->input('cedula');// NUEVO
      $cliente->password = Hash::make($request->input('password'));
      $cliente->telefono = $request->input('telefono');
      $cliente->whatsapp = $request->input('whatsapp');
      $cliente->direccion = $request->input('direccion');
      $cliente->casa_apartamento = strtolower($request->input('casa_apartamento')); //NUEVO
      $cliente->torre_apto = $request->input('torre_apto'); //NUEVO
      $cliente->pregunta_1 = $request->input('pregunta_1'); //NUEVO
      $cliente->pregunta_2 = $request->input('pregunta_2'); //NUEVO
      //$cliente->latitud = $request->input('latitud');
      //$cliente->longitud = $request->input('longitud');
      //$cliente->id_zona = $request->input('id_zona');
      $cliente->perfil_complete = 1;
      $cliente->id_rol = 3;

      if($request->hasfile('image')) {
          $file = $request->file('image');
          $extension = $file->getClientOriginalExtension();
          $filename = time().'.'.$extension;
          $file->move('uploads/usuarios', $filename);
          $cliente->image = $filename;
      }
      else {
          $cliente->image = 'no-photo.png';
      }

      /****************GENERACION DE JSON PARA LISTA DE DIAS EL CLIENTE********/
      $semana = Config::get('weekDays.week');
      $daysSelected = $request->input('dia_recoleccion');
      $daysJson = Cliente::generateJsonDaysSelected($semana, $daysSelected);
      $cliente->dias_recoleccion = $daysJson;
      $cliente->save();

      return redirect('app/account')->with('success','¡Cambios guardados con éxito!');
    }


    public function destroy($id)
    {
        $cliente = Cliente::findOrFail($id);
        $cliente->status = 2;
        $cliente->save();

        /*******************LIMPIEZA DE REGISTROS EXISTENTES DEL CLIENTE**************/
        Cliente::cleanClientRoutesRegister($id);

        return redirect('cliente')->with('success','Registro eliminado con éxito.');
    }


    public function messages()
    {
        return [
            'required' => 'ES UN title is required'
        ];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use App\Ruta;
use App\RutaLista;
use App\Zona;
use App\ZonaMapa;
use App\User;
use App\Configuration;
use App\Cliente;
use App\DiaFestivo;

class RutaController extends Controller
{
    protected $customMessages = [
            'required' => 'El campo :attribute es obligatorio.',
            'regex' => 'El campo :attribute debe contener solo letras.',
            'numeric' => 'El campo :attribute debe ser numerico.',
        ];

    protected $niceNames = [
            'titulo' => 'Titulo'
        ];

    public function index($daySelected, $weekSelected)
    {
        $semanaTranslate = Config::get('weekDays.week');

        $capacidadClientesPorRuta = Configuration::find(1)->value;

        if($daySelected==0) { /*****Si entra por primera vez el dia seleccionado es el dia en curso*/
            $daySelected = date('Y-m-d');
            if(date('D',strtotime($daySelected)) == 'Sun' || date('D',strtotime($daySelected)) == 'Sat') {
                $daySelected = date('Y-m-d', strtotime('next monday'));
            }
            //$weekSelected = 0;
        }

        $arraySemanas = array();

        $rutas = RutaLista::where('fecha', $daySelected)->get();

        for($i=1; $i<=8; $i++) {
            if($i==1) {
                $fecha = date('Y-m-d');
            }
            else{
                $fecha = date("d-m-Y",strtotime($fecha."+ 1 week"));
            }
            $lunes =  date( 'Y-m-d', strtotime( 'monday this week', strtotime($fecha) ) );
            $viernes =  date( 'Y-m-d', strtotime( 'friday this week', strtotime($fecha) ) );

            $arraySemanas[$i] = [
                            "Mon" => $lunes,
                            "Fri" => $viernes
                           ];

            /**************Verificamos en que semana se encuentra la fecha seleccionada*****/////
            if($weekSelected==0){
              if($daySelected >= $lunes && $daySelected <= $viernes) {
                  $weekSelected = $i;
              }
            }
            elseif($weekSelected == $i){
                $daySelected = $lunes;
            }
        }

        #$daySelected = "2021-12-12";
        $dayAcron = date('D', strtotime($daySelected));


        //Cargar recolectores disponibles para ese dia
        $recolectores = User::where('status', 1)->where('id_rol', 2)
                            ->whereJsonContains('dias_recoleccion', [''.$dayAcron => 1])
                            ->orderBy('name')->get();

        $recolectoresDisponibles = array();

        foreach ($recolectores as $recolector) {
          //Buscamos las rutas para el recolector
          $rutasRecolector = RutaLista::where('id_recolector', $recolector->id)->get();
          if(count($rutasRecolector)>0){
            // Si el recolector tiene rutas asignadas, sumamos la cantidad total
            // de clientes que tiene segun las rutas
            $sumaClientesPorRuta = 0;
            foreach ($rutasRecolector as $ruta) {
              $sumaClientesPorRuta += $ruta->total_clientes;
            }
            // Si esa cantidad no sobre pasa la capacidad del recolector por dia,
            // entonces tiene disponibilidad para mas asignaciones
            if($sumaClientesPorRuta < $recolector->capacidad){
              array_push($recolectoresDisponibles, $recolector);
            }

          }else{
            array_push($recolectoresDisponibles, $recolector);
          }

        }

        /*Si no existen rutas en BD para este dia se procede a mostrar la proyeccion*/
        $flagRutasEnDb = true;

        if(!$rutas && $weekSelected>1) {
            $rutas = $this->proyectionRoute2($daySelected, $recolectores);
            $flagRutasEnDb = false;
        }

        /*Buscamos los clientes novedad*/
        $clientesNovedad = Cliente::where('status', 1) /*****Activo */
                        ->where('id_rol', 3) /*****Cliente */
                        ->where('novedad', 1) /* Novedad */
                        ->OrderBy('latitud','desc') /*****Norte a sur */
                        ->where('fecha_vencimiento_plan', '>=', date('Y-m-d', strtotime($daySelected)))
                        ->get();

        /*Cargamos clientes que no pudieron ser atendidos por dia festivo*/
        $clientesDiaFestivo = Cliente::where('status', 1) /*****Activo */
                        ->where('id_rol', 3) /*****Cliente */
                        ->where('dia_festivo', 1) /* Novedad */
                        ->OrderBy('latitud','desc') /*****Norte a sur */
                        ->where('fecha_vencimiento_plan', '>=', date('Y-m-d', strtotime($daySelected)))
                        ->get();

        return view('ruta/index' , [
                                    #'zonas' => $zonas,
                                    'dayAcron' => $dayAcron,
                                    'weekSelected' => $weekSelected,
                                    'daySelected' => $daySelected,
                                    'arraySemanas' => $arraySemanas,
                                    'semanaTranslate' => $semanaTranslate,
                                    'recolectores' => $recolectoresDisponibles,
                                    'rutas' => $rutas,
                                    'capacidadClientesPorRuta' => $capacidadClientesPorRuta,
                                    'flagRutasEnDb' => $flagRutasEnDb,
                                    'clientesNovedad' => $clientesNovedad,
                                    'clientesDiaFestivo' => $clientesDiaFestivo
                                    ]);
    }

    public function proyectionRoute($day, $recolectores) {
        /*Listamos todos los recolectores disponibles */
        $listRecolectores = array();

        foreach($recolectores as $recolector) {
            $listRecolectores[] = array(
                "id_recolector" => $recolector->id,
                "capacidad" => $recolector->capacidad
            );
        }

        $firstDay = $day;

        #$firstDay = date('Y-m-d', strtotime( $firstDay . " +1 days"));
        $acrDay = date('D', strtotime($firstDay));
        #echo "<br>---------------------------". $firstDay." ".$acrDay." <br>";

        /*************DEFINICION DE CLIENTES DE RECOLECION */
        $cuentaRuta = 1;
        $cuentaClientes = 0;
        $capacidadRuta = Configuration::find(1)->value;
        $ruta = array();
        $clienteRuta = array();
        $zonasLista = array();
        $zonasLista[0] = array("radio"=>0);
        $cuentaZonas = 1;
        $totalRuta=0;

        /* Traemos el listado de zonas*/
        $zonas = ZonaMapa::all();
        foreach($zonas as $key => $zona) {
            $zonasLista[$zona->id] = array(
                "radio" => $zona->radio,
                "titulo" => "Zona #".$cuentaZonas,
                "valor" => $zona->precio
            );
            $cuentaZonas++;
        }
        $zonasLista[$cuentaZonas] = array("radio"=>10000, "titulo" => "Zona otro", "valor"=>0);

        #$clientes = Cliente::where('status', '<>', 2)->where('id_rol', 3)->get();

        $clientesRecoleccion = Cliente::where('status', 1) /*****Activo */
                        ->where('id_rol', 3) /*****Cliente */
                        ->where('semana_inicial', 0) /*****Cliente para recoleccion */
                        ->where('novedad', 0) /* No novedad */
                        ->whereJsonContains('dias_recoleccion', [''.$acrDay => 1]) /*****Dia Lunes*/
                        ->where('fecha_vencimiento_plan', '>=', date('Y-m-d', strtotime($firstDay)))
                        ->OrderBy('latitud','asc') /*****Sur a norte */
                        ->get();

        #echo "<br>Antes";

        #echo "SEMANA NORMAL ".$clientesRecoleccion->count();

        if($clientesRecoleccion->count()!=0){ /*Si hay clientes*/
            //print_r($firstElement);
            #echo "<br><br><br>-----Ruta 1";
            /*Listado de Clientes orden norte a sur*/

            $ruta[$cuentaRuta] = array(
                "titulo" => "Ruta #".$cuentaRuta,
                "fecha" => date('Y-m-d', strtotime($firstDay))
            );


            foreach($clientesRecoleccion as $key => $value) {
                #echo "<br>------>Normal ".$value->name;
                if($cuentaClientes == $capacidadRuta) {

                    $ruta[$cuentaRuta]['listadoClientes'] = $clienteRuta;
                    $ruta[$cuentaRuta]['pagoRecolector'] = $totalRuta;
                    $totalRuta = 0;
                    $clienteRuta = array();

                    $cuentaRuta++;

                    /*Creamo la nueva ruta*/
                    $ruta[$cuentaRuta] = array(
                        "titulo" => "Ruta #".$cuentaRuta,
                        "fecha" => date('Y-m-d', strtotime($firstDay))
                    );

                    #echo "<br><br><br>-----Ruta ".$cuentaRuta;
                    $cuentaClientes = 0;
                }

                $distanciaCliente = $this->distance(4.74112408575684, -74.04163608173303, floatval($value->latitud), floatval($value->longitud));

                /*Verificamos a que zona pertenece el cliente*/
                foreach($zonasLista as $key => $zona) {
                    if($cuentaZonas>$key){
                        $nextZonaId = $key+1;
                        $nextZona = $zonasLista[$nextZonaId];
                        /*echo "<br>Zona";
                        print_r($zona);
                        echo "<br>NextZona";
                        print_r($nextZona);*/
                        if($distanciaCliente >= $zona['radio'] && $distanciaCliente <= $nextZona['radio']){
                            //echo "<br> es Zona ".$nextZona['titulo']." DISTANCIA ".$distanciaCliente." -> ".floatval($value->latitud).",".floatval($value->longitud);
                            //Asignamos el cliente a la ruta
                            $clienteRuta[$cuentaClientes] = array(
                                "idCliente" => $value->id,
                                "nombre" => $value->name,
                                "tipo" => "Recogida Normal",
                                "distancia" => $distanciaCliente,
                                "pagoRecolecto" => $nextZona['valor'],
                                "zona" => $nextZona['titulo'],
                            );
                            $totalRuta += $nextZona['valor'];
                            break;
                        }
                    }
                }

                $cuentaClientes++;
                $listPoints[$key] = $value;
                #echo "<br> ----".$value->name;
            }

            /*Asignamos los clientes restantes a la ultima ruta*/
            $ruta[$cuentaRuta]['listadoClientes'] = $clienteRuta;
            $ruta[$cuentaRuta]['pagoRecolector'] = $totalRuta;
        }

        $clientesSemanaInicial = Cliente::where('status', 1) /*****Activo */
                        ->where('id_rol', 3) /*****Cliente */
                        ->where('semana_inicial', 1) /*****Cliente Semana 0 */
                        ->where('novedad', 0) /* No novedad */
                        ->whereJsonContains('dias_recoleccion', [''.$acrDay => 1]) /*****Dia Lunes*/
                        ->OrderBy('latitud','desc') /*****Norte a sur */
                        ->where('fecha_vencimiento_plan', '>=', date('Y-m-d', strtotime($firstDay)))
                        ->get();

                        #echo "SEMANA INICIAL ".$clientesSemanaInicial->count();

        if($clientesSemanaInicial->count()!=0){ /*Si hay clientes*/
            foreach($clientesSemanaInicial as $key => $value) {
                #echo "<br>------> Inicial".$value->name;
                if($cuentaClientes == $capacidadRuta) {

                    $ruta[$cuentaRuta]['listadoClientes'] = $clienteRuta;
                    $ruta[$cuentaRuta]['pagoRecolector'] = $totalRuta;
                    $totalRuta = 0;
                    $clienteRuta = array();

                    $cuentaRuta++;

                    /*Creamo la nueva ruta*/
                    $ruta[$cuentaRuta] = array(
                        "titulo" => "Ruta #".$cuentaRuta,
                        "fecha" => date('Y-m-d', strtotime($firstDay))
                    );

                    #echo "<br><br><br>-----Ruta ".$cuentaRuta;
                    $cuentaClientes = 0;
                }

                $distanciaCliente = $this->distance(4.74112408575684, -74.04163608173303, floatval($value->latitud), floatval($value->longitud));
                /*Verificamos a que zona pertenece el cliente*/
                foreach($zonasLista as $key => $zona) {
                    if($cuentaZonas>$key){
                        $nextZonaId = $key+1;
                        $nextZona = $zonasLista[$nextZonaId];
                        #print_r($zona);
                        #print_r($nextZona);
                        if($distanciaCliente >= $zona['radio'] && $distanciaCliente <= $nextZona['radio']){
                            #echo "<br> es Zona ".$nextZona['titulo']." DISTANCIA ".$distanciaCliente." -> ".floatval($value->latitud).",".floatval($value->longitud);

                            $clienteRuta[$cuentaClientes] = array(
                                "idCliente" => $value->id,
                                "nombre" => $value->name,
                                "tipo" => "Semana Inicial",
                                "distancia" => $distanciaCliente,
                                "pagoRecolecto" => $nextZona['valor'],
                                "zona" => $nextZona['titulo'],
                            );
                            $totalRuta += $nextZona['valor'];
                            #exit("AQUI VA");
                            break;
                        }
                    }
                }

                $cuentaClientes++;
                $listPoints[$key] = $value;
                #echo "<br> ----".$value->name;
            }

            /*Asignamos los clientes restantes a la ultima ruta*/
            $ruta[$cuentaRuta]['listadoClientes'] = $clienteRuta;
            $ruta[$cuentaRuta]['pagoRecolector'] = $totalRuta;
        }

        #print("<pre>".print_r($ruta,true)."</pre>");
        return $ruta;
    }

    public function proyectionRoute2($day, $recolectores) {

      $firstDay = (string) $day;
      $acrDay = date('D', strtotime($firstDay));

      /*Listamos todos los recolectores disponibles para este dia*/
      $listRecolectores = array();
      $recolectores = User::where('id_rol',2)->where('status', 1)
                          ->whereJsonContains('dias_recoleccion', [''.$acrDay => 1])
                          ->get();
      foreach($recolectores as $recolector) {
          array_push($listRecolectores, array(
                "id_recolector" => $recolector->id,
                "capacidad" => $recolector->capacidad,
                //"diasRecoleccion" => json_decode($recolector->dias_recoleccion, true)
              )
          );
      }

      //Verificamos que esa fecha no sea un dia festivo
      $dayArray = explode("-", $firstDay);
      $dayEvent = DiaFestivo::where("year", $dayArray[0])->where("month", $dayArray[1])->where("day", $dayArray[2])->get();

      //Si la fecha no esta registra como dia festivo, se procede con las asignaciones
      if($dayEvent->count()==0){
        /*************DEFINICION DE CLIENTES DE RECOLECION */
        $cuentaRuta = 1;
        $cantidadClientesAsignados = 0;
        $capacidadRuta = Configuration::find(1)->value;
        $ruta = array();
        $listaClienteParaRuta = array();
        $zonasLista = array();
        $totalRuta=0;
        $cantidadZonas = 0;

        //Traemos el listado de zonas
        $zonas = ZonaMapa::all();
        foreach($zonas as $key => $zona) {
            $zonasLista[$zona->id] = array(
                "radio" => $zona->radio,
                "titulo" => "Zona #".($cantidadZonas+1),
                "valor" => $zona->precio
            );
            $cantidadZonas++;
        }
        //Esta zona ficticia representa la zona restante que queda despues
        //de la ultima zona disponible
        $zonasLista[sizeof($zonasLista)] = array("radio"=>10000, "titulo" => "Zona otro", "valor"=>0);

        $cantidadZonas = sizeof($zonasLista);

        //$clientes = Cliente::where('status', '<>', 2)->where('id_rol', 3)->get();

        $clientesRecoleccion = Cliente::where('status', 1) /*****Activo */
                        ->where('id_rol', 3) /*****Cliente */
                        ->where('semana_inicial', 0) /*****Cliente que no está asignado a la semana de recoleccion */
                        ->where('novedad', 0) /* No novedad */
                        ->whereJsonContains('dias_recoleccion', [''.$acrDay => 1]) /*****Dia Lunes*/
                        ->where('fecha_vencimiento_plan', '>=', date('Y-m-d', strtotime($firstDay)))
                        ->OrderBy('latitud','asc') /*****Sur a norte */
                        ->get();

        if($clientesRecoleccion->count()!=0){ /*Si hay clientes*/
          /*Listado de Clientes orden norte a sur*/
          $ruta[$cuentaRuta] = array(
              "titulo" => "Ruta #".$cuentaRuta,
              "fecha" => date('Y-m-d', strtotime($firstDay))
          );

          foreach($clientesRecoleccion as $key => $value) {
            // Si la cantidad de clientes ya asignados alcanza la capacidad de clientes
            // por ruta, se guarda el listado de clientes creados hasta el momento junto con
            // el monto total de esa ruta para el recolector
            if($cantidadClientesAsignados == $capacidadRuta) {

                $ruta[$cuentaRuta]['listadoClientes'] = $listaClienteParaRuta;
                $ruta[$cuentaRuta]['pagoRecolector'] = $totalRuta;

                //Creamos la siguiente ruta
                $cuentaRuta++;
                $ruta[$cuentaRuta] = array(
                    "titulo" => "Ruta #".$cuentaRuta,
                    "fecha" => date('Y-m-d', strtotime($firstDay))
                );
                //Y se reinician las variables y se procede a continuar a hacer las asignaciones
                $totalRuta = 0;
                $listaClienteParaRuta = array();
                $cantidadClientesAsignados = 0;
            }

            //Distancia del cliente con respecto a la sede.
            $distanciaCliente = $this->distance(4.74112408575684, -74.04163608173303, floatval($value->latitud), floatval($value->longitud));

            //Verificamos a que zona pertenece el cliente
            foreach($zonasLista as $key => $zona) {
              if($cantidadZonas>$key){
                $nextZonaId = $key+1;
                $nextZona = $zonasLista[$nextZonaId];

                // Como las zonas se definen como el area circunferencial de la diferencia
                // entre el area circular con radio de ZonaActual y area circular con radio de ZonaSiguiente,
                // debemos verificar que el cliente este en en algun punto de esa area.
                if($distanciaCliente >= $zona['radio'] && $distanciaCliente <= $nextZona['radio']){
                  //Asignamos el cliente a la ruta
                  $listaClienteParaRuta[$cantidadClientesAsignados] = array(
                      "idCliente" => $value->id,
                      "nombre" => $value->name,
                      "tipo" => "Semana Inicial",
                      "distancia" => $distanciaCliente,
                      "pagoRecolecto" => $nextZona['valor'],
                      "zona" => $nextZona['titulo'],
                  );
                  $totalRuta += $nextZona['valor'];
                  break;
                }
                //En caso de que cliente este en el radio de zona 1
                else if(($key == 1) && ($distanciaCliente < $zona['radio'])){
                  //Asignamos el cliente a la ruta
                  $listaClienteParaRuta[$cantidadClientesAsignados] = array(
                      "idCliente" => $value->id,
                      "nombre" => $value->name,
                      "tipo" => "Semana Inicial",
                      "distancia" => $distanciaCliente,
                      "pagoRecolecto" => $zona['valor'],
                      "zona" => $zona['titulo'],
                  );
                  $totalRuta += $zona['valor'];
                  break;
                }
              }
            }

            $cantidadClientesAsignados++;
            $listPoints[$key] = $value;
          }

          /*Asignamos los clientes restantes a la ultima ruta*/
          $ruta[$cuentaRuta]['listadoClientes'] = $listaClienteParaRuta;
          $ruta[$cuentaRuta]['pagoRecolector'] = $totalRuta;
        }

        $clientesSemanaInicial = Cliente::where('status', 1) /*****Activo */
                        ->where('id_rol', 3) /*****Cliente */
                        ->where('semana_inicial', 1) /*****Cliente Semana 0 */
                        ->where('novedad', 0) /* No novedad */
                        ->whereJsonContains('dias_recoleccion', [''.$acrDay => 1]) /*****Dia Lunes*/
                        ->OrderBy('latitud','desc') /*****Norte a sur */
                        ->where('fecha_vencimiento_plan', '>=', date('Y-m-d', strtotime($firstDay)))
                        ->get();

        if($clientesSemanaInicial->count()!=0){ /*Si hay clientes*/

          $ruta[$cuentaRuta] = array(
              "titulo" => "Ruta #".$cuentaRuta,
              "fecha" => date('Y-m-d', strtotime($firstDay))
          );
          foreach($clientesSemanaInicial as $key => $value) {
            // Si la cantidad de clientes ya asignados alcanza la capacidad de clientes
            // por ruta, se guarda el listado de clientes creados hasta el momento junto con
            // el monto total de esa ruta para el recolector
            if($cantidadClientesAsignados == $capacidadRuta) {

                $ruta[$cuentaRuta]['listadoClientes'] = $listaClienteParaRuta;
                $ruta[$cuentaRuta]['pagoRecolector'] = $totalRuta;

                //Creamos la siguiente ruta
                $cuentaRuta++;
                $ruta[$cuentaRuta] = array(
                    "titulo" => "Ruta #".$cuentaRuta,
                    "fecha" => date('Y-m-d', strtotime($firstDay))
                );
                //Y se reinician las variables y se procede a continuar a hacer las asignaciones
                $totalRuta = 0;
                $listaClienteParaRuta = array();
                $cantidadClientesAsignados = 0;
            }

            //Distancia del cliente con respecto a la sede.
            $distanciaCliente = $this->distance(4.74112408575684, -74.04163608173303, floatval($value->latitud), floatval($value->longitud));

            //Verificamos a que zona pertenece el cliente
            foreach($zonasLista as $key => $zona) {
              if($cantidadZonas>$key){
                $nextZonaId = $key+1;
                $nextZona = $zonasLista[$nextZonaId];

                // Como las zonas se definen como el area circunferencial de la diferencia
                // entre el area circular con radio de ZonaActual y area circular con radio de ZonaSiguiente,
                // debemos verificar que el cliente este en en algun punto de esa area.
                if($distanciaCliente >= $zona['radio'] && $distanciaCliente <= $nextZona['radio']){
                  //Asignamos el cliente a la ruta
                  $listaClienteParaRuta[$cantidadClientesAsignados] = array(
                      "idCliente" => $value->id,
                      "nombre" => $value->name,
                      "tipo" => "Semana Inicial",
                      "distancia" => $distanciaCliente,
                      "pagoRecolecto" => $nextZona['valor'],
                      "zona" => $nextZona['titulo'],
                  );
                  $totalRuta += $nextZona['valor'];
                  break;
                }
                //En caso de que cliente este en el radio de zona 1
                else if(($key == 1) && ($distanciaCliente < $zona['radio'])){
                  //Asignamos el cliente a la ruta
                  $listaClienteParaRuta[$cantidadClientesAsignados] = array(
                      "idCliente" => $value->id,
                      "nombre" => $value->name,
                      "tipo" => "Semana Inicial",
                      "distancia" => $distanciaCliente,
                      "pagoRecolecto" => $zona['valor'],
                      "zona" => $zona['titulo'],
                  );
                  $totalRuta += $zona['valor'];
                  break;
                }
              }
            }

            $cantidadClientesAsignados++;
            $listPoints[$key] = $value;
          }

          //Guardamos el listado actual y el monto total para la ruta actual.
          $ruta[$cuentaRuta]['listadoClientes'] = $listaClienteParaRuta;
          $ruta[$cuentaRuta]['pagoRecolector'] = $totalRuta;
        }

        /*
        Esta seccion se comenta porque esta ruta es una proyeccion  y no se guarda en DB
        if(count($ruta)>0){
          $this->storeRoutes2($ruta, $listRecolectores);
          //return $listRecolectores;
        }else{
          //no hay clientes para este dia;
        }
        */

      }
      else{
        //Si la fecha aparece como dia festivo, se ignora y se procede con el siguiente dia.
        //array_push($response, array('fecha' => $firstDay, 'nota'=>"dia festivo"));
      }

      return  $ruta;
    }

    private function distance($lat1, $lon1, $lat2, $lon2) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;

            return ($miles * 1.609344);
        }
    }

    public function create()
    {
        return view('ruta/create');
    }

    public function store(Request $request)
    {
        $rules = [
            'titulo' => 'required',
            'descripcion' => 'required',
            'precio' => 'required|numeric',
            'frecuencia_cobro' => 'required|numeric'
        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);

        $ruta = new Ruta;
        $ruta->titulo = $request->input('titulo');
        $ruta->descripcion = $request->input('descripcion');
        $ruta->precio = $request->input('precio');
        $ruta->frecuencia_cobro = $request->input('frecuencia_cobro');
        $ruta->recurrente = $request->has('recurrente') ? 1 : 0;
        $ruta->save();

        return redirect('ruta')->with('success','Registro guardado con éxito.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $ruta = Ruta::findOrFail($id);
        return view('ruta/edit' , ['ruta' => $ruta]);
    }

    public function update(Request $request, $id)
    {
        $ruta = Ruta::findOrFail($id);

        $rules = [
            'titulo' => 'required',
            'descripcion' => 'required',
            'precio' => 'required|numeric',
            'frecuencia_cobro' => 'required|numeric'
        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);

        $ruta->titulo = $request->input('titulo');
        $ruta->descripcion = $request->input('descripcion');
        $ruta->precio = $request->input('precio');
        $ruta->frecuencia_cobro = $request->input('frecuencia_cobro');
        $ruta->recurrente = $request->has('recurrente') ? 1 : 0;
        $ruta->save();

        return redirect('ruta')->with('success','Registro editado con éxito.');
    }

    public function destroy($id)
    {
        $ruta = Ruta::findOrFail($id);
        $ruta->status = 2;
        $ruta->save();
        return redirect('ruta')->with('success','Registro eliminado con éxito.');
    }

    public function setStatus($id)
    {
        $ruta = Ruta::findOrFail($id);
        $ruta->status = $ruta->status==1?0:1;
        $ruta->save();
        return redirect('ruta')->with('success','Estado modificado con éxito.');
    }

    public function messages()
    {
        return [
            'required' => 'ES UN title is required'
        ];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\ZonaMapa;
use App\Cliente;
use App\Configuration;

class MapaController extends Controller
{
    public function index()
    {
        $zonas = ZonaMapa::all();
        $clientes = Cliente::where('status', '<>', 2)->where('id_rol', 3)->get();
        $qMaxRoute = Configuration::find(1);
        return view('mapa/index', ['zonas' => $zonas, 'clientes' => $clientes, 'qMaxRoute' => $qMaxRoute]);
    }

    public function store(Request $request)
    {
        $colores = ["#DFFF00", "#FFBF00", "#FF7F50", "#DE3163", "#9FE2BF", "#40E0D0", "#6495ED", "#CCCCFF", "#DF8FF9", "#F98FDC", "#F98F9A", "#F6F98F", "#AEF98F", "#8FF9F4"];
        $totalZonas = $request->input('totalZonas');
        ZonaMapa::truncate();
        for ($i=0; $i < $totalZonas; $i++) { 
        	$zona = new ZonaMapa;
        	$zona->titulo = "-";
        	$zona->color = $colores[$i];
        	$zona->radio = $request->input('radio_'.$i);        	
        	$zona->precio = $request->input('precio_'.$i);   
        	$zona->save();
        }

        $qMaxRoute = Configuration::findOrFail(1);
        $qMaxRoute->value = $request->input('cantidad_maxima');  
        $qMaxRoute->save();

        return redirect('mapa')->with('success','Datos guardados con éxito.');
    }
}

<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\RecolectorDiaAsignado;
use App\DiaFestivo;

//Para simular el algoritmo
use App\Cliente;
use App\Zona;
use App\Ruta;
use App\Configuration;
use App\ZonaMapa;
use App\RutaLista;
use App\RutaClienteLista;
use App\User;
use App\Inventarios;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $role = Auth::user()->roles()->first()->id;
        //$simulacion = $this->simulacion();
        //return $simulacion;
        //return view('home', ['simulacion' => $simulacion]);
        return view('home');
    }

    public function reasignarFechas(Request $request, $fecha=null){

      //$diasAsignados = RecolectorDiaAsignado::all();
      //return view('reasignar.reasignarFechas', ['diasAsignados' => $diasAsignados]);
      if(!isset($fecha)){
  			$fecha = date('Y-m-d');
  		}

      $dateArray = explode("-", $fecha);

      $diasFestivos = [];
  		$diasFestivos = DiaFestivo::all();

      $festividadesParaFecha = [];
  		$festividadesParaFecha = DiaFestivo::where("year", $dateArray[0])->where("month", $dateArray[1])->where("day", $dateArray[2])->get();
  		/*foreach ($result as $item) {
  			$diaFestivo = $item->relatedRecolector()->first();
  			array_push($diasFestivos, $diaFestivo);
  		}*/
      return view('reasignar.reasignarFechas', ['diasFestivos' => $diasFestivos, 'festividadesParaFecha' => $festividadesParaFecha, 'dia' => $fecha]);
    }


    public function simulacion(){

      $firstDay = date("Y-m-d", strtotime('next sunday'));
      $nextFriday = date("Y-m-d", strtotime('next friday'));
      $response = array();

      //$firstDay = '2021-05-02';
      //$nextFriday = '2021-05-07';

      while($firstDay != $nextFriday) {
        //Dia en formato fecha
        $firstDay = date('Y-m-d', strtotime( $firstDay . " +1 days"));
        //Dia con formato de acronimo. // Ejemplo: Mon, Tue, Wed, etc.
        $acrDay = date('D', strtotime($firstDay));

        /*Listamos todos los recolectores disponibles para este dia*/
        $listRecolectores = array();
        $recolectores = User::where('id_rol',2)->where('status', 1)
                            ->whereJsonContains('dias_recoleccion', [''.$acrDay => 1])
                            ->get();
        foreach($recolectores as $recolector) {
            array_push($listRecolectores, array(
                  "id_recolector" => $recolector->id,
                  "capacidad" => $recolector->capacidad,
                  //"diasRecoleccion" => json_decode($recolector->dias_recoleccion, true)
                )
            );
        }

        //Verificamos que esa fecha no sea un dia festivo
        $dayArray = explode("-", $firstDay);
        $dayEvent = DiaFestivo::where("year", $dayArray[0])->where("month", $dayArray[1])->where("day", $dayArray[2])->get();

        //Si la fecha no esta registra como dia festivo, se procede con las asignaciones
        if($dayEvent->count()==0){
          /*************DEFINICION DE CLIENTES DE RECOLECION */
          $cuentaRuta = 1;
          $cantidadClientesAsignados = 0;
          $capacidadRuta = Configuration::find(1)->value;
          $ruta = array();
          $listaClienteParaRuta = array();
          $zonasLista = array();
          $totalRuta=0;
          $cantidadZonas = 0;

          //Traemos el listado de zonas
          $zonas = ZonaMapa::all();
          foreach($zonas as $key => $zona) {
              $zonasLista[$zona->id] = array(
                  "radio" => $zona->radio,
                  "titulo" => "Zona #".($cantidadZonas+1),
                  "valor" => $zona->precio
              );
              $cantidadZonas++;
          }
          //Esta zona ficticia representa la zona restante que queda despues
          //de la ultima zona disponible
          $zonasLista[sizeof($zonasLista)] = array("radio"=>10000, "titulo" => "Zona otro", "valor"=>0);

          $cantidadZonas = sizeof($zonasLista);

          //$clientes = Cliente::where('status', '<>', 2)->where('id_rol', 3)->get();

          $clientesRecoleccion = Cliente::where('status', 1) /*****Activo */
                          ->where('id_rol', 3) /*****Cliente */
                          ->where('semana_inicial', 0) /*****Cliente que no está asignado a la semana de recoleccion */
                          ->where('novedad', 0) /* No novedad */
                          ->whereJsonContains('dias_recoleccion', [''.$acrDay => 1]) /*****Dia Lunes*/
                          ->where('fecha_vencimiento_plan', '>=', date('Y-m-d', strtotime($firstDay)))
                          ->OrderBy('latitud','asc') /*****Sur a norte */
                          ->get();

          if($clientesRecoleccion->count()!=0){ /*Si hay clientes*/
            /*Listado de Clientes orden norte a sur*/
            $ruta[$cuentaRuta] = array(
                "titulo" => "Ruta #".$cuentaRuta,
                "fecha" => date('Y-m-d', strtotime($firstDay))
            );

            foreach($clientesRecoleccion as $key => $value) {
              // Si la cantidad de clientes ya asignados alcanza la capacidad de clientes
              // por ruta, se guarda el listado de clientes creados hasta el momento junto con
              // el monto total de esa ruta para el recolector
              if($cantidadClientesAsignados == $capacidadRuta) {

                  $ruta[$cuentaRuta]['listadoClientes'] = $listaClienteParaRuta;
                  $ruta[$cuentaRuta]['pagoRecolector'] = $totalRuta;

                  //Creamos la siguiente ruta
                  $cuentaRuta++;
                  $ruta[$cuentaRuta] = array(
                      "titulo" => "Ruta #".$cuentaRuta,
                      "fecha" => date('Y-m-d', strtotime($firstDay))
                  );
                  //Y se reinician las variables y se procede a continuar a hacer las asignaciones
                  $totalRuta = 0;
                  $listaClienteParaRuta = array();
                  $cantidadClientesAsignados = 0;
              }

              //Distancia del cliente con respecto a la sede.
              $distanciaCliente = $this->distance(4.74112408575684, -74.04163608173303, floatval($value->latitud), floatval($value->longitud));

              //Verificamos a que zona pertenece el cliente
              foreach($zonasLista as $key => $zona) {
                if($cantidadZonas>$key){
                  $nextZonaId = $key+1;
                  $nextZona = $zonasLista[$nextZonaId];

                  // Como las zonas se definen como el area circunferencial de la diferencia
                  // entre el area circular con radio de ZonaActual y area circular con radio de ZonaSiguiente,
                  // debemos verificar que el cliente este en en algun punto de esa area.
                  if($distanciaCliente >= $zona['radio'] && $distanciaCliente <= $nextZona['radio']){
                    //Asignamos el cliente a la ruta
                    $listaClienteParaRuta[$cantidadClientesAsignados] = array(
                        "idCliente" => $value->id,
                        "nombre" => $value->name,
                        "tipo" => "Semana Inicial",
                        "distancia" => $distanciaCliente,
                        "pagoRecolecto" => $nextZona['valor'],
                        "zona" => $nextZona['titulo'],
                    );
                    $totalRuta += $nextZona['valor'];
                    break;
                  }
                  //En caso de que cliente este en el radio de zona 1
                  else if(($key == 1) && ($distanciaCliente < $zona['radio'])){
                    //Asignamos el cliente a la ruta
                    $listaClienteParaRuta[$cantidadClientesAsignados] = array(
                        "idCliente" => $value->id,
                        "nombre" => $value->name,
                        "tipo" => "Semana Inicial",
                        "distancia" => $distanciaCliente,
                        "pagoRecolecto" => $zona['valor'],
                        "zona" => $zona['titulo'],
                    );
                    $totalRuta += $zona['valor'];
                    break;
                  }
                }
              }

              $cantidadClientesAsignados++;
              $listPoints[$key] = $value;
            }

            /*Asignamos los clientes restantes a la ultima ruta*/
            $ruta[$cuentaRuta]['listadoClientes'] = $listaClienteParaRuta;
            $ruta[$cuentaRuta]['pagoRecolector'] = $totalRuta;
          }

          $clientesSemanaInicial = Cliente::where('status', 1) /*****Activo */
                          ->where('id_rol', 3) /*****Cliente */
                          ->where('semana_inicial', 1) /*****Cliente Semana 0 */
                          ->where('novedad', 0) /* No novedad */
                          ->whereJsonContains('dias_recoleccion', [''.$acrDay => 1]) /*****Dia Lunes*/
                          ->OrderBy('latitud','desc') /*****Norte a sur */
                          ->where('fecha_vencimiento_plan', '>=', date('Y-m-d', strtotime($firstDay)))
                          ->get();

          if($clientesSemanaInicial->count()!=0){ /*Si hay clientes*/

            $ruta[$cuentaRuta] = array(
                "titulo" => "Ruta #".$cuentaRuta,
                "fecha" => date('Y-m-d', strtotime($firstDay))
            );
            foreach($clientesSemanaInicial as $key => $value) {
              // Si la cantidad de clientes ya asignados alcanza la capacidad de clientes
              // por ruta, se guarda el listado de clientes creados hasta el momento junto con
              // el monto total de esa ruta para el recolector
              if($cantidadClientesAsignados == $capacidadRuta) {

                  $ruta[$cuentaRuta]['listadoClientes'] = $listaClienteParaRuta;
                  $ruta[$cuentaRuta]['pagoRecolector'] = $totalRuta;

                  //Creamos la siguiente ruta
                  $cuentaRuta++;
                  $ruta[$cuentaRuta] = array(
                      "titulo" => "Ruta #".$cuentaRuta,
                      "fecha" => date('Y-m-d', strtotime($firstDay))
                  );
                  //Y se reinician las variables y se procede a continuar a hacer las asignaciones
                  $totalRuta = 0;
                  $listaClienteParaRuta = array();
                  $cantidadClientesAsignados = 0;
              }

              //Distancia del cliente con respecto a la sede.
              $distanciaCliente = $this->distance(4.74112408575684, -74.04163608173303, floatval($value->latitud), floatval($value->longitud));

              //Verificamos a que zona pertenece el cliente
              foreach($zonasLista as $key => $zona) {
                if($cantidadZonas>$key){
                  $nextZonaId = $key+1;
                  $nextZona = $zonasLista[$nextZonaId];

                  // Como las zonas se definen como el area circunferencial de la diferencia
                  // entre el area circular con radio de ZonaActual y area circular con radio de ZonaSiguiente,
                  // debemos verificar que el cliente este en en algun punto de esa area.
                  if($distanciaCliente >= $zona['radio'] && $distanciaCliente <= $nextZona['radio']){
                    //Asignamos el cliente a la ruta
                    $listaClienteParaRuta[$cantidadClientesAsignados] = array(
                        "idCliente" => $value->id,
                        "nombre" => $value->name,
                        "tipo" => "Semana Inicial",
                        "distancia" => $distanciaCliente,
                        "pagoRecolecto" => $nextZona['valor'],
                        "zona" => $nextZona['titulo'],
                    );
                    $totalRuta += $nextZona['valor'];
                    break;
                  }
                  //En caso de que cliente este en el radio de zona 1
                  else if(($key == 1) && ($distanciaCliente < $zona['radio'])){
                    //Asignamos el cliente a la ruta
                    $listaClienteParaRuta[$cantidadClientesAsignados] = array(
                        "idCliente" => $value->id,
                        "nombre" => $value->name,
                        "tipo" => "Semana Inicial",
                        "distancia" => $distanciaCliente,
                        "pagoRecolecto" => $zona['valor'],
                        "zona" => $zona['titulo'],
                    );
                    $totalRuta += $zona['valor'];
                    break;
                  }
                }
              }

              $cantidadClientesAsignados++;
              $listPoints[$key] = $value;
            }

            //Guardamos el listado actual y el monto total para la ruta actual.
            $ruta[$cuentaRuta]['listadoClientes'] = $listaClienteParaRuta;
            $ruta[$cuentaRuta]['pagoRecolector'] = $totalRuta;
          }

          if(count($ruta)>0){
            $this->storeRoutes2($ruta, $listRecolectores);
            //return $listRecolectores;
          }else{
            //no hay clientes para este dia;
          }

        }
        else{
          // Si la fecha aparece como dia festivo, se ignora y se procede a marcar
          // a todos los usuarios que tengan recolecta ese como no atendidos por dia DiaFestivo
          // Y posteriormente, en el admin se deberan asignar de forma manual a una ruta disponible
          $clientesRecoleccion = Cliente::where('status', 1) /*****Activo */
                          ->where('id_rol', 3) /*****Cliente */
                          ->where('semana_inicial', 0) /*****Cliente regular */
                          ->where('novedad', 0) /* No novedad */
                          ->whereJsonContains('dias_recoleccion', [''.$acrDay => 1]) /*****Dia Lunes*/
                          ->where('fecha_vencimiento_plan', '>=', date('Y-m-d', strtotime($firstDay)))
                          ->OrderBy('latitud','asc') /*****Sur a norte */
                          ->update(['dia_festivo' => 1]);

          $clientesSemanaInicial = Cliente::where('status', 1) /*****Activo */
                          ->where('id_rol', 3) /*****Cliente */
                          ->where('semana_inicial', 1) /*****Cliente Semana inicial(Primerizos a los que solo se les hace entrega del kit inicial)*/
                          ->where('novedad', 0) /* No novedad */
                          ->whereJsonContains('dias_recoleccion', [''.$acrDay => 1]) /*****Dia Lunes*/
                          ->OrderBy('latitud','desc') /*****Norte a sur */
                          ->where('fecha_vencimiento_plan', '>=', date('Y-m-d', strtotime($firstDay)))
                          ->update(['dia_festivo' => 1]);

        }
      }

      return "Simulacion terminada";

    }
    private function storeRoutes2($routes, $listRecolectores){

      $actualRecolector = null;
      $recolectorIndex = 0;
      $hayRecolecParaEsteDia = false;

      if(sizeof($listRecolectores)>0){
        $hayRecolectoresParaDiaActual = true;
      }

      foreach($routes as $route) {
        $ruta = new RutaLista;
        $ruta->fecha = $route['fecha'];
        $ruta->titulo = $route['titulo'];
        $ruta->pago_recolector = $route['pagoRecolector'];
        $totalClientesRuta = $ruta->total_clientes = count($route['listadoClientes']);

        $flagRecolector = true;

        if($hayRecolectoresParaDiaActual){
          //Definimos previamente un id de recolector nulo en caso de que no exista un recolector
          //con capacidad disponible y esta ruta se guarde con asignacion de recolector pendiente.
          $ruta->id_recolector = 0;

          // Verificamos cada recolector para ver quien tiene capacidad disponible
          // e ir signandole rutas
          do{
            //$actualRecolector = $listRecolectores[$recolectorIndex];
            if($listRecolectores[$recolectorIndex]['capacidad'] >= $totalClientesRuta) {
                $listRecolectores[$recolectorIndex]['capacidad'] = $listRecolectores[$recolectorIndex]['capacidad'] - $totalClientesRuta;
                $ruta->id_recolector = $listRecolectores[$recolectorIndex]['id_recolector'];
                $flagRecolector = false;
            }
            $actualRecolector = next($listRecolectores);
            if($actualRecolector=="" || $recolectorIndex+1 >= count($listRecolectores)){
                //$actualRecolector = reset($listRecolectores);
                $recolectorIndex = -1;
            }
            $recolectorIndex++;
          } while($flagRecolector);

        }
        else{
          $ruta->id_recolector = 0;
        }

        $ruta->save();

        $contadorInventarioSemanaInicial = 0;
        $contadorInventarioRecogidaNormal = 0;

        /*Guardamos en DB los clientes de cada ruta */
        foreach($route['listadoClientes'] as $cliente) {
            //print_r($cliente);
            $clienteDB = new RutaClienteLista;
            $clienteDB->id_cliente = $cliente['idCliente'];
            $clienteDB->id_ruta = $ruta->id;
            $clienteDB->zona = $cliente['zona'];
            $clienteDB->pago_recolector = $cliente['pagoRecolecto'];
            $clienteDB->tipo_servicio = $cliente['tipo'];
            $clienteDB->save();

            if($cliente['tipo'] == 'Semana Inicial') {
                $contadorInventarioSemanaInicial++;
            }
            else{
                $contadorInventarioRecogidaNormal++;
            }
        }

        /* Generamos el Inventario para la ruta*/
        $inventarioSi = Inventarios::where('tipo_servicio', 'Semana Inicial')->get();
        $inventarioRn = Inventarios::where('tipo_servicio', 'Recogida Normal')->get();

        $arrayInventarioFinalDeRuta = array();

        if($contadorInventarioSemanaInicial !=0 ){
            /* Items Semana Inicial*/
            foreach ($inventarioSi as $key => $value) {
                $arrayInventarioFinalDeRuta[] = ($value->cantidad * $contadorInventarioSemanaInicial).' '.$value->producto;
            }
        }

        if($contadorInventarioRecogidaNormal !=0 ){
            /* Items Recogida Normal*/
            foreach ($inventarioRn as $key => $value) {
                $arrayInventarioFinalDeRuta[] = ($value->cantidad * $contadorInventarioRecogidaNormal).' '.$value->producto;
            }
        }

        $ruta->inventario = implode(", ", $arrayInventarioFinalDeRuta);
        $ruta->save();
      }
    }

    private function distance($lat1, $lon1, $lat2, $lon2) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;

            return ($miles * 1.609344);
        }
    }
}

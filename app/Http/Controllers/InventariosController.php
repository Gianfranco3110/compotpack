<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use App\Inventarios;
use Auth;

class InventariosController extends Controller
{
    protected $customMessages = [
            'required' => 'El campo :attribute es obligatorio.',
            'numeric' => 'El campo :attribute debe ser numerico.',
        ];

    protected $niceNames = [
            'tipo_servicio' => 'Tipo de Servicio'
        ];

    public function index()
    {
        $semanaInicial = Inventarios::where('tipo_servicio', 'Semana Inicial')->get();
        $recogidaNormal = Inventarios::where('tipo_servicio', 'Recogida Normal')->get();        

        return view('inventarios/index' , ['semanaInicial' => $semanaInicial, 'recogidaNormal' => $recogidaNormal]);
    }

    public function create()
    {
        return view('inventarios/create');
    }

    public function store(Request $request)
    {
        $rules = [
            'producto' => 'required',
            'tipo_servicio' => 'required',
            'cantidad' => 'required|numeric'
        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);
        
        $item = new Inventarios;
        $item->cantidad = $request->input('cantidad');
        $item->producto = $request->input('producto');
        $item->tipo_servicio = $request->input('tipo_servicio');
        $item->save();        

        return redirect('inventarios')->with('success','Registro guardado con éxito.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {        
        $item = Inventarios::findOrFail($id);
        return view('inventarios/edit' , [
                                        'item' => $item
                                        ]);
    }

    public function update(Request $request, $id)
    {
        $item = Inventarios::findOrFail($id);

        $rules = [
            'producto' => 'required',
            'tipo_servicio' => 'required',
            'cantidad' => 'required|numeric'
        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);

        $item->cantidad = $request->input('cantidad');
        $item->producto = $request->input('producto');
        $item->tipo_servicio = $request->input('tipo_servicio');
        $item->save();        

        return redirect('inventarios')->with('success','Registro editado con éxito.');
    }

    public function destroy($id)
    {
        $item = Inventarios::findOrFail($id);
        $item->delete();
        return redirect('inventarios')->with('success','Registro eliminado con éxito.');
    }

    public function setStatus($id)
    {
        $cliente = Cliente::findOrFail($id);
        $cliente->status = $cliente->status==1?0:1;
        $cliente->save();
        return redirect('cliente')->with('success','Estado modificado con éxito.');
    }
}
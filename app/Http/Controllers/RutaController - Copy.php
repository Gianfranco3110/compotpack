<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use App\Ruta;
use App\Zona;
use App\User;

class RutaController extends Controller
{
    protected $customMessages = [
            'required' => 'El campo :attribute es obligatorio.',
            'regex' => 'El campo :attribute debe contener solo letras.',
            'numeric' => 'El campo :attribute debe ser numerico.',
        ];

    protected $niceNames = [
            'titulo' => 'Titulo'
        ];

    public function index($daySelected, $weekSelected)
    {
        $semanaTranslate = Config::get('weekDays.week');

        if($daySelected==0) { /*****Si entra por primera ver el dia seleccionado es el dia en curso*/
            $daySelected = date('Y-m-d');
        }

        $arraySemanas = array();

        for($i=1; $i<=8; $i++) {

            if($i==1) {
                $fecha = date('Y-m-d');
            }
            else{
                $fecha = date("d-m-Y",strtotime($fecha."+ 1 week")); ;                 
            }
            
            $lunes =  date( 'Y-m-d', strtotime( 'monday this week', strtotime($fecha) ) );
            $viernes =  date( 'Y-m-d', strtotime( 'friday this week', strtotime($fecha) ) );

            $arraySemanas[$i] = [
                            "Mon" => $lunes,
                            "Fri" => $viernes
                           ];

            /**************Verificamos en que semana se encuentra la fecha selccionada*****/////
            if($weekSelected==0){
                if($daySelected >= $lunes && $daySelected <= $viernes) {
                    $weekSelected = $i;
                }
            }
            elseif($weekSelected == $i){
                $daySelected = $lunes;
            }
        }

        $dayAcron = date('D', strtotime($daySelected));
        $zonas = Zona::where('status', 1)->orderBy('titulo')->get();
        $recolectores = User::where('status', 1)->where('id_rol', 2)->orderBy('name')->get();
       
        return view('ruta/index' , [
                                    'zonas' => $zonas,
                                    'dayAcron' => $dayAcron,
                                    'weekSelected' => $weekSelected,
                                    'daySelected' => $daySelected,
                                    'arraySemanas' => $arraySemanas,
                                    'semanaTranslate' => $semanaTranslate,
                                    'recolectores' => $recolectores
                                    ]);
    }

    public function create()
    {
        return view('ruta/create');
    }

    public function store(Request $request)
    {
        $rules = [
            'titulo' => 'required',
            'descripcion' => 'required',
            'precio' => 'required|numeric',
            'frecuencia_cobro' => 'required|numeric'
        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);
        
        $ruta = new Ruta;
        $ruta->titulo = $request->input('titulo');
        $ruta->descripcion = $request->input('descripcion');
        $ruta->precio = $request->input('precio');
        $ruta->frecuencia_cobro = $request->input('frecuencia_cobro');
        $ruta->recurrente = $request->has('recurrente') ? 1 : 0;
        $ruta->save();

        return redirect('ruta')->with('success','Registro guardado con éxito.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $ruta = Ruta::findOrFail($id);
        return view('ruta/edit' , ['ruta' => $ruta]);
    }

    public function update(Request $request, $id)
    {
        $ruta = Ruta::findOrFail($id);

        $rules = [
            'titulo' => 'required',
            'descripcion' => 'required',
            'precio' => 'required|numeric',
            'frecuencia_cobro' => 'required|numeric'
        ];

        $this->validate($request, $rules, $this->customMessages, $this->niceNames);

        $ruta->titulo = $request->input('titulo');
        $ruta->descripcion = $request->input('descripcion');
        $ruta->precio = $request->input('precio');
        $ruta->frecuencia_cobro = $request->input('frecuencia_cobro');
        $ruta->recurrente = $request->has('recurrente') ? 1 : 0;
        $ruta->save();

        return redirect('ruta')->with('success','Registro editado con éxito.');
    }

    public function destroy($id)
    {
        $ruta = Ruta::findOrFail($id);
        $ruta->status = 2;
        $ruta->save();
        return redirect('ruta')->with('success','Registro eliminado con éxito.');
    }

    public function setStatus($id)
    {
        $ruta = Ruta::findOrFail($id);
        $ruta->status = $ruta->status==1?0:1;
        $ruta->save();
        return redirect('ruta')->with('success','Estado modificado con éxito.');
    }

    public function messages()
    {
        return [
            'required' => 'ES UN title is required'
        ];
    }
}
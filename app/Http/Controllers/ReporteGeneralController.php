<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\RecolectorRuta;
use App\RutaLista;
use App\RutaClienteLista;
use App\RecolectorDiaAsignado;
use App\EstadosRecoleccion;
use App\User;
use App\HistorialPagos;
use App\HistorialPagoRecolector;

class ReporteGeneralController extends Controller
{
    protected $customMessages = [
            'required' => 'El campo :attribute es obligatorio.',
            'regex' => 'El campo :attribute debe contener solo letras.',
            'numeric' => 'El campo :attribute debe ser numerico.',
        ];

    protected $niceNames = [
            'titulo' => 'Titulo'
        ];

    public function index(Request $request)
    {
        #print_r($request);
        $fecha_inicio;
        $fecha_fin;
        $today = date('Y-m-d');
        $fecha_maxima = date("d-m-Y",strtotime($today."-1 day"));

        if($request->exists('fecha_inicio') && $request->exists('fecha_fin') && $request->exists('tipo_reporte')) {

            $fecha_inicio = $request->input('fecha_inicio');
            $fecha_fin = $request->input('fecha_fin');

            switch ($request->input('tipo_reporte')) {
                case 1: /* Reporte de pagos por suscripcion */
                    $pagos = HistorialPagos::where('fecha', '>=', date('Y-m-d', strtotime($fecha_inicio))." 00:00:00")
                                            ->where('fecha', '<=', date('Y-m-d', strtotime($fecha_fin))." 23:59:59")
                                            ->where('estado', 'Aprobada')
                                            ->get();
                    return view('reportegeneral/suscripciones', [
                        'fecha_inicio' => $fecha_inicio,
                        'fecha_fin' => $fecha_fin,
                        'fecha_maxima' => $fecha_maxima,
                        'pagos' => $pagos
                    ]);
                    exit();
                    break;

                case 2: /* Reporte de Recoletores y servicios */
                    $recolectores = User::where('status',1)
                                        ->where('id_rol',2)
                                        ->get();
                    return view('reportegeneral/recolectores', [
                        'fecha_inicio' => $fecha_inicio,
                        'fecha_fin' => $fecha_fin,
                        'fecha_maxima' => $fecha_maxima,
                        'recolectores' => $recolectores
                    ]);
                    exit();
                    break;

                case 3: /* Reporte pagos a recolectores */
                    $historial = HistorialPagoRecolector::where('fecha', '>=', date('Y-m-d', strtotime($fecha_inicio))."")
                                                        ->where('fecha', '<=', date('Y-m-d', strtotime($fecha_fin))."")
                                                        ->get();
                    return view('reportegeneral/pagosrecolectores', [
                        'fecha_inicio' => $fecha_inicio,
                        'fecha_fin' => $fecha_fin,
                        'fecha_maxima' => $fecha_maxima,
                        'historial' => $historial
                    ]);
                    exit();
                        break;
            }
        }
        else{
            $fecha_inicio = date("d-m-Y",strtotime($today."-1 week"));
            $fecha_fin = $fecha_maxima;
        }
        
        return view('reportegeneral/index', [
                                    'fecha_inicio' => $fecha_inicio,
                                    'fecha_fin' => $fecha_fin,
                                    'fecha_maxima' => $fecha_maxima
                                ]);
    }

   
}
<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class CheckClientePerfil
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(Auth::user()->id_rol == 3 && Auth::user()->perfil_complete == 0) { /*****PERFIL SIN COMPLETAR******/
            return redirect('perfil/cliente');
        }

        return $next($request);
    }
}

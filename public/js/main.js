const animation = document.getElementById('animation'),
fragment = new DocumentFragment()

const mainPath = "/webApp01-30-2021/compotpack/public/";

/*
* Validar caracteres numericos para el número de WhatsApp
*/
function validaNumericos(event) {
  if(event.charCode >= 48 && event.charCode <= 57){
    return true;
   }
   return false;
}

function lluviaHojas() {
    let hoja = document.createElement('img')
    hoja.src = mainPath+'img/hoja2.png'
    hoja.classList.add('hojas')
    hoja.style.top = 10 + (Math.random() * 10 + 'px')
    hoja.style.left = Math.random() * innerWidth + 'px'
    fragment.appendChild(hoja)
    animation.appendChild(fragment)
    hoja.addEventListener('animationend', e => {
        if (e.animationName === 'down') {
            hoja.remove();
        }
    })
}

setInterval(lluviaHojas, 600)

function lluviaHojas2() {
    let hoja2 = document.createElement('img')
    hoja2.src = mainPath+'img/hoja.png'
    hoja2.classList.add('hojas2')
    hoja2.style.top = (Math.random() * 60) + 10 + '%'
    hoja2.style.left = Math.random() * 5 * innerWidth / 12 + '%'
    hoja2.style.animation = 'fly ' + Math.random() * 4 + 's alternate both'
    fragment.appendChild(hoja2)
    animation.appendChild(fragment)
    hoja2.addEventListener('animationend', e => {
        if (e.animationName === 'fly') {
            hoja2.remove();
        }
    })
}

setInterval(lluviaHojas2, 800)

function lluviaHojas3() {
    let hoja3 = document.createElement('img')
    hoja3.src = mainPath+'img/hoja3.png'
    hoja3.classList.add('hojas3')
    hoja3.style.top = (Math.random() * 60) + 10 + '%'
    hoja3.style.left = Math.random() * 5 * innerWidth / 12 + '%'
    hoja3.style.animation = 'fly2 ' + Math.random() * 4 + 's alternate both'
    fragment.appendChild(hoja3)
    animation.appendChild(fragment)
    hoja3.addEventListener('animationend', e => {
        if (e.animationName === 'fly2') {
            hoja3.remove();
        }
    })
}

setInterval(lluviaHojas3, 1000)

const State = () => {
    if (document.readyState === 'complete') {
        setInterval(() => {
            animation.style.animation = 'ocult 2s alternate both'
            animation.addEventListener('animationend', e => {
                if (e.animationName === 'ocult') {
                    animation.remove();
                    clearInterval(readyState)
                }
            }, 5000);
        })
    }
}

const readyState = setInterval(() => {
    State();
}, 2000);

window.sr = ScrollReveal();

sr.reveal('.header .logo2', {
    delay: 2200,
	duration: 600,
	origin: 'left',
	distance: '100px'
});

sr.reveal('.header .icon-menu', {
    delay: 2200,
	duration: 600,
	origin: 'right',
	distance: '100px'
});

sr.reveal('.home .container .logo', {
    delay: 2600,
	duration: 800,
	origin: 'bottom',
	distance: '100px'
});

sr.reveal('.home .container .logo2', {
    delay: 2600,
	duration: 800,
	origin: 'top',
	distance: '100px'
});

sr.reveal('.home .container .flecha', {
    delay: 2600,
	duration: 800,
	origin: 'bottom',
	distance: '100px'
});

sr.reveal('.home .container .flecha2', {
    delay: 2600,
	duration: 800,
	origin: 'top',
	distance: '100px'
});

sr.reveal('.home .container h1', {
    delay: 3000,
	duration: 800,
	origin: 'bottom',
	distance: '100px'
});

sr.reveal('.home .container .video .img-fluid', {
    delay: 4000,
	duration: 800,
	origin: 'top',
	distance: '100px'
});

sr.reveal('.home .container .img-planet.img1', {
    delay: 4000,
	duration: 800,
	origin: 'left',
	distance: '100px'
});

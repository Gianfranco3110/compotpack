const btnM = document.getElementById('btn-menu'),
nav = document.getElementById('nav'),
mainContainer = document.getElementById('main'),
check1 = document.getElementById('checkbox'),
check2 = document.getElementById('checkbox2'),
upArrow = document.getElementById('flecha-arriba'),
downArrow = document.getElementById('flecha-abajo')

btnM.addEventListener('click', () =>  {
    ShowMenu()
});

const ShowMenu = () => {
    nav.classList.toggle('active')
}

check1.addEventListener('change', () => {
    check2.checked = false;
});

check2.addEventListener('change', () => {
    check1.checked = false;
});

mainContainer.addEventListener('scroll', function () {
    if (mainContainer.scrollTop > 200) {
        upArrow.style.opacity = 1
        upArrow.style.zIndex = 60
    } else {
        upArrow.style.zIndex = -1
        upArrow.style.opacity = 0
    }
    if (mainContainer.scrollTop > 800) {
        downArrow.style.zIndex = -1
        downArrow.style.opacity = 0
    } else {
        downArrow.style.zIndex = 60
        downArrow.style.opacity = 1
    }
});

upArrow.addEventListener('click', () => {
    mainContainer.scrollTop = 0
});

downArrow.addEventListener('click', () => {
    mainContainer.scrollTop = 1300
});
$(document).ready(function(){
    // seleccionamos el  video
	let video = $("#video");
    // seleccionamos el anchor
	let anchor = $("#btn-play");
    // seleccionamos el iframe
    let frame = $("#youtube");
    // embed youtube url
    let embed = "https://www.youtube.com/embed/";

	// asignamos un evento click a los anchor
	anchor.on("click", function(){
        anchor.css('display', 'none')
        // optenemos el valor el href del anchor
        let href = 'https://www.youtube.com/watch?v=P_BWXCQ-V2o';
        console.log(href)
        // creamos un regex para youtube
        let you =/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
        // validamos que sea un video de youtube
        if (href.match(you)) {
            //obtenemos el id del video
            let idv = href.match(you)[1];
            // escondemos el reproductor de video html5
            video.hide();
            // configuramos el iframe con el video de youtub
            frame.attr("src",`${embed}${idv}?autoplay=1`);
            // mostramos el iframe
            frame.css("display","initial");  
        }else{
            // escondemos el iframe
            frame.css("display","none");
            // mostramos el reprodutor de video html5
            video.css("display","initial");
            // desconfiguramos el iframe para que el video se pause
            frame.attr("src",`${embed}`);
            // asignamos como src del video el valor el href del anchor
            video.src = href;
            //cargamos el video
            video.load();
            //le damos play
            video.play();
        }   
	});
});

